package com.cyh.blog.controller;

import com.cyh.blog.util.QiNiu;
import com.cyh.blog.util.Result;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 文件上傳
 */
@RestController
@RequestMapping("upload")
public class RestUploadFileController {

    /**
     * 上传封面
     * @param file
     * @return
     * @throws IOException
     */
    @PostMapping("/comment_img")
    public Result img(@RequestParam("comment_img") MultipartFile file) throws IOException {
        String path = new QiNiu().upload(file.getInputStream());
        return Result.success("上传成功").add("data",path);
    }
}
