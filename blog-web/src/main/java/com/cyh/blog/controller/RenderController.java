package com.cyh.blog.controller;

import com.cyh.blog.business.enums.ArticleStatusEnum;
import com.cyh.blog.business.enums.NavEnums;
import com.cyh.blog.business.service.BizArticleService;
import com.cyh.blog.business.vo.ArticleVo;
import com.cyh.blog.persistence.beans.BizArticle;
import com.cyh.blog.util.Result;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

@Controller
public class RenderController {

    @Autowired
    private BizArticleService bizArticleService;

    private final Integer HOT_RELATED_SIZE = 8;

    /**
     * 首页
     * @return
     */
    @GetMapping("/")
    public ModelAndView home(ModelMap map) {
        ArticleVo articleVo = new ArticleVo();
        articleVo.setPageNumber(1);
        articleVo.setPageSize(5);
        loadData(map, articleVo);
        map.addAttribute("typeId", NavEnums.HOME.getTypeId());
        return Result.view("index");
    }

    /**
     * 文章分页
     * @param map
     * @return
     */
    @GetMapping("/index/{number}")
    public ModelAndView home(@PathVariable Integer number, ModelMap map) {
        ArticleVo articleVo = new ArticleVo();
        articleVo.setPageNumber(number);
        articleVo.setPageSize(5);
        loadData(map, articleVo);
        return Result.view("index");
    }

    /**
     * 指定类别
     * @param map
     * @return
     */
    @GetMapping("/type/{typeId}")
    public ModelAndView type(@PathVariable Integer typeId, ModelMap map) {
        ArticleVo articleVo = new ArticleVo();
        articleVo.setTypeId(typeId);
        articleVo.setPageNumber(1);
        articleVo.setPageSize(5);
        loadData(map, articleVo);
        map.addAttribute("typeId",typeId);
        return Result.view("index");
    }

    /**
     * 指定标签
     * @param map
     * @return
     */
    @GetMapping("/tag/{tagId}")
    public ModelAndView tag(@PathVariable("tagId") Integer tagId, ModelMap map) {
        ArticleVo articleVo = new ArticleVo();
        articleVo.setTypeId(tagId);
        articleVo.setPageNumber(1);
        articleVo.setPageSize(5);
        loadData(map, articleVo);
        map.addAttribute("tagId",NavEnums.HOME.getTypeId());
        return Result.view("index");
    }

    /**
     * 加载一文
     * @return
     */
    @GetMapping("/article/{aid}")
    public ModelAndView article(@PathVariable("aid") Integer aid,ModelMap modelMap){
        BizArticle bizArticle = bizArticleService.selectByKey(aid);
        modelMap.addAttribute("article",bizArticle);
        //上下文信息
        Map<String, BizArticle> preAndNext =  bizArticleService.getPreAndNext(bizArticle.getCreateTime());
        //获取热门文章
        List<BizArticle> hotList = bizArticleService.getHotArticle(HOT_RELATED_SIZE);
        //相关文章, 根据本文章的同类别，同标签获取
        List<BizArticle> relatedList = bizArticleService.getRelatedArticle(bizArticle, HOT_RELATED_SIZE);
        modelMap.addAttribute("preAndNext", preAndNext);
        modelMap.addAttribute("relatedList", relatedList);
        modelMap.addAttribute("hotList", hotList);
        return Result.view("/article");
    }

    /**
     * 加载数据
     * @param map
     */
    private void loadData(ModelMap map, ArticleVo articleVo) {
        articleVo.setStatus(ArticleStatusEnum.PUBLISHED.getCode()); //已经发布
        PageInfo<BizArticle> pageInfo = bizArticleService.selectByCondition(articleVo);
        map.addAttribute("page", pageInfo);
    }


    /*
        about关于我
     */
    @GetMapping("/about")
    public ModelAndView about(ModelMap map) {
        map.addAttribute("typeId", NavEnums.ABOUT.getTypeId());
        return Result.view("/about");
    }

    /**
     * 留言
     * @return
     */
    @GetMapping("/leave")
    public ModelAndView leave(ModelMap map) {
        map.addAttribute("typeId", NavEnums.LEAVE.getTypeId());
        return Result.view("/leave");
    }

}
