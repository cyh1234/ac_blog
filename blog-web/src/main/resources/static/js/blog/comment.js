var current = 1;
$(function () {
    $.extend({
        //加载评论数据
        loadList:function (page) {
            var page = (page || 1);
            var sid = $('#sid').val();
            if (sid == null || sid.length == 0){
                sid = $('#submitComment').attr('data-id');
            }
            $.ajax({
                type: 'post',
                url: '/api/comments',
                data:{'pageNumber':page, 'status':'APPROVED','sid':sid},
                success: function (msg) {
                    if (msg.code == "0"){
                        $.alert(msg.message);
                    }else{
                        var pageInfo = msg.data.data;
                        $('#comment-number').html(pageInfo.total);
                        var html = $('#comment-list').html();
                        var box = $('#comment-box');
                        $.each(pageInfo.list , function (i, v) {
                            console.log('-++++++----');
                            console.log(v.parent);
                            var parent = "<br>";
                            var last = "";
                            if (v.pid != null && v.parent != null){
                               parent = "&nbsp;评论了&nbsp;<strong>"+v.parent.nickname+"</strong><br>";
                               last = '<div class="card border-secondary mb-3" style="max-width: 20rem;">' +
                                   '<div class="card-body" style="padding: 0.3rem">' +
                                   '<p class="card-text">'+v.parent.content+'</p>' +
                                   '</div></div>'
                            }
                            html += '<div class="feed-element">' +
                                '<input name="comment-'+v.id+'" hidden value="'+v.id+'"/>' +
                                '<a href="profile.html#" class="pull-left">' +
                                '<img alt="image" class="img-circle" src="'+v.avatar+'">' +
                                '</a><div class="media-body  ">' +
                                '<small class="pull-right text-navy">'+new Date(v.createTime).format("yyyy-MM-dd hh:mm:ss")+'</small>' +
                                '<strong>'+v.nickname+'</strong>' +parent+
                                '<span>'+v.content+'</span>' +
                                last+
                                '<div class="actions">' +
                                '<a class="btn btn-xs btn-white" onclick="supportFun(this, '+v.id+')" ><i class="fa fa-thumbs-up"></i> 赞&nbsp; <span name="supportNum">'+v.support+'</span></a>' +
                                '<a class="btn btn-xs btn-white" onclick="opposeFun(this, '+v.id+')" ><i class="fa fa-thumbs-down"></i> 踩&nbsp; <span name="opposeNum">'+v.oppose+'</span></a>' +
                                '<a class="btn btn-xs btn-white" onclick="commentUser(this, '+v.id+')"><i class="fa fa-pencil"></i> 评论 </a>' +
                                '</div></div></div>';
                        })
                        $('#comment-list').html(html);
                        console.log($('#show-more'));
                        $('.show-more').remove();
                        if (pageInfo.hasNextPage){
                            box.append('<button  class="show-more btn btn-primary btn-block btn-sm"><i class="fa fa-arrow-down"></i> 显示更多</button>');
                        }
                    }
                }
            });
        }
    });

    //初始化评论
    $.loadList(1);

    //显示更多
    $('#comment-box').on('click','.show-more', function () {
        $.loadList(++current);
    })
})


//用户提交评论,弹出身份对话框
function submitComment(){
    var qqInfo = localStorage.getItem("qqInfo");
    //设置到模态中
    if (qqInfo != null){
        qqInfo = JSON.parse(qqInfo);
        $('#qqInfoDiv').find('input').each(function () {
            $(this).val(qqInfo[$(this).attr('id')]);
        });
    }
    $('#commentModal').modal('show');
}

//提交评论
$('#submitComment').on('click',function(){
    var content = editor.txt.text();
    var qqInfo = localStorage.getItem("qqInfo");
    if (qqInfo != null){
        qqInfo = JSON.parse(qqInfo);
        qqInfo['content'] = content;
    }
    var sid = $('#sid').val();
    if (sid == null || sid.length == 0){
        sid = $(this).attr('data-id');
    }
    $.ajax({
        type: 'post',
        url: '/api/comment?sid='+sid+"&pid="+$('#comment-pid').val(),
        data: qqInfo,
        success: function (msg) {
            $.alert(msg.message);
            $('#commentModal').modal('hide');
            editor.txt.html('')
        }
    });
})

//用户评论用户
function commentUser(node, pid) {
    var editor = $('.comment-editor');
    $(node).parent().parent().parent().append(editor);
    if ($('span[name="exitComment"]').length == 0){
        $('span[name="comment-header"]').after('<span name="exitComment" style="float: right">' +
            '<button onclick="exitComment(this)" class="btn btn-info btn-sm">取消评论</button>' +
            '</span>');
    }
    $('#comment-pid').val(pid); //设置当前被评论的id
}

//取消用户评论用户
function exitComment(node) {
    console.log($(node).parent().parent().parent().html());
    $('#related-article').after($(node).parent().parent().parent());
    $('span[name="exitComment"]').remove();
}

//评论点赞
function supportFun(node, commentId) {
    $.ajax({
        type: 'post',
        url: '/api/support?commentId='+commentId,
        success: function (msg) {
            if (msg.code == "0"){
                $.alert(msg.message);
            }else{
                var tag = $(node).find('span');
                var num = parseInt(tag.text());
                tag.text(num+1);
            }
        }
    });
}

//踩事件
function opposeFun(node, commentId) {
    aj
    $.ajax({
        type: 'post',
        url: '/api/oppose?commentId='+commentId,
        success: function (msg) {
            if (msg.code == "0"){
                $.alert(msg.message);
            }else{
                var tag = $(node).find('span');
                var num = parseInt(tag.text());
                tag.text(num+1);
            }
        }
    });
}
