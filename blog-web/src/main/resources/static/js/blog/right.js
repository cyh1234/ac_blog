var classList =  ["badge-primary", "badge-secondary", "badge-success",
                    "badge-danger", "badge-warning", "badge-info",
                    "badge-light","badge-dark"];
//修改标签样式
function updateTagsClass(){
    $.each($("span[name=tag]"), function (i, v) {
        var index = i % (classList.length+1);
        $(this).addClass(classList[index]);
    });
}



$(function(){
    updateTagsClass();

    $('input[name="searchText"]').bind('input oninput onpropertychange',function(){
        var content = $(this).val();
        if (content.length == 0){
            $('ul[name="searchBox"]').html("");
            return ;
        }
        $.ajax({
            type: 'post',
            url: '/api/search',
            data: {'content': content},
            success: function (msg) {
                var list = msg.data.data;
                var html = "";
                $.each(list, function (i, v) {
                    html += '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                        '<span><a href="/article/'+v.id+'">'+v.title+'</a></span>' +
                        '</li>';
                });
                $('ul[name="searchBox"]').html(html);
            }
        });
    })
});
