var editor ;
$(function(){
    editor  = new wangEditor('#editor');
    editor.customConfig.menus = [
        'head',  // 标题
        'bold',  // 粗体
        'fontSize',  // 字号
        'fontName',  // 字体
        'italic',  // 斜体
        'foreColor',  // 文字颜色
        'backColor',  // 背景颜色
        'link',  // 插入链接
        'list',  // 列表
        'justify',  // 对齐方式
        'emoticon',  // 表情
        'image',  // 插入图片
        'table',  // 表格
        'code',  // 插入代码
    ]
    editor.customConfig.uploadImgServer = '/upload/comment_img'; //上传URL
    editor.customConfig.uploadImgMaxSize = 3 * 1024 * 1024;
    editor.customConfig.uploadImgMaxLength = 5;
    editor.customConfig.uploadFileName = 'comment_img'; //和后台的对应
    editor.customConfig.uploadImgHooks = {
        customInsert: function (insertImg, result, editor) {
            // 图片上传并返回结果，自定义插入图片的事件（而不是编辑器自动插入图片！！！）
            // insertImg 是插入图片的函数，editor 是编辑器对象，result 是服务器端返回的结果
            var url = result.data.data;
            // 举例：假如上传图片成功后，服务器端返回的是 {url:'....'} 这种格式，即可这样插入图片：
            insertImg(url);
            // result 必须是一个 JSON 格式字符串！！！否则报错
        }
    };
    editor.customConfig.zIndex = 1;
    editor.create();

    //添加语言，识别代码高亮
    $('pre').addClass('language-X line-numbers').css("white-space", "pre-wrap");

    //初始化页面数据
    initData();
})


//初始化页面数据
function initData(){
    var qqInfo = localStorage.getItem("qqInfo");
    if (qqInfo != null){
        qqInfo = JSON.parse(qqInfo);
        $('#qqName').html("&nbsp;当前用户：&nbsp;"+qqInfo.nikeName);
    }
    //qq号输入框失去焦点
    $('#number').blur(function(){
        var qq = $(this).val();
        $.ajax({
            type: 'post',
            url: '/api/qqInfo',
            data: {"number" : qq},
            success: function (msg) {
                var qqInfo = msg.data.data;
                console.log(qqInfo);
                qqInfo = JSON.parse(qqInfo);
                $('#qqInfoDiv').find('input').each(function () {
                    $(this).val(qqInfo[$(this).attr('id')]);
                });
                if (msg.message == "成功"){
                    localStorage.setItem("qqInfo",JSON.stringify(qqInfo));
                    $('#qqName').html("&nbsp;当前用户：&nbsp;"+qqInfo.nikeName);
                }
            }
        });
    })
}


//文章点赞
function pariseFun(node,articleId){
    $.ajax({
        type: 'post',
        url: '/api/parise?articleId='+articleId,
        success: function (msg) {
            if (msg.code=="0"){
                $.alert(msg.message);
            }else{
                var num = parseInt($('span[name="pariseNum"]').text());
                $('span[name="pariseNum"]').text(num+1);
            }
        }
    });
}