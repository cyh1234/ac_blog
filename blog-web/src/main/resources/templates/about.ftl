<#include "include/macros.ftl">
<@header></@header>


<div class="container top30">
    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                <h1 class="display-3">您好！<span name="userName">访客</span></h1>
                <p class="lead">本博客是本人学习完springboot后，参考了<a href="https://www.zhyd.me/">张亚东博客</a>，大致完成了博客的功能。</p>
                <hr class="my-4">
                <p>您可以通过下面方式找到我：</p>
                <p class="lead">
                    <a target="_blank" class="btn btn-primary btn-lg" href="http://wpa.qq.com/msgrd?v=3&uin=2228114147&menu=yes" role="button">联系我</a>
                    <a  target="_blank" class="btn btn-primary btn-lg" href="//shang.qq.com/wpa/qunwpa?idkey=792e0976f5f8aa9392f5a3c8cc4f13ca5e62a14e173c89ff8eb8bd090540855b" role="button">加入早起计划</a>
                </p>
                <p>您还可以：</p>
                <p class="lead">
                    <button onclick="friendfun()" target="_blank" class="btn btn-primary btn-lg">申请友链</button>
                </p>
            </div>
        </div>
    <#--<@rightDiv></@rightDiv>-->
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="addroleLabel">申请友链</h4>
                <button article="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form id="applyForm" class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label">网站名称</label>
                        <div class="controls">
                            <input id="roleText" name="name" class="form-control" type="text" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">网站地址</label>
                        <div class="controls">
                            <input id="roleEnText" name="url" class="form-control" type="text" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">网站描述</label>
                        <div class="controls">
                            <input id="roleEnText" name="description" class="form-control" type="text" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">站长邮箱</label>
                        <div class="controls">
                            <input id="roleEnText" name="email" class="form-control" type="text" />
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button article="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button article="button" class="btn btn-primary apply">提交</button>
            </div>
        </div>
    </div>
</div>

<@footer></@footer>


