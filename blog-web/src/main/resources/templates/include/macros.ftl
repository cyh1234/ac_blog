<#--头部s-->
<#macro header title="blog" keywords="默认文字">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="Keywords" content="${config.homeKeywords }">
    <meta name="description" content="${config.homeDesc}" />
    <meta name="author" content="${config.authorName}">
    <title>${title}</title>
    <#include "/layout/link.ftl">
</head>
<body style="background: #ebebeb">
    <#include "/layout/header.ftl">
</#macro>
<#--头部e-->

<#--bulletin公告-->
<#macro bulletin>

</#macro>
<#--bulletin公告e-->

<#--分页s-->
<#macro pageBar>
    <div class="bottom">
        <ul class="pagination">
          <#--有前一页-->
            <#if page.hasPreviousPage>
                <li class="page-item">
                    <a href="/index/${page.pageNum - 1}" class="page-link" >&laquo;</a>
                </li>
            </#if>

            <#--遍历所有-->
            <#if page?? && page.list?size gt 0>
                <#list 1..page.pages as item>
                    <li ${(page.pageNum == item)?string("class='active page-item'","")} class="page-item">
                        <a  href="/index/${item}" class="page-link" href="#">${item}</a>
                    </li>
                </#list>
            </#if>

            <#--是否有下一页-->
            <#if page.hasNextPage>
                <li class="page-item">
                    <a  href="/index/${page.pageNum + 1}" class="page-link">&raquo;</a>
                </li>
            </#if>
        </ul>
    </div>
</#macro>
<#--分页e-->



<!--右侧s-->
<#macro rightDiv>
    <#include "/layout/right.ftl">
</#macro>
<!--右侧e-->


<#--页脚s-->
<#macro footer>
    <#include "/layout/footer.ftl">
    </body>
    </html>
</#macro>
<#--页脚e-->