<#include "include/macros.ftl">
<@header  title="啊程 | 文章详情"></@header>
<style>
    .div{
        margin: 20px 0 20px 0;
        width: 100%;
        height: 1px;
        border-top: 1px #ccc dotted;
        text-align: center;
        overflow: initial !important;
    }
    .highlight {background-color: #FFFF88; }

</style>

<div class="container top30">
    <div class="row">
        <div class="col-md-12" style="font-size: 16px; ">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">🏡主页</a></li>
                <li class="breadcrumb-item"><a href="/type/${article.bizType.id}">${article.bizType.name}</a></li>
                <li class="breadcrumb-item active">正文</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="col-md-12 article-div" style="background: white">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <span class="badge badge-pill badge-warning">
                            ${(article.status == 0)?string("原创","转载")}
                        </span>
                    </div>
                    <div class="col-md-8" style="font-size: 18px;">
                        <span>${article.createTime?string('yyyy-MM-dd hh:mm:ss')}</span>
                        <span class="badge badge-light">👓浏览量&nbsp;${article.lookCount}</span>
                        <span class="badge badge-light"><a class=".special-a black" href="#">💬评论&nbsp;${article.commentCount}</a></span>
                    </div>
                </div>
                <div class="col-md-12">
                    <hr>
                </div>
                <div class="col-md-12">
                    <div class="col-md-12">
                        <h4>${article.title}</h4>
                        ${article.content}
                    </div>
                </div>
                <div class="col-md-12">
                    <hr>
                    <div style="text-align: center">
                        <a class="btn btn-xs btn-info" onclick="pariseFun(this, ${article.id})" ><i class="fa fa-thumbs-up"></i>
                            赞&nbsp; <span name="pariseNum">${article.loveCount}</span></a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-12">
                        所属分类：<a href="/type/${article.typeId}">前端技术</a>
                    </div>
                </div>
            </div>

            <div class="col-md-12 article-div" style="background: white">
                <div class="col-md-12">
                    <span class="badge bottom">本文标签</span>
                    <#list article.tags as tags>
                        <span class="badge bottom badge-secondary"><a href="/tag/${tags.id}">${tags.name}</a></span>
                    </#list>
                </div>
                <div class="col-md-12">
                    <span>版权声明：</span>
                    <span>本站原创文章,转载请注明出处</span>
                </div>
            </div>

            <#--上下页-->
            <div class="article-div" style="background: white">
                <div class="col-md-12">
                    <div class="row">

                    <#list preAndNext as item>
                        <div class="col-md-6" style="text-align: right">
                            <span class="badge bottom badge-warning">
                                <a href="/article/${item.pre.id}"><<&nbsp;上一篇</a>
                            </span>
                        </div>

                        <div class="col-md-6" style="text-align: left">
                            <span class="badge bottom badge-warning">
                                <a href="/article/${item.next.id}">下一篇&nbsp;>></a>
                            </span>
                        </div>
                    </#list>
                    </div>
                </div>
            </div>

            <#--热门推荐-->
            <div class="col-md-12 article-div" style="background: white">
                <div class="col-md-12">
                    <span>🔥热门推荐</span>
                    <hr>
                </div>
                <div class="col-md-12">
                    <div class="row">
                    <#list hotList as item>
                        <div class="col-md-6" style="margin-bottom: 10px;">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="${item.coverImage}" style="width: 70px; height: 70px;">
                                </div>
                                <div class="col-md-8">
                                    <div>
                                        <a href="/article/${item.id}">${item.title}</a>
                                    </div>
                                    <div style="font-size: 14px">
                                        <span>👓浏览量&nbsp;${item.lookCount}</span>
                                        <span >💬评论&nbsp;${item.commentCount}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </#list>
                    </div>
                </div>
            </div>

            <#--相关文章-->
            <div class="col-md-12 article-div" id="related-article" style="background: white">
                <div class="col-md-12">
                    <span>📃相关文章</span>
                    <hr>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <#list relatedList as item>
                            <div class="col-md-6" style="margin-bottom: 10px;">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="${item.coverImage}" style="width: 70px; height: 70px;">
                                    </div>
                                    <div class="col-md-8">
                                        <div>
                                            <a href="/article/${item.id}">${item.title}</a>
                                        </div>
                                        <div style="font-size: 14px">
                                            <span>👓浏览量&nbsp;${item.lookCount}</span>
                                            <span >💬评论&nbsp;${item.commentCount}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </#list>
                    </div>
                </div>
            </div>

            <#--用户评论框-->
            <div class="col-md-12 article-div comment-editor" style="background: white">
                <div class="col-md-12">
                    <span name="comment-header">💬发表评论
                        <sapn id="qqName" style="font-size: 14px"></sapn>
                    </span>
                </div>
                <hr>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12" style="height: 150px;">
                            <div id="editor" style="max-height: 100px;">
                            </div>
                        </div>
                        <#--本文id-->
                        <input hidden id="sid" value="${article.id}"/>
                    </div>
                    <button style="margin-top: 20px;" class="btn btn-info btn-block btn-sm" onclick="submitComment()">评论</button>
                </div>
            </div>

            <#--用户评论-->
                <div class="col-md-12 article-div" style="background: white">
                    <div class="col-md-12">
                        <span>💬<span id="comment-number">0</span>条评论</span>
                        <hr>
                    </div>
                        <div class="col-sm-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content">
                                    <div id="comment-box">
                                        <div id="comment-list" class="feed-activity-list">
                                            <!--评论item-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
        </div>
        <@rightDiv></@rightDiv>
    </div>
</div>


<#--当前被评论的的标识-->
<input id="comment-pid" hidden/>
<#--当前赞踩的的标识-->
<input id="ops-pid" hidden/>

<!-- 评论模态框 -->
<div class="modal fade bd-example-modal-sm" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">修改信息</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <!--表单s-->
                <form id="qqInfoDiv" class="form-horizontal qq-info-form">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text" id="btnGroupAddon">QQ</div>
                        </div>
                        <input id="number" type="text" class="form-control" placeholder="请输入qq号码"  aria-describedby="btnGroupAddon">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text" id="btnGroupAddon">昵称</div>
                        </div>
                        <input id="nikeName" type="text" class="form-control" aria-describedby="btnGroupAddon">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text" id="btnGroupAddon">邮箱</div>
                        </div>
                        <input id="email" type="text" class="form-control" aria-describedby="btnGroupAddon">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text" id="btnGroupAddon">网址</div>
                        </div>
                        <input id="zone" type="text" class="form-control"  aria-describedby="btnGroupAddon">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="submitComment" data-id="0" type="button" class="btn btn-primary btn-sm">提交评论</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<@footer></@footer>
