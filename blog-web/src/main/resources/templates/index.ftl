<#include "include/macros.ftl">
<@header title="啊程 | 博客"></@header>
<script>
    $(function(){
        $('#breakingnews3').BreakingNews({
            title: '网站公告',
            titlebgcolor: '#cf0c36',
            linkhovercolor: '#099',
            timer: 2000,
            effect: 'slide'
        });
    });
</script>

<div class="container top30">
    <div class="row">
        <div class="col-md-8">

            <div class="col-md-12" style="margin-bottom: 10px; padding-left: 0px; padding-right: 0px;">
                <div class="demo demo3">
                    <div class="BreakingNewsController easing" id="breakingnews3">
                        <div class="bn-title"></div>
                        <ul>
                        <@bkTag method="notices">
                            <#if notices?? && (notices?size > 0)>
                                <#list notices as item>
                                    <li><a>${item.title}</a></li>
                                </#list>
                            </#if>
                        </@bkTag>
                        </ul>
                        <div class="bn-arrows"><span class="bn-arrows-left"></span><span class="bn-arrows-right"></span></div>
                    </div>
                </div>
            </div>

            <#if page?? && page.list?size gt 0>
                <#list page.list as item>
                    <div class="col-md-12 article-item" style="background: white">
                        <div class="col-md-12 article-header">
                            <h3><a href="/article/${item.id}">${item.title}</a></h3>
                            <span>${item.createTime?string('yyyy-MM-dd hh:mm:ss')}</span>
                        </div>
                        <div class="col-md-12">
                            <img style="width: 100%; height: 250px" src="${item.coverImage}">
                        </div>
                        <div class="col-md-12">
                        <span>
                             <div style="col-md-12; white-space:nowrap;overflow:hidden;text-overflow:ellipsis;">
                                ${item.description}
                             </div>
                        </span>
                        </div>
                        <div class="col-md-12">
                            <span class="badge  badge-secondary"><a class="white" href="/article/${item.id}">阅读全文</a></span>
                        </div>
                        <div class="col-md-12">
                            <hr>
                            <span class="badge badge-light">👓浏览量&nbsp; ${item.lookCount}</span>
                            <span class="badge badge-light"><a class=".special-a black" href="#">💬评论&nbsp;${item.commentCount}</a></span>
                        </div>
                    </div>
                </#list>
            </#if>
            <@pageBar></@pageBar>
        </div>
        <@rightDiv></@rightDiv>
    </div>
</div>


<@footer></@footer>


<script>
    window.prettyPrint && prettyPrint();
    $('.code h3').not(':first').next().addClass('f-dn');
    $('.code h3').click(function(){
        $(this).addClass('cur').siblings('h3').removeClass('cur');
        $(this).next().removeClass('f-dn').siblings('.pre').addClass('f-dn');
    });
</script>
