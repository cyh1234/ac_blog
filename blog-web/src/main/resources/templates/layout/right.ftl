<div class="col-md-4">

    <div class="card border-secondary mb-3" style="max-width: 20rem;">
        <div class="card-header">搜索🔍</div>
        <div class="card-body">
            <div class="input-group">
                <input type="text" name="searchText"  class="form-control" placeholder="搜一搜"/>
                <#--<button type="button" class="btn btn-primary disabled btn-sm">搜索</button>-->
            </div>
            <ul class="list-group" name="searchBox">
                <#--<li class="list-group-item d-flex justify-content-between align-items-center">-->
                    <#--<span>Cras justo odio</span>-->
                    <#--<span class="badge badge-primary badge-pill">14</span>-->
                <#--</li>-->
            </ul>
        </div>
    </div>

    <div class="card border-secondary mb-3" style="max-width: 20rem;">
        <div class="card-header">关于我🙃</div>
        <div class="card-body">
            <div  style="text-align: center">
                <img class="img-circle" style="width: 120px; height: 120px;" src="${config.headImg}">
                <p>${config.authorName}</p>
                <marquee>${config.siteDesc}</marquee>
                <hr>
            </div>
            <@bkTag method="sysInfos">
                <#if sysInfos?? && (sysInfos?size > 0)>
                    <#list sysInfos as item>
                        <span name="tag" class="badge bottom">
                            <i class="${item.infoIcon}"></i>&nbsp;${item.infoKey}&nbsp;:&nbsp; ${item.infoValue}
                        </span>
                    </#list>
                </#if>
            </@bkTag>
        </div>
    </div>

    <div class="card border-secondary mb-3" style="max-width: 20rem;">
        <div class="card-header">文章标签📃</div>
        <div class="card-body">
            <@bkTag method="tags">
                <#if tags?? && (tags?size > 0)>
                    <#list tags as item>
                        <span name="tag" class="badge bottom">
                            <a href="/tag/${item.id}">
                                ${item.name}
                            </a>
                        </span>
                    </#list>
                </#if>
            </@bkTag>
        </div>
    </div>

    <div class="card border-secondary mb-3" style="max-width: 20rem;">
        <div class="card-header">近期评论💬</div>
        <div class="card-body" style="padding: 0rem">
            <ul class="list-group" name="lastComment">
                <@bkTag method="lastComments">
                    <#if lastComments?? && (lastComments?size > 0)>
                        <#list lastComments as item>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <span>
                                    <img src="${item.avatar}" style="width: 25px; height: 25px;">
                                    <a href="/article/${item.sid}">
                                         ${item.nickname}&nbsp;:&nbsp;
                                         ${item.content}
                                    </a>
                                </span>
                            </li>
                        </#list>
                    </#if>
                </@bkTag>
            </ul>
        </div>
    </div>

    <div class="card border-secondary mb-3" style="max-width: 20rem;">
        <div class="card-header">网站信息📻</div>
        <div class="card-body" style="padding: 0rem">
            <@bkTag method="webInfo">
                <#if webInfo??>
                    <ul class="list-group">
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            文章总数
                            <span class="badge badge-primary badge-pill">${webInfo.articleTotal}</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            评论总数
                            <span class="badge badge-primary badge-pill">${webInfo.commentTotal}</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            留言总数
                            <span class="badge badge-primary badge-pill">${webInfo.leavlTotal}</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            文章总浏览量
                            <span class="badge badge-primary badge-pill">${webInfo.lookTotal}</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            本站已经运行了：${webInfo.duration} 天
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            最后更新时间：${webInfo.lastUpdate?string('yyyy-MM-dd hh:mm:ss')}
                        </li>
                    </ul>
                </#if>
            </@bkTag>
        </div>
    </div>
</div>

<#--<script>-->
    <#--var urodz= new Date("5/20/2018");  //建站时间-->
    <#--var now = new Date();-->
    <#--var ile = now.getTime()-urodz.getTime();-->
    <#--var dni = Math.floor(ile / (1000 * 60 * 60 * 24));-->
    <#--alert(dni);-->
<#--</script>-->