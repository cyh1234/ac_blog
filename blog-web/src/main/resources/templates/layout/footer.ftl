<div class="blogfooter" style="background: #202020">
    <div class="container">
        <div class="row">
            <div class="col-4">
                <h5 class="title">友链</h5>
                <ul>
                    <@bkTag method="friendLinks">
                        <#if friendLinks?? && friendLinks?size gt 0>
                            <#list friendLinks as item>
                               <li><a href="${item.url}">${item.name}</a></li>
                            </#list>
                        </#if>
                    </@bkTag>
                </ul>
            </div>

            <#--<div class="col-4">-->
                <#--<h5 class="title">标签</h5>-->
                <#--<ul>-->
                    <#--<li><a href="#">ff</a></li>-->
                    <#--<li>ff</li>-->
                    <#--<li>ff</li>-->
                <#--</ul>-->
            <#--</div>-->

            <#--<div class="col-4">-->
                <#--<h5 class="title">合作</h5>-->
                <#--<ul>-->
                    <#--<li>ff</li>-->
                    <#--<li>ff</li>-->
                    <#--<li>ff</li>-->
                    <#--<li>ff</li>-->
                    <#--<li>ff</li>-->
                <#--</ul>-->
            <#--</div>-->
        </div>
    </div>
</div>

<div class="blogfooter" style="background: #111">
    <div class="container">
        <div style="text-align: center">
            Copyright © 啊程博客 | 皖ICP备
        </div>
    </div>
</div>


<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/4.1.1/js/bootstrap.js"></script>
<script src="/static/js/wangEditor.min.js"></script>
<script src="/static/js/prism.js"></script>
<script src="/static/js/jquery-confirm.min.js"></script>
<#--滚动-->

<script src="http://cdn.dowebok.com/121/BreakingNews.js"></script>


<!--自定-->
<script src="/static/js/blog/right.js"></script>
<script src="/static/js/blog/article.js"></script>
<script src="/static/js/blog/comment.js"></script>
<script src="/static/js/blog/mytools.js"></script>
<script src="/static/js/blog/bulletin.js"></script>
<script src="/static/js/blog/about.js"></script>
