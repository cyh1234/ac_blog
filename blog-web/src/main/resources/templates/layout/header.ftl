

<div class="row top" >
    <div class="col-md-4 header container">
        <h2>${config.authorName}🍊博客</h2>
        <sapn>${config.siteDesc}</sapn>
    </div>
</div>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor03">
        <ul class="navbar-nav mr-auto">
            <li>
                <a  ${(typeId == -1)?string("class='nav-link active'","class='nav-link'")} href="/">主页</a>
            </li>

            <@bkTag method="types">
                <#if types?? && types?size gt 0>
                    <#list types as item>
                        <li ${(typeId == item.id)?string("class='nav-item active'","class='nav-item'")}>
                            <a class="nav-link" href="/type/${item.id}">${item.name}</a>
                        </li>
                    </#list>
                </#if>
            </@bkTag>

            <li>
                <a ${(typeId == -2)?string("class='nav-link active'","class='nav-link'")} href="/about">关于我</a>
            </li>

            <li>
                <a ${(typeId == -3)?string("class='nav-link active'","class='nav-link'")} href="/leave">留言板</a>
            </li>
        </ul>
    </div>
</nav>


