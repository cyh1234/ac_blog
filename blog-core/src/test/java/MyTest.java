import java.util.HashMap;
import java.util.Map;

public class MyTest {
    public static void test(){
        Map<String, Object> map = new HashMap<>();
        map.put("a",12);
        map.put("b",13);
        map.put("c",14);

        Map<String, Object> mapAll = new HashMap<>();
        mapAll.put("a", 90);
        mapAll.putAll(map);
    }

    public static void main(String[] args) {
         test();
    }
}
