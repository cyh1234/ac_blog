package com.cyh.blog.framework.object;

import com.cyh.blog.persistence.beans.SysUser;

import java.util.List;

/**
 * 控制层返回给 bootstrap-tabel 的数据类型
 */
public class PageResult {
    private Long total;
    private List rows;

    public PageResult(long total, List<SysUser> list) {
        this.total = total;
        this.rows = list;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List getRows() {
        return rows;
    }

    public void setRows(List rows) {
        this.rows = rows;
    }
}
