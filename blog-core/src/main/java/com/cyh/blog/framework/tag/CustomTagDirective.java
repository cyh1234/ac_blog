/**
 * MIT License
 *
 * Copyright (c) 2018 yadong.zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.cyh.blog.framework.tag;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.cyh.blog.business.service.*;
import com.cyh.blog.persistence.beans.SysLink;
import com.cyh.blog.persistence.beans.SysNotice;
import com.cyh.blog.persistence.beans.SysResources;
import freemarker.core.Environment;
import freemarker.template.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 自定义标签， 实现TemplateDirectiveModel， 的execute
 */

@Component
public class CustomTagDirective implements TemplateDirectiveModel {

    private static final String METHOD_KEY = "method";

    @Autowired
    private BizTypeService bizTypeService;

    @Autowired
    private BizTagsService bizTagsService;

    @Autowired
    private BizCommentService bizCommentService;

    @Autowired
    private SysConfigService configService;

    @Autowired
    private SysNoticeService sysNoticeService;

    @Autowired
    private SysLinkService sysLinkService;

    @Autowired
    private SysInfoService sysInfoService;

    @Autowired
    private SysResourcesService sysResourcesService;



    @Override
    public void execute(Environment environment, Map map, TemplateModel[] templateModels, TemplateDirectiveBody templateDirectiveBody) throws TemplateException, IOException {
        DefaultObjectWrapperBuilder builder = new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_25);
        if (map.containsKey(METHOD_KEY)) {
            String method = map.get(METHOD_KEY).toString(); //页面method=“types” ，取出的是types
//            int pageSize = 10;
//            if (map.containsKey("pageSize")) {
//                String pageSizeStr = map.get("pageSize").toString();
//                pageSize = Integer.parseInt(pageSizeStr);
//            }
            switch (method) {
                /**
                 * 前台的
                 */
                case "types": //所有类别
                    environment.setVariable("types", builder.build().wrap(bizTypeService.selectList(new EntityWrapper<>())));
                    break;
                case "tags": //所有标签
                    environment.setVariable("tags", builder.build().wrap(bizTagsService.selectList(new EntityWrapper<>())));
                    break;
                case "comments":
                    environment.setVariable("comments", builder.build().wrap(bizCommentService.selectByNotApproved()));
                    break;
                case "lastComments": //最近评论
                    environment.setVariable("lastComments", builder.build().wrap(
                            bizCommentService.selectByLast(10)));
                    break;
                case "sysInfos":
                    environment.setVariable("sysInfos", builder.build().wrap(sysInfoService.selectList(null)));
                    break;
                case "webInfo":
                    environment.setVariable("webInfo", builder.build().wrap(configService.selectWebInfo()));
                    break;
                case "notices": //公告
                    environment.setVariable("notices", builder.build().wrap(sysNoticeService.selectList(
                            new EntityWrapper<SysNotice>().eq("status", "RELEASE")
                    )));
                    break;
                case "friendLinks": //友鏈
                    environment.setVariable("friendLinks", builder.build().wrap(sysLinkService.selectList(
                            new EntityWrapper<SysLink>().eq("status", 1)
                    )));
                    break;
                /**
                 * 后台的
                 */
                case "parentResources": //所有父资源
                    environment.setVariable("parentResources", builder.build().wrap(sysResourcesService.selectList(
                            new EntityWrapper<SysResources>().eq("parent_id", 0)
                    )));
                    break;
                case "menus": //菜单
                    if (StringUtils.isEmpty(map.get("userId").toString())){
                        return ;
                    }
                    System.out.println(map.get("userId"));
                    Integer userId = Integer.parseInt(map.get("userId").toString());
                    Map<String, Object> m = new HashMap<>();
                    m.put("type", "menu");
                    m.put("userId", userId);
                    environment.setVariable("menus", builder.build().wrap(
                            sysResourcesService.selectUserResources(m)
                    ));
                    break;
                default:
                    break;
            }
        }
        templateDirectiveBody.render(environment.getOut());
    }
}
