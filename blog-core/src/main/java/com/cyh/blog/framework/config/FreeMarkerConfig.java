package com.cyh.blog.framework.config;

import com.cyh.blog.business.service.SysConfigService;
import com.cyh.blog.framework.tag.CustomTagDirective;
import freemarker.template.TemplateModelException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class FreeMarkerConfig {
    @Autowired
    private freemarker.template.Configuration configuration;

    @Autowired
    private SysConfigService sysConfigService;

    @Autowired
    private CustomTagDirective customTagDirective;

    /**
     * 1. 一个类中只有一个函数可以被@PostConstruct修饰。以为当修饰多个时，无法判断先执行哪个。
     * 2. 修饰的函数的返回类型必须为void。是因为后续没有操作
     *  设置给模板使用的共享变量
     */
    @PostConstruct
    public void setSharedVariable(){
        try {
            configuration.setSharedVariable("bkTag",customTagDirective);
            //网站设置信息
            configuration.setSharedVariable("config", sysConfigService.selectOne(null));
            //shiro标签
        } catch (TemplateModelException e) {
            e.printStackTrace();
        }
    }

}
