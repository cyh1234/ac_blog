package com.cyh.blog.business.service.impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cyh.blog.business.service.BizTagsService;
import com.cyh.blog.business.vo.TagsVo;
import com.cyh.blog.persistence.beans.BizTags;
import com.cyh.blog.persistence.mapper.BizTagsDao;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cyh
 * @since 2018-05-16
 */
@Service
public class BizTagsServiceImpl extends ServiceImpl<BizTagsDao, BizTags> implements BizTagsService {

    @Autowired
    private BizTagsDao bizTagsDao;

    @Override
    public PageInfo<BizTags> selectByCondition(TagsVo tagsVo) {
        PageHelper.startPage(tagsVo.getPageNumber(), tagsVo.getPageSize());
        List<BizTags> bizTags = bizTagsDao.selectList(
                new EntityWrapper<BizTags>().orderBy("id",true));
        PageInfo pageInfo = new PageInfo(bizTags);
        return pageInfo;
    }
}
