package com.cyh.blog.business.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cyh.blog.business.service.BizArticleTagsService;
import com.cyh.blog.persistence.beans.BizArticleTags;
import com.cyh.blog.persistence.mapper.BizArticleTagsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cyh
 * @since 2018-05-16
 */
@Service
public class BizArticleTagsServiceImpl extends ServiceImpl<BizArticleTagsDao, BizArticleTags> implements BizArticleTagsService {

    @Autowired
    private BizArticleTagsDao bizArticleTagsDao;


    @Override
    public void insertByIds(Integer aid, Integer[] tags) {
        List<BizArticleTags> list = new ArrayList<>();
        for (int i=0; i<tags.length; i++){
            BizArticleTags articleTags = new BizArticleTags();
            articleTags.setTagId(tags[i]);
            articleTags.setArticleId(aid);
            list.add(articleTags);
        }
        this.insertBatch(list);
    }
}
