package com.cyh.blog.business.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cyh.blog.business.service.SysResourcesService;
import com.cyh.blog.business.vo.ResourcesVo;
import com.cyh.blog.persistence.beans.BizTags;
import com.cyh.blog.persistence.beans.SysResources;
import com.cyh.blog.persistence.mapper.SysResourcesDao;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.omg.CORBA.ObjectHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
@Service
public class SysResourcesServiceImpl extends ServiceImpl<SysResourcesDao, SysResources> implements SysResourcesService {

    @Autowired
    private SysResourcesDao sysResourcesDao;

    @Override
    public PageInfo<SysResources> selectByCondition(ResourcesVo resourcesVo) {
        PageHelper.startPage(resourcesVo.getPageNumber(), resourcesVo.getPageSize());
        List<SysResources> sysResources = sysResourcesDao.selectByCondition(resourcesVo);
        PageInfo<SysResources> pageInfo = new PageInfo<>(sysResources);
        return pageInfo;
    }

    /**
     * 查询所有资源,并根据角色选中资源
     *
     * @param roleId
     * @return
     */
    @Override
    public List<Map<String, Object>> selectResourcesSelected(Integer roleId) {
        List<SysResources>  list = sysResourcesDao.selectResourcesSelected(roleId);
        //将list转换成zTree需要的格式 { id:121, pId:12, name:"随意勾选 1-2-1", checked:true},
        List<Map<String, Object>> mList = new ArrayList<>();
        Map<String, Object> map = null;
        for (SysResources r : list){
            map = new HashMap<>();
            map.put("id",r.getId());
            map.put("pId", r.getParentId());
            map.put("checked",r.isSelect());
            map.put("name", r.getName());
            mList.add(map);
        }
        return mList;
    }

    /**
     * 查询用户拥有的资源列表
     * @param m userId,  type（菜单，按钮）
     * @return
     */
    @Override
    public List<SysResources> selectUserResources(Map<String, Object> m) {
        List<SysResources> sysResources = sysResourcesDao.selectUserResources(m);
        return sysResources;
    }
}
