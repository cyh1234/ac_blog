package com.cyh.blog.business.service;


import com.baomidou.mybatisplus.service.IService;
import com.cyh.blog.persistence.beans.SysConfig;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-05-15
 */
public interface SysConfigService extends IService<SysConfig> {
    public Map<String, Object> selectWebInfo();
}
