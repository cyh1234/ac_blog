package com.cyh.blog.business.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cyh.blog.business.service.SysInfoService;
import com.cyh.blog.business.vo.InfoVo;
import com.cyh.blog.exception.CustomException;
import com.cyh.blog.persistence.beans.SysInfo;
import com.cyh.blog.persistence.mapper.SysInfoDao;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
@Service
public class SysInfoServiceImpl extends ServiceImpl<SysInfoDao, SysInfo> implements SysInfoService {


    @Autowired
    private SysInfoDao sysInfoDao;

    /**
     * 分页查询
     *
     * @param infoVo
     * @return
     */
    @Override
    public PageInfo<SysInfo> selectByCondition(InfoVo infoVo) {
        PageHelper.startPage(infoVo.getPageNumber(), infoVo.getPageSize());
        List<SysInfo> notices = sysInfoDao.selectList(null);
        PageInfo pageInfo = new PageInfo(notices);
        return pageInfo;
    }
}