package com.cyh.blog.business.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cyh.blog.business.service.SysRoleResourcesService;
import com.cyh.blog.persistence.beans.SysRoleResources;
import com.cyh.blog.persistence.mapper.SysRoleResourcesDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
@Service
public class SysRoleResourcesServiceImpl extends ServiceImpl<SysRoleResourcesDao, SysRoleResources> implements SysRoleResourcesService {

    @Autowired
    private SysRoleResourcesDao sysRoleResourcesDao;

    @Override
    public void saveRoleResource(String ids, Integer roleId) {
        removeRoleResource(roleId);
        if (!StringUtils.isEmpty(ids)){
            String[] split = ids.split(",");
            List<SysRoleResources> list = new ArrayList<>();
            SysRoleResources s;
            for (String id : split){
                s = new SysRoleResources();
                s.setResourcesId(Integer.parseInt(id));
                s.setRoleId(roleId);
                list.add(s);
            }
            sysRoleResourcesDao.insertList(list);
        }
    }

    @Override
    public void removeRoleResource(Integer roleId) {
        sysRoleResourcesDao.delete(new EntityWrapper<SysRoleResources>().eq("role_id", roleId));
    }
}
