package com.cyh.blog.business.vo;

import com.cyh.blog.framework.object.BaseVo;

public class NoticeVo extends BaseVo{
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}