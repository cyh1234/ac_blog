package com.cyh.blog.business.service;


import com.baomidou.mybatisplus.service.IService;
import com.cyh.blog.persistence.beans.BizArticleLove;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-05-16
 */
public interface BizArticleLoveService extends IService<BizArticleLove> {

}
