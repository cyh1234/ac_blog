package com.cyh.blog.business.enums;

/**
 * 导航标识
 */
public enum NavEnums {
    HOME(-1), ABOUT(-2), LEAVE(-3);
    private Integer typeId;

    NavEnums(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getTypeId() {
        return typeId;
    }
}
