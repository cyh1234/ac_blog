
package com.cyh.blog.business.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cyh.blog.business.service.SysNoticeService;
import com.cyh.blog.business.vo.NoticeVo;
import com.cyh.blog.persistence.beans.BizTags;
import com.cyh.blog.persistence.beans.SysNotice;
import com.cyh.blog.persistence.mapper.SysNoticeDao;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
@Service
public class SysNoticeServiceImpl extends ServiceImpl<SysNoticeDao, SysNotice> implements SysNoticeService {

    @Autowired
    private SysNoticeDao sysNoticeDao;

    @Override
    public PageInfo<SysNotice> selectByCondition(NoticeVo noticeVo) {
        PageHelper.startPage(noticeVo.getPageNumber(), noticeVo.getPageSize());
        List<SysNotice> notices = sysNoticeDao.selectByCondition(noticeVo);
        PageInfo pageInfo = new PageInfo(notices);
        return pageInfo;
    }
}
