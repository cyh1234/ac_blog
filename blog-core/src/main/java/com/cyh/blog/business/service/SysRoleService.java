package com.cyh.blog.business.service;

import com.baomidou.mybatisplus.service.IService;
import com.cyh.blog.business.vo.RoleVo;
import com.cyh.blog.persistence.beans.SysRole;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
public interface SysRoleService extends IService<SysRole> {

    PageInfo<SysRole> selectByCondition(RoleVo roleVo);

    /**
     * 根据所有角色， 并将该用户的角色设为选中.
     * @param roleId
     * @return
     */
    List<Map<String,Object>> selectRoleSelected(Integer roleId);
}
