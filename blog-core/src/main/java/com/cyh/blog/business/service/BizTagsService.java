package com.cyh.blog.business.service;

import com.baomidou.mybatisplus.service.IService;
import com.cyh.blog.business.vo.TagsVo;
import com.cyh.blog.persistence.beans.BizTags;
import com.github.pagehelper.PageInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-05-16
 */
public interface BizTagsService extends IService<BizTags> {

    PageInfo<BizTags> selectByCondition(TagsVo tagsVo);
}
