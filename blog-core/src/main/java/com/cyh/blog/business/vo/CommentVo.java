package com.cyh.blog.business.vo;

import com.cyh.blog.framework.object.BaseVo;

/**
 * 状态和那一篇文章
 */
public class CommentVo extends BaseVo{
    private String status;
    private Integer sid;
    private Integer pid;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }
}