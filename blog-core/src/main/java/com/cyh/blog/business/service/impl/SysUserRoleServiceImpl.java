package com.cyh.blog.business.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cyh.blog.business.service.SysUserRoleService;
import com.cyh.blog.persistence.beans.SysUserRole;
import com.cyh.blog.persistence.mapper.SysUserRoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cyh
 * @since 2018-06-04
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleDao, SysUserRole> implements SysUserRoleService {

    @Autowired
    private SysUserRoleDao sysUserRoleDao;

    /**
     * 清空用户角色
     *
     * @param userId
     */
    @Override
    public void removeByUserId(Integer userId) {
        sysUserRoleDao.delete(new EntityWrapper<SysUserRole>().eq(
                "user_id", userId
        ));
    }

    /**
     * 保存用户对应的角色
     *
     * @param userId
     * @param roleId
     */
    @Override
    public void saveUserRole(Integer userId, Integer roleId) {
        this.removeByUserId(userId);
        SysUserRole sysUserRole = new SysUserRole();
        sysUserRole.setUserId(userId);
        sysUserRole.setRoleId(roleId);
        sysUserRoleDao.insert(sysUserRole);
    }
}
