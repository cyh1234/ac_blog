package com.cyh.blog.business.service;


import com.baomidou.mybatisplus.service.IService;
import com.cyh.blog.persistence.beans.SysUserRole;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-06-04
 */
public interface SysUserRoleService extends IService<SysUserRole> {

    /**
     * 清空用户角色
     * @param userId
     */
    void removeByUserId(Integer userId);

    /**
     * 保存用户对应的角色
     * @param userId
     * @param roleId
     */
    void saveUserRole(Integer userId, Integer roleId);
}
