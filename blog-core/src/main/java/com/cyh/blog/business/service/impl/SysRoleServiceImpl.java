package com.cyh.blog.business.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cyh.blog.business.service.SysRoleService;
import com.cyh.blog.business.vo.RoleVo;
import com.cyh.blog.persistence.beans.SysRole;
import com.cyh.blog.persistence.mapper.SysRoleDao;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleDao, SysRole> implements SysRoleService {

    @Autowired
    private SysRoleDao sysRoleDao;

    @Override
    public PageInfo<SysRole> selectByCondition(RoleVo roleVo) {
        PageHelper.startPage(roleVo.getPageNumber(), roleVo.getPageSize());
        List<SysRole> sysRoles = sysRoleDao.selectList(
                new EntityWrapper<SysRole>().orderBy("id",true));
        PageInfo pageInfo = new PageInfo(sysRoles);
        return pageInfo;
    }

    /**
     * 根据所有角色， 并将该用户的角色设为选中.
     * @param userId
     * @return
     */
    @Override
    public List<Map<String, Object>> selectRoleSelected(Integer userId) {
        List<SysRole> list = sysRoleDao.selectRoleSelected(userId);
        //将list转换成zTree需要的格式 { id:121, pId:12, name:"随意勾选 1-2-1", checked:true},
        if (CollectionUtils.isEmpty(list)){
            return null;
        }
        List<Map<String, Object>> mList = new ArrayList<>();
        Map<String, Object> map = null;
        for (SysRole r : list){
            map = new HashMap<>();
            map.put("id",r.getId());
            map.put("pId", 0);
            map.put("name",r.getDescription());
            map.put("checked", r.isSelect());
            mList.add(map);
        }
        return mList;
    }
}
