package com.cyh.blog.business.service.impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cyh.blog.business.service.BizCommentService;
import com.cyh.blog.business.vo.CommentVo;
import com.cyh.blog.business.vo.QQInfo;
import com.cyh.blog.exception.CustomException;
import com.cyh.blog.persistence.beans.BizArticleLove;
import com.cyh.blog.persistence.beans.BizComment;
import com.cyh.blog.persistence.mapper.BizCommentDao;
import com.cyh.blog.util.ToolUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cyh
 * @since 2018-05-16
 */
@Service
public class BizCommentServiceImpl extends ServiceImpl<BizCommentDao, BizComment> implements BizCommentService {

    @Autowired
    private BizCommentDao bizCommentDao;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    private final static String TAG = "commentId";

    @Override
    public PageInfo<BizComment> selectByCondition(CommentVo commentVo) {
        PageHelper.startPage(commentVo.getPageNumber(), commentVo.getPageSize());
        List<BizComment> bizComments = bizCommentDao.selectByCondition(commentVo);
        PageInfo pageInfo = new PageInfo(bizComments);
        return pageInfo;
    }

    /**
     * 用户评论
     * @param qqInfo
     * @param commentVo
     */
    @Override
    public void insertComment(QQInfo qqInfo, CommentVo commentVo) {
            BizComment bizComment = new BizComment();
        if (commentVo.getPid() != null){
            bizComment.setPid(commentVo.getPid());
        }
        if (commentVo.getSid() != null){
            bizComment.setSid(commentVo.getSid());
        }
        bizComment.setEmail(qqInfo.getEmail());
        bizComment.setCreateTime(new Date());
        bizComment.setStatus("VERIFYING");
        bizComment.setNickname(qqInfo.getNikeName());
        bizComment.setQq(qqInfo.getNumber());
        bizComment.setContent(qqInfo.getContent());
        bizComment.setAvatar(qqInfo.getHeadImg());
        bizComment.setUrl(qqInfo.getZone());
        bizCommentDao.insert(bizComment);
    }

    @Override
    public List<BizComment> selectByNotApproved() {
        List<BizComment> bizComments = bizCommentDao.selectList(
                new EntityWrapper<BizComment>().
                        eq("status", "VERIFYING").
                        orderBy("create_time", false));
        return bizComments;
    }

    /**
     * 用户给评论点赞
     * @param request
     */
    @Override
    public void support(HttpServletRequest request) throws CustomException {
        String commentId = request.getParameter(TAG);
        String ip = ToolUtil.getClientIp(request);
        String key = ip+TAG+"support"+commentId;
        if (redisTemplate.hasKey(key)){
            throw new CustomException("一小时内只能赞一次哈");
        }
        ValueOperations<String, Object> ops= redisTemplate.opsForValue();
        ops.set(key, commentId, 1, TimeUnit.HOURS);
        Integer id = Integer.parseInt(commentId);
        bizCommentDao.updateSupport(id);
    }

    /**
     * 评论踩
     * @param request
     */
    @Override
    public void oppose(HttpServletRequest request) throws CustomException {
        String commentId = request.getParameter(TAG);
        String ip = ToolUtil.getClientIp(request);
        String key = ip+TAG+"oppose"+commentId;
        if (redisTemplate.hasKey(key)){
            throw new CustomException("一小时内只能踩一次哈，又没有什么深仇大恨");
        }
        ValueOperations<String, Object> ops = redisTemplate.opsForValue();
        ops.set(key, commentId, 1, TimeUnit.HOURS);
        Integer id = Integer.parseInt(commentId);
        bizCommentDao.updateOppose(id);
    }

    /**
     * 最近评论
     *
     * @param number@return
     */
    @Override
    public List<BizComment> selectByLast(Integer number) {
        return bizCommentDao.selectByLast();
    }
}
