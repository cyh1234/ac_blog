package com.cyh.blog.business.service;

import com.baomidou.mybatisplus.service.IService;
import com.cyh.blog.business.vo.UserVo;
import com.cyh.blog.persistence.beans.SysUser;
import com.github.pagehelper.PageInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-05-15
 */
public interface SysUserService extends IService<SysUser> {
    /**
     * 用户分页
     * @param userVo
     * @return
     */
   PageInfo<SysUser> selectByCondition(UserVo userVo);
}
