package com.cyh.blog.business.service;


import com.baomidou.mybatisplus.service.IService;
import com.cyh.blog.persistence.beans.BizArticleTags;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-05-16
 */
public interface BizArticleTagsService extends IService<BizArticleTags> {

    void insertByIds(Integer aid, Integer[] ts);

}
