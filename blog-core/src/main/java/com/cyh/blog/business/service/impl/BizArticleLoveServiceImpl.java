package com.cyh.blog.business.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cyh.blog.business.service.BizArticleLoveService;
import com.cyh.blog.persistence.beans.BizArticleLove;
import com.cyh.blog.persistence.mapper.BizArticleLoveDao;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cyh
 * @since 2018-05-16
 */
@Service
public class BizArticleLoveServiceImpl extends ServiceImpl<BizArticleLoveDao, BizArticleLove> implements BizArticleLoveService {

}
