package com.cyh.blog.business.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cyh.blog.business.service.SysConfigService;
import com.cyh.blog.persistence.beans.SysConfig;
import com.cyh.blog.persistence.mapper.SysConfigDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cyh
 * @since 2018-05-15
 */
@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigDao, SysConfig> implements SysConfigService {

    @Autowired
    private SysConfigDao sysConfigDao;

    @Override
    public Map<String, Object> selectWebInfo() {
        Map<String, Object> map = sysConfigDao.selectWebInfo();
        long create_time = ((Date) map.get("create_time")).getTime();
        long duration = new Date().getTime() - create_time;
        int day = (int) (duration/1000/3600/24);
        map.put("duration", day);
        return map;
    }
}
