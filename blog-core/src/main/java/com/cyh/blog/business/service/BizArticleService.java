package com.cyh.blog.business.service;

import com.baomidou.mybatisplus.service.IService;
import com.cyh.blog.business.vo.ArticleVo;
import com.cyh.blog.exception.CustomException;
import com.cyh.blog.persistence.beans.BizArticle;
import com.github.pagehelper.PageInfo;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-05-16
 */
public interface BizArticleService extends IService<BizArticle> {

    PageInfo<BizArticle> selectByCondition(ArticleVo articleVo);

    BizArticle selectByKey(Integer id);

    void insertBean(BizArticle bizArticle);

    Integer insertOrUpdateBean(ArticleVo article);

    /**
     * 获取上下文
     * @return
     * @param createTime
     */
    Map<String, BizArticle> getPreAndNext(Date createTime);

    /**
     * 相关文章, 根据本文章的同类别，同标签获取
     * @param bizArticle
     * @return
     */
    List<BizArticle> getRelatedArticle(BizArticle bizArticle, Integer pageSize);

    /**
     * 获取热门文章
     * @param hot_related_size
     * @return
     */
    List<BizArticle> getHotArticle(Integer hot_related_size);

    /**
     * 用户对文章点赞
     * @param request
     */
    void parise(HttpServletRequest request) throws CustomException;

    /**
     * 文章搜索
     * @param content
     */
    List<BizArticle> search(String content);

    /**
     * 文章删除
     * @param id
     */
    void removeArticleById(Integer id);
}
