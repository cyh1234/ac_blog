package com.cyh.blog.business.service;


import com.baomidou.mybatisplus.service.IService;
import com.cyh.blog.persistence.beans.BizArticleLook;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-06-04
 */
public interface BizArticleLookService extends IService<BizArticleLook> {

}
