package com.cyh.blog.business.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cyh.blog.business.service.BizArticleLookService;
import com.cyh.blog.persistence.beans.BizArticleLook;
import com.cyh.blog.persistence.mapper.BizArticleLookDao;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cyh
 * @since 2018-06-04
 */
@Service
public class BizArticleLookServiceImpl extends ServiceImpl<BizArticleLookDao, BizArticleLook> implements BizArticleLookService {

}
