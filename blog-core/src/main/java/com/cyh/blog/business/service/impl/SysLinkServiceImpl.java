package com.cyh.blog.business.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cyh.blog.business.service.SysLinkService;
import com.cyh.blog.business.vo.LinkVo;
import com.cyh.blog.persistence.beans.SysLink;
import com.cyh.blog.persistence.beans.SysLink;
import com.cyh.blog.persistence.mapper.SysLinkDao;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
@Service
public class SysLinkServiceImpl extends ServiceImpl<SysLinkDao, SysLink> implements SysLinkService {

    @Autowired
    private SysLinkDao sysLinkDao;

    @Override
    public PageInfo<SysLink> selectByCondition(LinkVo linkVo) {
        PageHelper.startPage(linkVo.getPageNumber(), linkVo.getPageSize());
        List<SysLink> notices = sysLinkDao.selectByCondition(linkVo);
        PageInfo pageInfo = new PageInfo(notices);
        return pageInfo;
    }
}
