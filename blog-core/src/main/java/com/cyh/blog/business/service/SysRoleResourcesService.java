package com.cyh.blog.business.service;


import com.baomidou.mybatisplus.service.IService;
import com.cyh.blog.persistence.beans.SysRoleResources;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
public interface SysRoleResourcesService extends IService<SysRoleResources> {

    void saveRoleResource(String ids,Integer roleId);

    void removeRoleResource(Integer roleId);
}
