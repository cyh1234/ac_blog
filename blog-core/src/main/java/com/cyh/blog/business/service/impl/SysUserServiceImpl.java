package com.cyh.blog.business.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cyh.blog.business.service.SysUserService;
import com.cyh.blog.business.vo.UserVo;
import com.cyh.blog.persistence.beans.SysUser;
import com.cyh.blog.persistence.mapper.SysUserDao;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cyh
 * @since 2018-05-15
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserDao, SysUser> implements SysUserService {

    @Autowired
    private  SysUserDao sysUserDao;

    /**
     * 用户分页
     * @param userVo
     * @return
     */
    @Override
    public PageInfo<SysUser> selectByCondition(UserVo userVo) {
        PageHelper.startPage(userVo.getPageNumber(), userVo.getPageSize());
        List<SysUser> sysUsers = sysUserDao.selectAll();
        PageInfo pageInfo = new PageInfo(sysUsers);
        return pageInfo;
    }
}
