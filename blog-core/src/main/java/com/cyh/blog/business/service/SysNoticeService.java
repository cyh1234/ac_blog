package com.cyh.blog.business.service;

import com.baomidou.mybatisplus.service.IService;
import com.cyh.blog.business.vo.NoticeVo;
import com.cyh.blog.persistence.beans.SysNotice;
import com.github.pagehelper.PageInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
public interface SysNoticeService extends IService<SysNotice> {

    PageInfo<SysNotice> selectByCondition(NoticeVo noticeVo);
}
