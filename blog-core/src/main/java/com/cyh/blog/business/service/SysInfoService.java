package com.cyh.blog.business.service;

import com.baomidou.mybatisplus.service.IService;
import com.cyh.blog.business.vo.InfoVo;
import com.cyh.blog.exception.CustomException;
import com.cyh.blog.persistence.beans.SysInfo;
import com.github.pagehelper.PageInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
public interface SysInfoService extends IService<SysInfo> {

    /**
     * 分页查询
     * @param infoVo
     * @return
     */
    PageInfo<SysInfo> selectByCondition(InfoVo infoVo);
}
