package com.cyh.blog.business.service;


import com.baomidou.mybatisplus.service.IService;
import com.cyh.blog.business.vo.CommentVo;
import com.cyh.blog.business.vo.QQInfo;
import com.cyh.blog.exception.CustomException;
import com.cyh.blog.persistence.beans.BizComment;
import com.github.pagehelper.PageInfo;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-05-16
 */
public interface BizCommentService extends IService<BizComment> {

    PageInfo<BizComment> selectByCondition(CommentVo commentVo);

    void insertComment(QQInfo qqInfo, CommentVo commentVo);

    List<BizComment> selectByNotApproved();

    /*
    评论点赞
     */
    void support(HttpServletRequest request) throws CustomException;

    /**
     * 评论踩
     * @param request
     */
    void oppose(HttpServletRequest request) throws CustomException;

    /**
     * 最近评论
     * @param i
     * @return
     */
    List<BizComment> selectByLast(Integer number);
}
