package com.cyh.blog.business.enums;

public enum ArticleStatusEnum {
    PUBLISHED("发布",1),
    UNPUBLISHED("草稿",0);
    private String desc;
    private Integer code;

    ArticleStatusEnum(String desc, Integer code) {
        this.desc = desc;
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getCode() {
        return code;
    }
}