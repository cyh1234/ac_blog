package com.cyh.blog.business.service;


import com.baomidou.mybatisplus.service.IService;
import com.cyh.blog.business.vo.TypeVo;
import com.cyh.blog.persistence.beans.BizType;
import com.github.pagehelper.PageInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-05-16
 */
public interface BizTypeService extends IService<BizType> {

    PageInfo<BizType> selectByCondition(TypeVo typeVo);
}
