package com.cyh.blog.business.service;

import com.baomidou.mybatisplus.service.IService;
import com.cyh.blog.business.vo.ResourcesVo;
import com.cyh.blog.persistence.beans.SysResources;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
public interface SysResourcesService extends IService<SysResources> {

    PageInfo<SysResources> selectByCondition(ResourcesVo resourcesVo);

    /**
     * 查询所有资源,并根据角色选中资源
     * @param roleId
     * @return
     */
    List<Map<String, Object>> selectResourcesSelected(Integer roleId);

    /**
     * 查询用户拥有的资源列表
     * @param m  userID,  type（菜单，按钮）
     * @return
     */
    List<SysResources> selectUserResources(Map<String, Object> m);
}
