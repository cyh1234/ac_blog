package com.cyh.blog.business.service;


import com.baomidou.mybatisplus.service.IService;
import com.cyh.blog.business.vo.LinkVo;
import com.cyh.blog.persistence.beans.SysLink;
import com.github.pagehelper.PageInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
public interface SysLinkService extends IService<SysLink> {

    PageInfo<SysLink> selectByCondition(LinkVo noticeVo);
}
