package com.cyh.blog.business.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cyh.blog.business.service.BizTypeService;
import com.cyh.blog.business.vo.TypeVo;
import com.cyh.blog.persistence.beans.BizType;
import com.cyh.blog.persistence.beans.SysUser;
import com.cyh.blog.persistence.mapper.BizTypeDao;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cyh
 * @since 2018-05-16
 */
@Service
public class BizTypeServiceImpl extends ServiceImpl<BizTypeDao, BizType> implements BizTypeService {
    @Autowired
    private  BizTypeDao bizTypeDao;

    @Override
    public PageInfo<BizType> selectByCondition(TypeVo typeVo) {
        PageHelper.startPage(typeVo.getPageNumber(), typeVo.getPageSize());
        List<BizType> bizTypes = bizTypeDao.selectList(
                new EntityWrapper<BizType>().orderBy("sort",true));
        PageInfo pageInfo = new PageInfo(bizTypes);
        return pageInfo;
    }
}
