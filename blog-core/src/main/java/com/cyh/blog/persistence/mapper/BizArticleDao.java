package com.cyh.blog.persistence.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.cyh.blog.business.vo.ArticleVo;
import com.cyh.blog.persistence.beans.BizArticle;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cyh
 * @since 2018-05-16
 */
public interface BizArticleDao extends BaseMapper<BizArticle> {

    List<BizArticle> selectByCondition(ArticleVo articleVo);

    List<BizArticle> selectArticleTagsByArticleId(List<Integer> ids);

    List<BizArticle> selectArticleByTags(@Param("list")List<Integer> ids, @Param("vo")ArticleVo articleVo);

    BizArticle selectByKey(Integer id);

    void insertBean(BizArticle bizArticle);

    List<BizArticle> selectPreAndNext(Date time);

    List<BizArticle> selectHotArticle();

    List<BizArticle> selectByTitle(String content);
}
