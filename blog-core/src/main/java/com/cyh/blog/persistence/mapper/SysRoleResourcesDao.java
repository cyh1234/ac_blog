package com.cyh.blog.persistence.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.cyh.blog.persistence.beans.SysRoleResources;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
public interface SysRoleResourcesDao extends BaseMapper<SysRoleResources> {

    void insertList(List<SysRoleResources> list);
}
