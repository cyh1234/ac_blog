package com.cyh.blog.persistence.beans;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author cyh
 * @since 2018-05-16
 */
@TableName("biz_comment")
public class BizComment extends Model<BizComment> {

    private static final long serialVersionUID = 1L;

    //非数据库字段
    @TableField(exist = false)
    private BizComment parent ; //父评论
    @TableField(exist = false)
    private BizArticle bizArticle; //评论的文章



    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 被评论的文章或者页面的ID
     */
    private Integer sid;
    /**
     * 评论人的ID
     */
    @TableField("user_id")
    private Integer userId;
    /**
     * 父级评论的id
     */
    private Integer pid;
    /**
     * 评论人的QQ（未登录用户）
     */
    private String qq;
    /**
     * 评论人的昵称（未登录用户）
     */
    private String nickname;
    /**
     * 评论人的头像地址
     */
    private String avatar;
    /**
     * 评论人的邮箱地址（未登录用户）
     */
    private String email;
    /**
     * 评论人的网站地址（未登录用户）
     */
    private String url;
    /**
     * 评论的状态
     */
    private String status;
    /**
     * 评论时的ip
     */
    private String ip;
    /**
     * 经度
     */
    private String lng;
    /**
     * 纬度
     */
    private String lat;
    /**
     * 评论时的地址
     */
    private String address;
    /**
     * 评论时的系统类型
     */
    private String os;
    /**
     * 评论时的系统的简称
     */
    @TableField("os_short_name")
    private String osShortName;
    /**
     * 评论时的浏览器类型
     */
    private String browser;
    /**
     * 评论时的浏览器的简称
     */
    @TableField("browser_short_name")
    private String browserShortName;
    /**
     * 评论的内容
     */
    private String content;
    /**
     * 备注（审核不通过时添加）
     */
    private String remark;
    /**
     * 支持（赞）
     */
    private Integer support;
    /**
     * 反对（踩）
     */
    private Integer oppose;
    /**
     * 添加时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;


    public BizComment getParent() {
        return parent;
    }

    public void setParent(BizComment parent) {
        this.parent = parent;
    }

    public BizArticle getBizArticle() {
        return bizArticle;
    }

    public void setBizArticle(BizArticle bizArticle) {
        this.bizArticle = bizArticle;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getOsShortName() {
        return osShortName;
    }

    public void setOsShortName(String osShortName) {
        this.osShortName = osShortName;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getBrowserShortName() {
        return browserShortName;
    }

    public void setBrowserShortName(String browserShortName) {
        this.browserShortName = browserShortName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getSupport() {
        return support;
    }

    public void setSupport(Integer support) {
        this.support = support;
    }

    public Integer getOppose() {
        return oppose;
    }

    public void setOppose(Integer oppose) {
        this.oppose = oppose;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BizComment{" +
        ", id=" + id +
        ", sid=" + sid +
        ", userId=" + userId +
        ", pid=" + pid +
        ", qq=" + qq +
        ", nickname=" + nickname +
        ", avatar=" + avatar +
        ", email=" + email +
        ", url=" + url +
        ", status=" + status +
        ", ip=" + ip +
        ", lng=" + lng +
        ", lat=" + lat +
        ", address=" + address +
        ", os=" + os +
        ", osShortName=" + osShortName +
        ", browser=" + browser +
        ", browserShortName=" + browserShortName +
        ", content=" + content +
        ", remark=" + remark +
        ", support=" + support +
        ", oppose=" + oppose +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
