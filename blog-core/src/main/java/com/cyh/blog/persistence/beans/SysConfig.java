package com.cyh.blog.persistence.beans;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
@TableName("sys_config")
public class SysConfig extends Model<SysConfig> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 首页描述
     */
    @TableField("home_desc")
    private String homeDesc;
    /**
     * 首页关键字
     */
    @TableField("home_keywords")
    private String homeKeywords;
    /**
     * 资料头像
     */
    @TableField("head_img")
    private String headImg;
    /**
     * 根域名
     */
    private String domain;
    /**
     * 网站地址
     */
    @TableField("site_url")
    private String siteUrl;
    /**
     * 站点名称
     */
    @TableField("site_name")
    private String siteName;
    /**
     * 站点描述
     */
    @TableField("site_desc")
    private String siteDesc;
    /**
     * 站点LOGO
     */
    @TableField("site_favicon")
    private String siteFavicon;
    /**
     * 资源文件（js、css等的路径）
     */
    @TableField("static_web_site")
    private String staticWebSite;
    /**
     * 站长名称
     */
    @TableField("author_name")
    private String authorName;
    /**
     * 站长邮箱
     */
    @TableField("author_email")
    private String authorEmail;
    /**
     * QQ
     */
    private String qq;
    /**
     * 是否开启维护通知
     */
    private Integer maintenance;
    /**
     * 系统维护时间
     */
    @TableField("maintenance_data")
    private Date maintenanceData;
    /**
     * 七牛路径
     */
    @TableField("qiuniu_base_path")
    private String qiuniuBasePath;
    /**
     * 添加时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHomeDesc() {
        return homeDesc;
    }

    public void setHomeDesc(String homeDesc) {
        this.homeDesc = homeDesc;
    }

    public String getHomeKeywords() {
        return homeKeywords;
    }

    public void setHomeKeywords(String homeKeywords) {
        this.homeKeywords = homeKeywords;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getSiteDesc() {
        return siteDesc;
    }

    public void setSiteDesc(String siteDesc) {
        this.siteDesc = siteDesc;
    }

    public String getSiteFavicon() {
        return siteFavicon;
    }

    public void setSiteFavicon(String siteFavicon) {
        this.siteFavicon = siteFavicon;
    }

    public String getStaticWebSite() {
        return staticWebSite;
    }

    public void setStaticWebSite(String staticWebSite) {
        this.staticWebSite = staticWebSite;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public Integer getMaintenance() {
        return maintenance;
    }

    public void setMaintenance(Integer maintenance) {
        this.maintenance = maintenance;
    }

    public Date getMaintenanceData() {
        return maintenanceData;
    }

    public void setMaintenanceData(Date maintenanceData) {
        this.maintenanceData = maintenanceData;
    }

    public String getQiuniuBasePath() {
        return qiuniuBasePath;
    }

    public void setQiuniuBasePath(String qiuniuBasePath) {
        this.qiuniuBasePath = qiuniuBasePath;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SysConfig{" +
                ", id=" + id +
                ", homeDesc=" + homeDesc +
                ", homeKeywords=" + homeKeywords +
                ", headImg=" + headImg +
                ", domain=" + domain +
                ", siteUrl=" + siteUrl +
                ", siteName=" + siteName +
                ", siteDesc=" + siteDesc +
                ", siteFavicon=" + siteFavicon +
                ", staticWebSite=" + staticWebSite +
                ", authorName=" + authorName +
                ", authorEmail=" + authorEmail +
                ", qq=" + qq +
                ", maintenance=" + maintenance +
                ", maintenanceData=" + maintenanceData +
                ", qiuniuBasePath=" + qiuniuBasePath +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                "}";
    }
}