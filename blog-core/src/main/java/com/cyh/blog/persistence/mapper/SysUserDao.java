package com.cyh.blog.persistence.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.cyh.blog.persistence.beans.SysUser;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cyh
 * @since 2018-05-15
 */
public interface SysUserDao extends BaseMapper<SysUser> {

    /**
     * 查询全部
     * @return
     */
    List<SysUser> selectAll();
}
