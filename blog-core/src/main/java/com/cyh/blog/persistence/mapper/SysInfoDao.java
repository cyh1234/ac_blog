package com.cyh.blog.persistence.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.cyh.blog.persistence.beans.SysInfo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
public interface SysInfoDao extends BaseMapper<SysInfo> {

}
