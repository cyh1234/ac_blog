package com.cyh.blog.persistence.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.cyh.blog.persistence.beans.SysConfig;

import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cyh
 * @since 2018-05-15
 */
public interface SysConfigDao extends BaseMapper<SysConfig> {

    public Map<String, Object> selectWebInfo();
}
