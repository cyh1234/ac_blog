package com.cyh.blog.persistence.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.cyh.blog.persistence.beans.SysRole;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
public interface SysRoleDao extends BaseMapper<SysRole> {

    List<SysRole> selectRoleSelected(Integer userId);
}
