package com.cyh.blog.persistence.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.cyh.blog.business.vo.ResourcesVo;
import com.cyh.blog.persistence.beans.SysResources;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
public interface SysResourcesDao extends BaseMapper<SysResources> {

    List<SysResources> selectByCondition(ResourcesVo resourcesVo);

    List<SysResources> selectResourcesSelected(Integer roleId);

    List<SysResources> selectUserResources(Map<String, Object> m);
}
