package com.cyh.blog.persistence.beans;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
@TableName("sys_info")
public class SysInfo extends Model<SysInfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    @TableField("info_icon")
    private String infoIcon;
    @TableField("info_key")
    private String infoKey;
    @TableField("info_value")
    private String infoValue;

    public String getInfoKey() {
        return infoKey;
    }

    public void setInfoKey(String infoKey) {
        this.infoKey = infoKey;
    }


    public String getInfoValue() {
        return infoValue;
    }

    public void setInfoValue(String infoValue) {
        this.infoValue = infoValue;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInfoIcon() {
        return infoIcon;
    }

    public void setInfoIcon(String infoIcon) {
        this.infoIcon = infoIcon;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
