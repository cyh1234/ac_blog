package com.cyh.blog.persistence.beans;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
@TableName("sys_resources")
public class SysResources extends Model<SysResources> {

    private static final Long serialVersionUID = 1L;

    //非数据库字段
    @TableField(exist = false)
    private SysResources sysResourcesParent; //父资源
    @TableField(exist = false)
    private List<SysResources> sysResourcesList; //子资源集合
    @TableField(exist = false)
    private boolean isSelect; //是否选中

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String name;
    private String type;
    private String url;
    private String permission;
    @TableField("parent_id")
    private Integer parentId;
    private Integer sort;
    /**
     * 是否外部链接
     */
    private Integer external;
    private Integer available;
    /**
     * 菜单图标
     */
    private String icon;
    /**
     * 添加时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;


    public SysResources getSysResourcesParent() {
        return sysResourcesParent;
    }

    public void setSysResourcesParent(SysResources sysResourcesParent) {
        this.sysResourcesParent = sysResourcesParent;
    }

    public List<SysResources> getSysResourcesList() {
        return sysResourcesList;
    }

    public void setSysResourcesList(List<SysResources> sysResourcesList) {
        this.sysResourcesList = sysResourcesList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getExternal() {
        return external;
    }

    public void setExternal(Integer external) {
        this.external = external;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"sysResourcesParent\":")
                .append(sysResourcesParent);
        sb.append(",\"sysResourcesList\":")
                .append(sysResourcesList);
        sb.append(",\"isSelect\":")
                .append(isSelect);
        sb.append(",\"id\":")
                .append(id);
        sb.append(",\"name\":\"")
                .append(name).append('\"');
        sb.append(",\"type\":\"")
                .append(type).append('\"');
        sb.append(",\"url\":\"")
                .append(url).append('\"');
        sb.append(",\"permission\":\"")
                .append(permission).append('\"');
        sb.append(",\"parentId\":")
                .append(parentId);
        sb.append(",\"sort\":")
                .append(sort);
        sb.append(",\"external\":")
                .append(external);
        sb.append(",\"available\":")
                .append(available);
        sb.append(",\"icon\":\"")
                .append(icon).append('\"');
        sb.append(",\"createTime\":\"")
                .append(createTime).append('\"');
        sb.append(",\"updateTime\":\"")
                .append(updateTime).append('\"');
        sb.append('}');
        return sb.toString();
    }
}
