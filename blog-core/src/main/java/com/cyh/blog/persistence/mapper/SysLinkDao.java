package com.cyh.blog.persistence.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.cyh.blog.business.vo.LinkVo;
import com.cyh.blog.persistence.beans.SysLink;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
public interface SysLinkDao extends BaseMapper<SysLink> {

    List<SysLink> selectByCondition(LinkVo linkVo);
}
