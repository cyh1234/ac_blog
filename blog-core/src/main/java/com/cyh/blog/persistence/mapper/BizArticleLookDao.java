package com.cyh.blog.persistence.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.cyh.blog.persistence.beans.BizArticleLook;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cyh
 * @since 2018-06-04
 */
public interface BizArticleLookDao extends BaseMapper<BizArticleLook> {

}
