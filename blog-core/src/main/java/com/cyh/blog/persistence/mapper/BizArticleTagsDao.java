package com.cyh.blog.persistence.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.cyh.blog.persistence.beans.BizArticleTags;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cyh
 * @since 2018-05-16
 */
public interface BizArticleTagsDao extends BaseMapper<BizArticleTags> {

}
