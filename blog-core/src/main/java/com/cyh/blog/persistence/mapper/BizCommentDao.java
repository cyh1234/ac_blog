package com.cyh.blog.persistence.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.cyh.blog.business.vo.CommentVo;
import com.cyh.blog.persistence.beans.BizComment;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cyh
 * @since 2018-05-16
 */
public interface BizCommentDao extends BaseMapper<BizComment> {

    List<BizComment> selectByCondition(CommentVo commentVo);

    void updateSupport(Integer id);

    void updateOppose(Integer id);

    List<BizComment> selectByLast();
}
