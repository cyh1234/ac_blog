package com.cyh.blog.persistence.mapper;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.cyh.blog.persistence.beans.BizType;

/**
 * <p>
 *  Mapper 接口
 * </p>
 * @author cyh
 * @since 2018-05-16
 */
public interface BizTypeDao extends BaseMapper<BizType> {

    BizType selectByKey(Integer biztype_id);
}
