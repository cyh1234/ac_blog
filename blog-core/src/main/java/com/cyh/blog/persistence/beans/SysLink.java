package com.cyh.blog.persistence.beans;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
@TableName("sys_link")
public class SysLink extends Model<SysLink> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 链接地址
     */
    private String url;
    /**
     * 链接名
     */
    private String name;
    /**
     * 链接介绍
     */
    private String description;
    /**
     * 友链站长邮箱
     */
    private String email;
    /**
     * 友链站长QQ
     */
    private String qq;
    private String favicon;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 是否首页显示
     */
    @TableField("home_page_display")
    private Integer homePageDisplay;
    /**
     * 备注
     */
    private String remark;
    /**
     * 来源：管理员添加、自动申请
     */
    private String source;
    /**
     * 添加时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getFavicon() {
        return favicon;
    }

    public void setFavicon(String favicon) {
        this.favicon = favicon;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getHomePageDisplay() {
        return homePageDisplay;
    }

    public void setHomePageDisplay(Integer homePageDisplay) {
        this.homePageDisplay = homePageDisplay;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SysLink{" +
        ", id=" + id +
        ", url=" + url +
        ", name=" + name +
        ", description=" + description +
        ", email=" + email +
        ", qq=" + qq +
        ", favicon=" + favicon +
        ", status=" + status +
        ", homePageDisplay=" + homePageDisplay +
        ", remark=" + remark +
        ", source=" + source +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
