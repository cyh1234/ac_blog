package com.cyh.blog.persistence.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.cyh.blog.business.vo.NoticeVo;
import com.cyh.blog.persistence.beans.SysNotice;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cyh
 * @since 2018-05-31
 */
public interface SysNoticeDao extends BaseMapper<SysNotice> {

    List<SysNotice> selectByCondition(NoticeVo noticeVo);
}
