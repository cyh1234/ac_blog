/*
Navicat MySQL Data Transfer

Source Server         : 阿里云
Source Server Version : 50722
Source Host           : 120.79.59.65:3306
Source Database       : acblog

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2018-06-07 18:54:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for biz_article
-- ----------------------------
DROP TABLE IF EXISTS `biz_article`;
CREATE TABLE `biz_article` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '文章标题',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户ID',
  `cover_image` varchar(255) DEFAULT NULL COMMENT '文章封面图片',
  `qrcode_path` varchar(255) DEFAULT NULL COMMENT '文章专属二维码地址',
  `content` longtext COMMENT '文章内容',
  `top` tinyint(1) DEFAULT '0' COMMENT '是否置顶',
  `type_id` bigint(20) unsigned NOT NULL COMMENT '类型',
  `status` tinyint(1) unsigned DEFAULT NULL COMMENT '状态',
  `recommended` tinyint(1) unsigned DEFAULT '0' COMMENT '是否推荐',
  `original` tinyint(1) unsigned DEFAULT '1' COMMENT '是否原创',
  `description` varchar(300) DEFAULT NULL COMMENT '文章简介，最多200字',
  `keywords` varchar(200) DEFAULT NULL COMMENT '文章关键字，优化搜索',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of biz_article
-- ----------------------------
INSERT INTO `biz_article` VALUES ('83', '通过路径和输入流上传图片到七牛云', '14', 'http://p77ivlkgj.bkt.clouddn.com/201806060803303', null, '<p>下面的upload方法可以传入路径和输入流的方式上传， 文档 ：&nbsp;<a href=\"https://developer.qiniu.com/kodo/sdk/1239/java#upload-file\" target=\"_blank\">https://developer.qiniu.com/kodo/sdk/1239/java#upload-file</a></p><pre><code>package com.chenqian.Utils;<br>import com.google.gson.Gson;<br>import com.qiniu.common.QiniuException;<br>import com.qiniu.http.Response;<br>import com.qiniu.storage.Configuration;<br>import com.qiniu.storage.UploadManager;<br>import com.qiniu.storage.model.DefaultPutRet;<br>import com.qiniu.util.Auth;<br><br>import java.io.*;<br>import java.text.SimpleDateFormat;<br>import java.util.Date;<br><br>public class QiNiu {<br><br>    //设置好账号的ACCESS_KEY和SECRET_KEY<br>    String ACCESS_KEY = \"***\";<br>    String SECRET_KEY = \"***\";<br>    //要上传的空间名--<br>    String bucketname = 空间名字\";<br><br><br>    String key = \"\";<br><br>    //密钥配置<br>    Auth auth = Auth.create(ACCESS_KEY, SECRET_KEY);<br>    //创建上传对象<br>    UploadManager uploadManager = new UploadManager(new Configuration());<br><br><br>    //简单上传，使用默认策略，只需要设置上传的空间名就可以了<br>    public String getUpToken() {<br>        return auth.uploadToken(bucketname);<br>    }<br><br>    /**<br>     * @param FilePath 本地文件路径<br>     * @return 保存的文件名称<br>     * @throws IOException<br>     */<br>    public String upload(String FilePath) throws IOException {<br><br>        String[] split = FilePath.split(\"\\\\.\");<br><br>        //通过日期生成图片名称<br>        SimpleDateFormat sdf = new SimpleDateFormat(\"yyyyMMddHHmmss\");<br>        key = sdf.format(new Date());<br>        key += (int)(Math.random()*1000);<br>        key += \".\"+split[split.length-1];<br><br>        try {<br>            System.out.println(FilePath);<br>            //调用put方法上传<br>            Response res = uploadManager.put(FilePath, key, getUpToken());<br>            //打印返回的信息<br>            System.out.println(res.bodyString());<br>            System.out.println(res.statusCode);//200为上传成功<br>        } catch (QiniuException e) {<br>            Response r = e.response;<br>            // 请求失败时打印的异常的信息<br>            System.out.println(r.toString());<br>            try {<br>                //响应的文本信息<br>                System.out.println(r.bodyString());<br>            } catch (QiniuException e1) {<br>            }<br>        }<br>        return \"http://p77ivlkgj.bkt.clouddn.com/\"+key;  //返回图片地址<br>    }<br><br>    /**<br>     * 通过输入流上传<br>     * @param inputStream<br>     * @return<br>     * @throws IOException<br>     */<br>    public String upload(InputStream inputStream) throws IOException {<br><br>        SimpleDateFormat sdf = new SimpleDateFormat(\"yyyyMMddHHmmss\");<br>        key = sdf.format(new Date());<br>        key += (int)(Math.random()*1000);<br><br>        try {<br>            byte[] uploadBytes = new byte[inputStream.available()];<br><br>            inputStream.read(uploadBytes);<br><br>            ByteArrayInputStream byteInputStream=new ByteArrayInputStream(uploadBytes);<br>            Auth auth = Auth.create(ACCESS_KEY, SECRET_KEY);<br>            String upToken = auth.uploadToken(bucketname);<br>            try {<br>                Response response = uploadManager.put(byteInputStream,key,upToken,null, null);<br>                //解析上传成功的结果<br>                DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);<br>                System.out.println(putRet.key);<br>                System.out.println(putRet.hash);<br>            } catch (QiniuException ex) {<br>                Response r = ex.response;<br>                System.err.println(r.toString());<br>                try {<br>                    System.err.println(r.bodyString());<br>                } catch (QiniuException ex2) {<br>                    //ignore<br>                }<br>            }<br>        } catch (UnsupportedEncodingException ex) {<br><br>        }<br>        return \"http://p77ivlkgj.bkt.clouddn.com/\"+key;<br>    }<br><br>    public static void main(String[] args) throws IOException {<br>       InputStream input = new FileInputStream(\"D:\\\\360Downloads\\\\a2.jpg\");<br>       new QiNiu().upload(input);<br>    }<br>}</code></pre><p><br></p><p>&nbsp;</p><p><br></p><p>&nbsp;<br></p>', '0', '2', '1', '0', '0', '通过路径和输入流上传图片到七牛云', '通过路径和输入流上传图片到七牛云', '2018-06-06 08:05:39', '2018-06-06 08:05:39');
INSERT INTO `biz_article` VALUES ('84', 'SpringMvc 文件上传', '14', 'http://p77ivlkgj.bkt.clouddn.com/20180606082742513', null, '<p>上传文件的表单必须指定为：&nbsp;&nbsp;<br></p><pre><code>enctype=\"multipart/form-data\" <br>method=\"post\"</code></pre><p>springmvc.xml配置&nbsp;</p><pre><code>&lt;bean id=\"multipartResolver\" class=\"org.springframework.web.multipart.commons.CommonsMultipartResolver\"&gt;<br>        &lt;property name=\"defaultEncoding\" value=\"utf-8\"&gt;&lt;/property&gt;<br>        &lt;property name=\"maxUploadSize\" value=\"10485760000\"&gt;&lt;/property&gt;<br>    &lt;/bean&gt;</code></pre><h3 id=\"用form表单提交\">用form表单提交</h3><p>前台</p><pre><code>&lt;form id=\"loadForm\" action=\"${path}/upload\" enctype=\"multipart/form-data\" method=\"post\"&gt;<br>    &lt;input name=\"fileInput\" type=\"file\"&gt;<br>    &lt;input  type=\"button\" onclick=\"upload()\" value=\"提交\"/&gt;<br>&lt;/form&gt;</code></pre><p>控制器&nbsp;</p><pre><code>@RequestMapping(value=\"upload\", method = RequestMethod.POST)<br>    public String upload(<br>            HttpSession session,<br>            @RequestParam(\"fileInput\") MultipartFile file) throws IOException {<br><br>        String path = session.getServletContext().getRealPath(\"/static/img\");<br>        String fileName = file.getOriginalFilename();<br>        file.transferTo(new File(path, fileName));<br>        System.out.println(path+\",\"+fileName);<br>        return \"success\";<br>    }</code></pre><h2 id=\"ajax提交\">ajax提交</h2><p>前台代码</p><pre><code>&lt;script src=\"https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js\"&gt;&lt;/script&gt;<br>&lt;form id=\"loadForm\" action=\"${path}/upload\" enctype=\"multipart/form-data\" method=\"post\"&gt;<br>    &lt;input name=\"fileInput\" type=\"file\"&gt;<br>    &lt;input  type=\"button\" onclick=\"upload()\" value=\"提交\"/&gt;<br>&lt;/form&gt;<br><br>&lt;script&gt;<br>    function upload(){<br>        var formData = new FormData($( \"#loadForm\" )[0]);<br>        $.ajax({<br>            url: \'${path}/upload\',<br>            type: \'POST\',<br>            data: formData,<br>            async: false,<br>            cache: false,<br>            contentType: false,<br>            processData: false,<br>            success: function (msg) {<br>                alert(msg);<br>            }<br>        });<br>    }<br>&lt;/script&gt;</code></pre><p>控制器还是一样的&nbsp;&nbsp;<br></p><p>&nbsp;</p>', '0', '2', '1', '0', '1', 'SpringMvc 文件上传', 'SpringMvc 文件上传', '2018-06-06 08:26:46', '2018-06-06 08:26:46');

-- ----------------------------
-- Table structure for biz_article_look
-- ----------------------------
DROP TABLE IF EXISTS `biz_article_look`;
CREATE TABLE `biz_article_look` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` bigint(20) unsigned NOT NULL COMMENT '文章ID',
  `user_id` bigint(20) unsigned DEFAULT NULL COMMENT '已登录用户ID',
  `user_ip` varchar(50) DEFAULT NULL COMMENT '用户IP',
  `look_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '浏览时间',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of biz_article_look
-- ----------------------------

-- ----------------------------
-- Table structure for biz_article_love
-- ----------------------------
DROP TABLE IF EXISTS `biz_article_love`;
CREATE TABLE `biz_article_love` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` bigint(20) unsigned NOT NULL COMMENT '文章ID',
  `user_id` bigint(20) unsigned DEFAULT NULL COMMENT '已登录用户ID',
  `user_ip` varchar(50) DEFAULT NULL COMMENT '用户IP',
  `love_time` datetime DEFAULT NULL COMMENT '浏览时间',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of biz_article_love
-- ----------------------------
INSERT INTO `biz_article_love` VALUES ('47', '84', null, '0:0:0:0:0:0:0:1', '2018-06-06 23:04:42', '2018-06-06 23:04:42', '2018-06-06 23:04:42');
INSERT INTO `biz_article_love` VALUES ('48', '84', null, '127.0.0.1', '2018-06-07 13:51:19', '2018-06-07 13:51:19', '2018-06-07 13:51:19');
INSERT INTO `biz_article_love` VALUES ('49', '83', null, '127.0.0.1', '2018-06-07 13:52:15', '2018-06-07 13:52:15', '2018-06-07 13:52:15');
INSERT INTO `biz_article_love` VALUES ('50', '84', null, '127.0.0.1', '2018-06-07 16:36:50', '2018-06-07 16:36:50', '2018-06-07 16:36:50');

-- ----------------------------
-- Table structure for biz_article_tags
-- ----------------------------
DROP TABLE IF EXISTS `biz_article_tags`;
CREATE TABLE `biz_article_tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` bigint(20) unsigned NOT NULL COMMENT '标签表主键',
  `article_id` bigint(20) unsigned NOT NULL COMMENT '文章ID',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of biz_article_tags
-- ----------------------------
INSERT INTO `biz_article_tags` VALUES ('113', '3', '75', '2018-05-23 22:48:33', '2018-05-23 22:48:33');
INSERT INTO `biz_article_tags` VALUES ('114', '5', '76', '2018-05-23 22:49:42', '2018-05-23 22:49:42');
INSERT INTO `biz_article_tags` VALUES ('116', '5', '77', '2018-05-23 22:50:41', '2018-05-23 22:50:41');
INSERT INTO `biz_article_tags` VALUES ('117', '5', '78', '2018-05-23 22:51:16', '2018-05-23 22:51:16');
INSERT INTO `biz_article_tags` VALUES ('120', '2', '79', '2018-06-03 16:56:03', '2018-06-03 16:56:03');
INSERT INTO `biz_article_tags` VALUES ('121', '4', '79', '2018-06-03 16:56:04', '2018-06-03 16:56:04');
INSERT INTO `biz_article_tags` VALUES ('122', '1', '80', '2018-06-03 17:07:50', '2018-06-03 17:07:50');
INSERT INTO `biz_article_tags` VALUES ('123', '2', '80', '2018-06-03 17:07:52', '2018-06-03 17:07:52');
INSERT INTO `biz_article_tags` VALUES ('130', '1', '81', '2018-06-03 17:09:46', '2018-06-03 17:09:46');
INSERT INTO `biz_article_tags` VALUES ('131', '2', '81', '2018-06-03 17:09:46', '2018-06-03 17:09:46');
INSERT INTO `biz_article_tags` VALUES ('132', '2', '82', '2018-06-06 07:53:28', '2018-06-06 07:53:28');
INSERT INTO `biz_article_tags` VALUES ('133', '3', '82', '2018-06-06 07:53:28', '2018-06-06 07:53:28');
INSERT INTO `biz_article_tags` VALUES ('134', '2', '83', '2018-06-06 08:05:39', '2018-06-06 08:05:39');
INSERT INTO `biz_article_tags` VALUES ('135', '4', '83', '2018-06-06 08:05:39', '2018-06-06 08:05:39');
INSERT INTO `biz_article_tags` VALUES ('137', '4', '84', '2018-06-06 08:27:48', '2018-06-06 08:27:48');

-- ----------------------------
-- Table structure for biz_comment
-- ----------------------------
DROP TABLE IF EXISTS `biz_comment`;
CREATE TABLE `biz_comment` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sid` bigint(20) DEFAULT NULL COMMENT '被评论的文章或者页面的ID',
  `user_id` bigint(20) unsigned DEFAULT NULL COMMENT '评论人的ID',
  `pid` bigint(20) unsigned DEFAULT NULL COMMENT '父级评论的id',
  `qq` varchar(13) DEFAULT NULL COMMENT '评论人的QQ（未登录用户）',
  `nickname` varchar(13) DEFAULT NULL COMMENT '评论人的昵称（未登录用户）',
  `avatar` varchar(255) DEFAULT NULL COMMENT '评论人的头像地址',
  `email` varchar(100) DEFAULT NULL COMMENT '评论人的邮箱地址（未登录用户）',
  `url` varchar(200) DEFAULT NULL COMMENT '评论人的网站地址（未登录用户）',
  `status` enum('VERIFYING','APPROVED','REJECT','DELETED') DEFAULT 'VERIFYING' COMMENT '评论的状态',
  `ip` varchar(64) DEFAULT NULL COMMENT '评论时的ip',
  `lng` varchar(50) DEFAULT NULL COMMENT '经度',
  `lat` varchar(50) DEFAULT NULL COMMENT '纬度',
  `address` varchar(100) DEFAULT NULL COMMENT '评论时的地址',
  `os` varchar(64) DEFAULT NULL COMMENT '评论时的系统类型',
  `os_short_name` varchar(10) DEFAULT NULL COMMENT '评论时的系统的简称',
  `browser` varchar(64) DEFAULT NULL COMMENT '评论时的浏览器类型',
  `browser_short_name` varchar(10) DEFAULT NULL COMMENT '评论时的浏览器的简称',
  `content` text COMMENT '评论的内容',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注（审核不通过时添加）',
  `support` int(10) unsigned DEFAULT '0' COMMENT '支持（赞）',
  `oppose` int(10) unsigned DEFAULT '0' COMMENT '反对（踩）',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of biz_comment
-- ----------------------------
INSERT INTO `biz_comment` VALUES ('1', '83', null, null, '2228114147', '渐行渐远', 'http://qlogo4.store.qq.com/qzone/2228114147/2228114147/100', '2228114147@qq.com', 'http://2228114147.qzone.qq.com ', 'VERIFYING', null, null, null, null, null, null, null, null, '哈哈', null, '0', '0', '2018-06-07 12:28:38', '2018-06-07 12:28:37');

-- ----------------------------
-- Table structure for biz_tags
-- ----------------------------
DROP TABLE IF EXISTS `biz_tags`;
CREATE TABLE `biz_tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '书签名',
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of biz_tags
-- ----------------------------
INSERT INTO `biz_tags` VALUES ('1', 'Linux', 'linux技术', '2018-01-14 21:35:31', '2018-01-14 21:35:31');
INSERT INTO `biz_tags` VALUES ('2', 'Java', 'java', '2018-01-14 21:35:45', '2018-01-14 21:35:45');
INSERT INTO `biz_tags` VALUES ('3', 'Spring', 'spring技术', '2018-01-14 21:35:52', '2018-01-14 21:35:52');
INSERT INTO `biz_tags` VALUES ('4', 'Spring Boot', null, '2018-01-14 21:36:01', '2018-01-14 21:36:01');
INSERT INTO `biz_tags` VALUES ('5', '其他', '个人随笔', '2018-01-14 21:36:07', '2018-01-14 21:36:07');

-- ----------------------------
-- Table structure for biz_type
-- ----------------------------
DROP TABLE IF EXISTS `biz_type`;
CREATE TABLE `biz_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT '文章类型名',
  `description` varchar(200) DEFAULT NULL COMMENT '类型介绍',
  `sort` int(10) DEFAULT NULL COMMENT '排序',
  `icon` varchar(100) DEFAULT NULL COMMENT '图标',
  `available` tinyint(1) unsigned DEFAULT '1' COMMENT '是否可用',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of biz_type
-- ----------------------------
INSERT INTO `biz_type` VALUES ('1', null, '前端技术', '主要收集、整理的基础前端类文章，包括JS、jQuery和CSS等Web开发所需的基础的文章总结', '1', 'fa fa-css3', '1', '2018-01-14 21:34:54', '2018-01-14 21:34:54');
INSERT INTO `biz_type` VALUES ('2', null, '后端技术', '网站中记录的后端类文章，包括Java、SSM、MySQL和其他在日常工作学习中所用的后端技术', '2', 'fa fa-coffee', '1', '2018-01-14 21:34:57', '2018-01-14 21:34:57');
INSERT INTO `biz_type` VALUES ('3', null, '其他文章', '记录网站建设以及日常工作、学习中的闲言碎语和个人笔记等文章', '3', 'fa fa-folder-open-o', '1', '2018-01-20 22:28:03', '2018-03-01 19:27:53');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `home_desc` varchar(255) DEFAULT NULL COMMENT '首页描述',
  `home_keywords` varchar(255) DEFAULT NULL COMMENT '首页关键字',
  `head_img` varchar(255) DEFAULT NULL COMMENT '资料头像',
  `domain` varchar(255) DEFAULT NULL COMMENT '根域名',
  `site_url` varchar(255) DEFAULT NULL COMMENT '网站地址',
  `site_name` varchar(255) DEFAULT NULL COMMENT '站点名称',
  `site_desc` varchar(255) DEFAULT NULL COMMENT '站点描述',
  `site_favicon` varchar(255) DEFAULT NULL COMMENT '站点LOGO',
  `static_web_site` varchar(255) DEFAULT NULL COMMENT '资源文件（js、css等的路径）',
  `author_name` varchar(100) DEFAULT NULL COMMENT '站长名称',
  `author_email` varchar(100) DEFAULT NULL COMMENT '站长邮箱',
  `qq` varchar(255) DEFAULT NULL COMMENT 'QQ',
  `maintenance` tinyint(1) unsigned DEFAULT NULL COMMENT '是否开启维护通知',
  `maintenance_data` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '系统维护时间',
  `qiuniu_base_path` varchar(255) DEFAULT NULL COMMENT '七牛路径',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', '啊程博客', '啊程博客,啊程个人技术博客,cyhblog,啊程blog', 'http://p77ivlkgj.bkt.clouddn.com/1_qq_36238595.jpg', 'cyhblog.top', 'http://localhost:8443', '博客', '一个业余程序员', 'https://static.zhyd.me/static/img/favicon.ico', 'http://localhost:8443', '啊程', '2228114147@qq.com', '2228114147', '0', '2018-01-21 23:07:52', 'http://p6fs0hjph.bkt.clouddn.com/', '2018-01-19 23:07:52', '2018-02-25 18:40:48');

-- ----------------------------
-- Table structure for sys_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_info`;
CREATE TABLE `sys_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info_icon` varchar(255) DEFAULT NULL,
  `info_key` varchar(255) DEFAULT '',
  `info_value` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_info
-- ----------------------------
INSERT INTO `sys_info` VALUES ('2', 'fa fa-chain', 'csdn', 'https://blog.csdn.net/qq_36238595');
INSERT INTO `sys_info` VALUES ('4', 'fa fa-chain', '码云', 'https://gitee.com/cyh1234');
INSERT INTO `sys_info` VALUES ('5', 'fa fa-qq', 'qq', '2228114147');

-- ----------------------------
-- Table structure for sys_link
-- ----------------------------
DROP TABLE IF EXISTS `sys_link`;
CREATE TABLE `sys_link` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(200) NOT NULL COMMENT '链接地址',
  `name` varchar(50) DEFAULT NULL COMMENT '链接名',
  `description` varchar(300) DEFAULT NULL COMMENT '链接介绍',
  `email` varchar(100) DEFAULT NULL COMMENT '友链站长邮箱',
  `qq` varchar(13) DEFAULT NULL COMMENT '友链站长QQ',
  `favicon` varchar(255) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT '0' COMMENT '状态',
  `home_page_display` tinyint(1) unsigned DEFAULT '1' COMMENT '是否首页显示',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `source` enum('ADMIN','AUTOMATIC') DEFAULT 'ADMIN' COMMENT '来源：管理员添加、自动申请',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_link
-- ----------------------------
INSERT INTO `sys_link` VALUES ('1', 'https://www.zhyd.me', '张亚东博客', '一个程序员的个人博客', 'yadong.zhang0415@gmail.com', null, 'https://static.zhyd.me/static/img/favicon.ico', '1', '1', null, 'ADMIN', '2016-11-16 23:32:03', '2018-01-23 11:27:19');
INSERT INTO `sys_link` VALUES ('38', 'https://blog.csdn.net/qq_36238595', '啊程csdn', '啊程csdn', '2228114147@qq.com', null, null, '1', '1', null, 'ADMIN', '2018-06-03 16:33:18', '2018-06-03 16:33:18');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL COMMENT '被通知的用户ID',
  `status` enum('RELEASE','NOT_RELEASE') DEFAULT 'NOT_RELEASE' COMMENT '通知状态',
  `title` varchar(200) DEFAULT NULL COMMENT '通知的标题',
  `content` varchar(2000) DEFAULT NULL COMMENT '通知的内容',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES ('11', '0', 'RELEASE', '6/13 晴', '6/13', '2018-06-03 14:44:17', '2018-06-03 14:44:17');
INSERT INTO `sys_notice` VALUES ('12', '0', 'RELEASE', '啊程博客发布啦', '啊程博客发布啦', '2018-06-03 14:45:59', '2018-06-03 14:45:59');

-- ----------------------------
-- Table structure for sys_resources
-- ----------------------------
DROP TABLE IF EXISTS `sys_resources`;
CREATE TABLE `sys_resources` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `permission` varchar(100) DEFAULT NULL,
  `parent_id` bigint(20) unsigned DEFAULT '0',
  `sort` int(10) unsigned DEFAULT NULL,
  `external` tinyint(1) unsigned DEFAULT NULL COMMENT '是否外部链接',
  `available` tinyint(1) unsigned DEFAULT '0',
  `icon` varchar(100) DEFAULT NULL COMMENT '菜单图标',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_sys_resource_parent_id` (`parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_resources
-- ----------------------------
INSERT INTO `sys_resources` VALUES ('1', '首页', 'menu', '/home', 'index', '0', '0', '0', '1', 'fa fa-home', '2017-12-20 16:40:06', '2018-05-31 22:39:34');
INSERT INTO `sys_resources` VALUES ('2', '用户管理', 'menu', '/users', 'users', '0', '1', '0', '1', 'fa fa-user', '2017-12-22 13:56:15', '2018-06-01 07:05:04');
INSERT INTO `sys_resources` VALUES ('3', '系统配置', 'menu', null, null, '0', '7', '0', '1', 'fa fa-cog', '2017-12-20 16:40:06', '2017-12-20 16:40:08');
INSERT INTO `sys_resources` VALUES ('4', '资源管理', 'menu', '/resources', 'resources', '3', '1', '0', '1', null, '2017-12-22 15:31:05', '2017-12-22 15:31:05');
INSERT INTO `sys_resources` VALUES ('5', '角色管理', 'menu', '/roles', 'roles', '3', '2', '0', '1', null, '2017-12-22 15:31:27', '2017-12-22 15:31:27');
INSERT INTO `sys_resources` VALUES ('11', '文章管理', 'menu', '', '', '0', '2', '0', '1', 'fa fa-file-text-o', '2018-01-13 21:23:00', '2018-01-13 21:23:00');
INSERT INTO `sys_resources` VALUES ('12', '文章列表', 'menu', '/articles', 'articles', '11', '1', '0', '1', '', '2018-01-13 21:45:50', '2018-01-13 21:51:47');
INSERT INTO `sys_resources` VALUES ('13', '发表文章', 'menu', '/article/publish', 'article:publish', '11', '2', '0', '1', '', '2018-01-13 21:48:36', '2018-01-13 21:51:53');
INSERT INTO `sys_resources` VALUES ('14', '分类列表', 'menu', '/article/types', 'article:types', '11', '3', '0', '1', '', '2018-01-13 21:49:36', '2018-01-13 21:51:58');
INSERT INTO `sys_resources` VALUES ('15', '标签列表', 'menu', '/article/tags', 'article:tags', '11', '4', '0', '1', '', '2018-01-13 21:50:10', '2018-01-13 21:51:41');
INSERT INTO `sys_resources` VALUES ('16', '友情链接', 'menu', '/links', 'links', '21', '3', '0', '1', null, '2018-01-13 21:54:21', '2018-01-13 21:54:21');
INSERT INTO `sys_resources` VALUES ('17', '评论管理', 'menu', '/comments', 'comments', '21', '4', '0', '1', null, '2018-01-14 20:25:29', '2018-01-14 20:29:56');
INSERT INTO `sys_resources` VALUES ('18', '模板管理', 'menu', '/templates', 'templates', '21', '3', '0', '1', null, '2018-01-14 20:26:28', '2018-01-14 20:28:10');
INSERT INTO `sys_resources` VALUES ('19', '更新记录', 'menu', '/updates', 'updates', '21', '5', '0', '1', null, '2018-01-15 17:46:23', '2018-01-15 17:49:46');
INSERT INTO `sys_resources` VALUES ('20', '公告管理', 'menu', '/notices', 'notices', '21', '6', '0', '1', null, '2018-01-15 17:54:14', '2018-01-15 17:54:14');
INSERT INTO `sys_resources` VALUES ('21', '网站管理', 'menu', '', null, '0', '2', '0', '1', 'fa fa-bold', '2018-01-31 11:25:13', '2018-01-31 11:25:13');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  `available` tinyint(1) DEFAULT '0',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', '1', '2017-12-20 16:40:24', '2017-12-20 16:40:26');
INSERT INTO `sys_role` VALUES ('2', '管理员', '1', '2017-12-22 13:56:39', '2017-12-22 13:56:39');
INSERT INTO `sys_role` VALUES ('3', '测试', '1', '2018-05-31 19:53:06', '2018-05-31 19:53:06');

-- ----------------------------
-- Table structure for sys_role_resources
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_resources`;
CREATE TABLE `sys_role_resources` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned NOT NULL,
  `resources_id` bigint(20) unsigned NOT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=571 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_role_resources
-- ----------------------------
INSERT INTO `sys_role_resources` VALUES ('213', '2', '11', null, null);
INSERT INTO `sys_role_resources` VALUES ('214', '2', '12', null, null);
INSERT INTO `sys_role_resources` VALUES ('215', '2', '13', null, null);
INSERT INTO `sys_role_resources` VALUES ('216', '2', '14', null, null);
INSERT INTO `sys_role_resources` VALUES ('217', '2', '15', null, null);
INSERT INTO `sys_role_resources` VALUES ('218', '2', '21', null, null);
INSERT INTO `sys_role_resources` VALUES ('219', '2', '16', null, null);
INSERT INTO `sys_role_resources` VALUES ('220', '2', '18', null, null);
INSERT INTO `sys_role_resources` VALUES ('221', '2', '17', null, null);
INSERT INTO `sys_role_resources` VALUES ('222', '2', '19', null, null);
INSERT INTO `sys_role_resources` VALUES ('223', '2', '20', null, null);
INSERT INTO `sys_role_resources` VALUES ('224', '2', '3', null, null);
INSERT INTO `sys_role_resources` VALUES ('225', '2', '4', null, null);
INSERT INTO `sys_role_resources` VALUES ('226', '2', '5', null, null);
INSERT INTO `sys_role_resources` VALUES ('457', '3', '1', '2018-06-03 14:57:41', '2018-06-03 14:57:41');
INSERT INTO `sys_role_resources` VALUES ('463', '3', '11', '2018-06-03 14:57:41', '2018-06-03 14:57:41');
INSERT INTO `sys_role_resources` VALUES ('464', '3', '12', '2018-06-03 14:57:41', '2018-06-03 14:57:41');
INSERT INTO `sys_role_resources` VALUES ('465', '3', '13', '2018-06-03 14:57:41', '2018-06-03 14:57:41');
INSERT INTO `sys_role_resources` VALUES ('466', '3', '14', '2018-06-03 14:57:41', '2018-06-03 14:57:41');
INSERT INTO `sys_role_resources` VALUES ('467', '3', '15', '2018-06-03 14:57:41', '2018-06-03 14:57:41');
INSERT INTO `sys_role_resources` VALUES ('468', '3', '21', '2018-06-03 14:57:41', '2018-06-03 14:57:41');
INSERT INTO `sys_role_resources` VALUES ('469', '3', '16', '2018-06-03 14:57:41', '2018-06-03 14:57:41');
INSERT INTO `sys_role_resources` VALUES ('470', '3', '17', '2018-06-03 14:57:41', '2018-06-03 14:57:41');
INSERT INTO `sys_role_resources` VALUES ('471', '3', '18', '2018-06-03 14:57:41', '2018-06-03 14:57:41');
INSERT INTO `sys_role_resources` VALUES ('472', '3', '19', '2018-06-03 14:57:41', '2018-06-03 14:57:41');
INSERT INTO `sys_role_resources` VALUES ('473', '3', '20', '2018-06-03 14:57:41', '2018-06-03 14:57:41');
INSERT INTO `sys_role_resources` VALUES ('550', '1', '1', '2018-06-06 13:06:43', '2018-06-06 13:06:43');
INSERT INTO `sys_role_resources` VALUES ('551', '1', '2', '2018-06-06 13:06:43', '2018-06-06 13:06:43');
INSERT INTO `sys_role_resources` VALUES ('557', '1', '11', '2018-06-06 13:06:43', '2018-06-06 13:06:43');
INSERT INTO `sys_role_resources` VALUES ('558', '1', '12', '2018-06-06 13:06:43', '2018-06-06 13:06:43');
INSERT INTO `sys_role_resources` VALUES ('559', '1', '13', '2018-06-06 13:06:43', '2018-06-06 13:06:43');
INSERT INTO `sys_role_resources` VALUES ('560', '1', '14', '2018-06-06 13:06:43', '2018-06-06 13:06:43');
INSERT INTO `sys_role_resources` VALUES ('561', '1', '15', '2018-06-06 13:06:43', '2018-06-06 13:06:43');
INSERT INTO `sys_role_resources` VALUES ('562', '1', '21', '2018-06-06 13:06:43', '2018-06-06 13:06:43');
INSERT INTO `sys_role_resources` VALUES ('563', '1', '16', '2018-06-06 13:06:43', '2018-06-06 13:06:43');
INSERT INTO `sys_role_resources` VALUES ('564', '1', '17', '2018-06-06 13:06:43', '2018-06-06 13:06:43');
INSERT INTO `sys_role_resources` VALUES ('565', '1', '18', '2018-06-06 13:06:43', '2018-06-06 13:06:43');
INSERT INTO `sys_role_resources` VALUES ('566', '1', '19', '2018-06-06 13:06:43', '2018-06-06 13:06:43');
INSERT INTO `sys_role_resources` VALUES ('567', '1', '20', '2018-06-06 13:06:43', '2018-06-06 13:06:43');
INSERT INTO `sys_role_resources` VALUES ('568', '1', '3', '2018-06-06 21:20:48', '2018-06-06 21:20:48');
INSERT INTO `sys_role_resources` VALUES ('569', '1', '4', '2018-06-06 21:20:54', '2018-06-06 21:20:54');
INSERT INTO `sys_role_resources` VALUES ('570', '1', '5', '2018-06-06 21:21:07', '2018-06-06 21:21:07');

-- ----------------------------
-- Table structure for sys_template
-- ----------------------------
DROP TABLE IF EXISTS `sys_template`;
CREATE TABLE `sys_template` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ref_key` varchar(100) DEFAULT NULL COMMENT '键',
  `ref_value` text COMMENT '模板内容',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_template
-- ----------------------------
INSERT INTO `sys_template` VALUES ('1', 'TM_SITEMAP_XML', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<urlset xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\r\n	<url>\r\n		<loc>${config.siteUrl}</loc>\r\n		<priority>1.0</priority>\r\n		<lastmod>${.now?string(\"yyyy-MM-dd\")}</lastmod>\r\n		<changefreq>daily</changefreq>\r\n	</url>\r\n	<url>\r\n		<loc>${config.siteUrl}/about</loc>\r\n		<priority>0.6</priority>\r\n		<lastmod>${.now?string(\"yyyy-MM-dd\")}</lastmod>\r\n		<changefreq>daily</changefreq>\r\n	</url>\r\n	<url>\r\n		<loc>${config.siteUrl}/links</loc>\r\n		<priority>0.6</priority>\r\n		<lastmod>${.now?string(\"yyyy-MM-dd\")}</lastmod>\r\n		<changefreq>daily</changefreq>\r\n	</url>\r\n	<url>\r\n		<loc>${config.siteUrl}/guestbook</loc>\r\n		<priority>0.6</priority>\r\n		<lastmod>${.now?string(\"yyyy-MM-dd\")}</lastmod>\r\n		<changefreq>daily</changefreq>\r\n	</url>\r\n	<url>\r\n		<loc>${config.siteUrl}/updateLog</loc>\r\n		<priority>0.6</priority>\r\n		<lastmod>${.now?string(\"yyyy-MM-dd\")}</lastmod>\r\n		<changefreq>daily</changefreq>\r\n	</url>\r\n	<url>\r\n		<loc>${config.siteUrl}/recommended</loc>\r\n		<priority>0.6</priority>\r\n		<lastmod>${.now?string(\"yyyy-MM-dd\")}</lastmod>\r\n		<changefreq>daily</changefreq>\r\n	</url>\r\n	<#list articleList as item>\r\n		<url>\r\n			<loc>${config.siteUrl}/article/${item.id}</loc>\r\n			<priority>0.6</priority>\r\n			<lastmod>${item.updateTime?string(\"yyyy-MM-dd\")}</lastmod>\r\n			<changefreq>daily</changefreq>\r\n		</url>\r\n	</#list>\r\n	<#list articleTypeList as item>\r\n	   <url>\r\n			<loc>${config.siteUrl}/type/${item.id}</loc>\r\n			<priority>0.6</priority>\r\n			<lastmod>${item.updateTime?string(\"yyyy-MM-dd\")}</lastmod>\r\n			<changefreq>daily</changefreq>\r\n		</url>\r\n	</#list>\r\n	\r\n	<#list articleTagsList as item>\r\n	   <url>\r\n			<loc>${config.siteUrl}/tag/${item.id}</loc>\r\n			<priority>0.6</priority>\r\n			<lastmod>${item.updateTime?string(\"yyyy-MM-dd\")}</lastmod>\r\n			<changefreq>daily</changefreq>\r\n		</url>\r\n	</#list>\r\n</urlset>', '2018-01-19 10:47:06', '2018-05-31 17:28:58');
INSERT INTO `sys_template` VALUES ('2', 'TM_SITEMAP_TXT', '${config.siteUrl}\r\n${config.siteUrl}/about\r\n${config.siteUrl}/links\r\n${config.siteUrl}/guestbook\r\n${config.siteUrl}/updateLog\r\n${config.siteUrl}/recommended\r\n<#list articleList as item>\r\n${config.siteUrl}/article/${item.id}\r\n</#list>\r\n<#list articleTypeList as item>\r\n${config.siteUrl}/type/${item.id}\r\n</#list>\r\n<#list articleTagsList as item>\r\n${config.siteUrl}/tag/${item.id}\r\n</#list>', '2018-01-19 10:47:12', '2018-02-28 12:11:59');
INSERT INTO `sys_template` VALUES ('3', 'TM_SITEMAP_HTML', '<!DOCTYPE html>\r\n<html lang=\"zh-CN\">\r\n<head>\r\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>\r\n    <title>${config.siteName} 网站地图</title>\r\n    <meta name=\"author\" content=\"SiteMapX.com\"/>\r\n    <meta name=\"robots\" content=\"index,follow\"/>\r\n    <style type=\"text/css\">\r\n        body {\r\n            color: #000000;\r\n            background: #ffffff;\r\n            margin: 20px;\r\n            font-family: Verdana, Arial, Helvetica, sans-serif;\r\n            font-size: 12px;\r\n        }\r\n\r\n        #myTable {\r\n            list-style: none;\r\n            margin: 10px 0px 10px 0px;\r\n            padding: 0px;\r\n            width: 100%;\r\n            min-width: 804px;\r\n        }\r\n\r\n        #myTable li {\r\n            list-style-type: none;\r\n            width: 100%;\r\n            min-width: 404px;\r\n            height: 20px;\r\n            line-height: 20px;\r\n        }\r\n\r\n        .pull-left{\r\n            float: left!important;\r\n        }\r\n        .pull-right{\r\n            float: right!important;\r\n        }\r\n\r\n        #myTable li .T1-h {\r\n            font-weight: bold;\r\n            min-width: 300px;\r\n        }\r\n\r\n        #myTable li .T2-h {\r\n            width: 200px;\r\n            font-weight: bold;\r\n        }\r\n\r\n        #myTable li .T3-h {\r\n            width: 200px;\r\n            font-weight: bold;\r\n        }\r\n\r\n        #myTable li .T4-h {\r\n            width: 100px;\r\n            font-weight: bold;\r\n        }\r\n\r\n        #myTable li .T1 {\r\n            min-width: 300px;\r\n        }\r\n\r\n        #myTable li .T2 {\r\n            width: 200px;\r\n        }\r\n\r\n        #myTable li .T3 {\r\n            width: 200px;\r\n        }\r\n\r\n        #myTable li .T4 {\r\n            width: 100px;\r\n        }\r\n\r\n        #footer {\r\n            padding: 2px;\r\n            margin: 0px;\r\n            font-size: 8pt;\r\n            color: gray;\r\n            min-width: 900px;\r\n        }\r\n\r\n        #footer a {\r\n            color: gray;\r\n        }\r\n\r\n        .myClear {\r\n            clear: both;\r\n        }\r\n\r\n        #nav, #content, #footer {padding: 8px; border: 1px solid #EEEEEE; clear: both; width: 95%; margin: auto; margin-top: 10px;}\r\n\r\n    </style>\r\n</head>\r\n<body>\r\n<h2 style=\"text-align: center; margin-top: 20px\">${config.siteName?if_exists} 网站地图 </h2>\r\n<div id=\"nav\"><a href=\"${config.siteUrl?if_exists}\"><strong>${config.siteName?if_exists}</strong></a> &raquo; <a href=\"${config.siteUrl?if_exists}/sitemap.html\">站点地图</a></div>\r\n<div id=\"content\">\r\n    <h3>最新文章</h3>\r\n    <ul id=\"myTable\">\r\n        <li>\r\n            <div class=\"T1-h pull-left\">URL</div>\r\n            <div class=\"T2-h pull-right\">Last Change</div>\r\n            <div class=\"T3-h pull-right\">Change Frequency</div>\r\n            <div class=\"T4-h pull-right\">Priority</div>\r\n        </li>\r\n        <div class=\"myClear\"></div>\r\n        <li>\r\n            <div class=\"T1 pull-left\"><a href=\"${config.siteUrl}\" title=\"${config.siteName}\">${config.siteName} | 一个程序员的个人博客</a></div>\r\n            <div class=\"T2 pull-right\">${.now?string(\"yyyy-MM-dd\")}</div>\r\n            <div class=\"T3 pull-right\">daily</div>\r\n            <div class=\"T4 pull-right\">1</div>\r\n        </li>\r\n        <div class=\"myClear\"></div>\r\n        <li>\r\n            <div class=\"T1 pull-left\"><a href=\"${config.siteUrl}/about\" title=\"${config.siteName}\">关于 | ${config.siteName}</a></div>\r\n            <div class=\"T2 pull-right\">${.now?string(\"yyyy-MM-dd\")}</div>\r\n            <div class=\"T3 pull-right\">daily</div>\r\n            <div class=\"T4 pull-right\">0.6</div>\r\n        </li>\r\n        <div class=\"myClear\"></div>\r\n        <li>\r\n            <div class=\"T1 pull-left\"><a href=\"${config.siteUrl}/links\" title=\"${config.siteName}\">友情链接 | ${config.siteName}</a></div>\r\n            <div class=\"T2 pull-right\">${.now?string(\"yyyy-MM-dd\")}</div>\r\n            <div class=\"T3 pull-right\">daily</div>\r\n            <div class=\"T4 pull-right\">0.6</div>\r\n        </li>\r\n        <div class=\"myClear\"></div>\r\n        <li>\r\n            <div class=\"T1 pull-left\"><a href=\"${config.siteUrl}/guestbook\" title=\"${config.siteName}\">留言板 | ${config.siteName}</a></div>\r\n            <div class=\"T2 pull-right\">${.now?string(\"yyyy-MM-dd\")}</div>\r\n            <div class=\"T3 pull-right\">daily</div>\r\n            <div class=\"T4 pull-right\">0.6</div>\r\n        </li>\r\n        <div class=\"myClear\"></div>\r\n        <li>\r\n            <div class=\"T1 pull-left\"><a href=\"${config.siteUrl}/updateLog\" title=\"${config.siteName}\">网站更新记录 | ${config.siteName}</a></div>\r\n            <div class=\"T2 pull-right\">${.now?string(\"yyyy-MM-dd\")}</div>\r\n            <div class=\"T3 pull-right\">daily</div>\r\n            <div class=\"T4 pull-right\">0.6</div>\r\n        </li>\r\n		<div class=\"myClear\"></div>\r\n        <li>\r\n            <div class=\"T1 pull-left\"><a href=\"${config.siteUrl}/recommended\" title=\"${config.siteName}\">站长推荐 | ${config.siteName}</a></div>\r\n            <div class=\"T2 pull-right\">${.now?string(\"yyyy-MM-dd\")}</div>\r\n            <div class=\"T3 pull-right\">daily</div>\r\n            <div class=\"T4 pull-right\">0.6</div>\r\n        </li>\r\n        <div class=\"myClear\"></div>\r\n        <#list articleList as item>\r\n            <li>\r\n                <div class=\"T1 pull-left\"><a href=\"${config.siteUrl}/article/${item.id}\" title=\"${item.title}\">${item.title} | ${config.siteName}</a></div>\r\n                <div class=\"T2 pull-right\">${item.updateTime?string(\"yyyy-MM-dd\")}</div>\r\n                <div class=\"T3 pull-right\">daily</div>\r\n                <div class=\"T4 pull-right\">0.6</div>\r\n            </li>\r\n            <div class=\"myClear\"></div>\r\n        </#list>\r\n    </ul>\r\n</div>\r\n<div id=\"content\">\r\n    <h3>分类目录</h3>\r\n    <ul id=\"myTable\">\r\n        <#list articleTypeList as item>\r\n            <li>\r\n                <div class=\"T1 pull-left\"><a href=\"${config.siteUrl}/type/${item.id}\" title=\"${item.name}\">${item.name} | ${config.siteName}</a></div>\r\n                <div class=\"T2 pull-right\">${item.updateTime?string(\"yyyy-MM-dd\")}</div>\r\n                <div class=\"T3 pull-right\">daily</div>\r\n                <div class=\"T4 pull-right\">0.6</div>\r\n            </li>\r\n            <div class=\"myClear\"></div>\r\n        </#list>\r\n    </ul>\r\n</div>\r\n<div id=\"content\">\r\n    <h3>标签目录</h3>\r\n    <ul id=\"myTable\">\r\n        <#list articleTagsList as item>\r\n            <li>\r\n                <div class=\"T1 pull-left\"><a href=\"${config.siteUrl}/tag/${item.id}\" title=\"${item.name}\">${item.name} | ${config.siteName}</a></div>\r\n                <div class=\"T2 pull-right\">${item.updateTime?string(\"yyyy-MM-dd\")}</div>\r\n                <div class=\"T3 pull-right\">daily</div>\r\n                <div class=\"T4 pull-right\">0.6</div>\r\n            </li>\r\n            <div class=\"myClear\"></div>\r\n        </#list>\r\n    </ul>\r\n</div>\r\n<div id=\"footer\">\r\n    该文件由<a href=\"${config.siteUrl}\" title=\"${config.siteName}\">${config.siteName}</a>网站自动生成。\r\n</div>\r\n</body>\r\n</html>', '2018-01-19 10:47:43', '2018-03-13 17:40:52');
INSERT INTO `sys_template` VALUES ('4', 'TM_ROBOTS', 'Crawl-delay: 5\r\nSitemap: https://www.zhyd.me/sitemap.txt\r\nSitemap: https://www.zhyd.me/sitemap.xml\r\nSitemap: https://www.zhyd.me/sitemap.html\r\n', '2018-01-19 10:48:27', '2018-01-19 10:48:27');
INSERT INTO `sys_template` VALUES ('5', 'TM_LINKS', '<!DOCTYPE html>\r\n<html lang=\"en\">\r\n<head>\r\n    <meta charset=\"UTF-8\">\r\n    <title>友情链接操作通知</title>\r\n</head>\r\n<body>\r\n<div style=\"border-radius:5px;font-size:13px;width:680px;font-family:微软雅黑,\'Helvetica Neue\',Arial,sans-serif;margin:10px auto 0px;border:1px solid #eee;max-width:100%\">\r\n    <div style=\"width:100%;background:#2f889a;color:#ffffff;border-radius:5px 5px 0 0\">\r\n        <p style=\"font-size:15px;word-break:break-all;padding:20px 32px;margin:0\">\r\n            友情链接操作通知\r\n        </p>\r\n    </div>\r\n    <div style=\"margin:0px auto;width:90%\">\r\n        <p>站长<a href=\"${link.url?if_exists}\" target=\"_blank\">${link.name?if_exists}</a>，您好!</p>\r\n        <p>您于 ${link.createTime?string(\'yyyy-MM-dd HH:mm:ss\')} 提交的友链申请已通过系统审核。以下为您提交的信息，请确认（如有误，请联系我）！</p>\r\n        <p>\r\n        <ul>\r\n            <li>${link.name?if_exists}</li>\r\n            <li>${link.url?if_exists}</li>\r\n            <li>${link.description?if_exists}</li>\r\n            <li>${link.email?if_exists}</li>\r\n            <li>${link.qq?if_exists}</li>\r\n            <li><img src=\"${link.favicon?if_exists}\" width=\"100\" alt=\"LOGO\"></li>\r\n        </ul>\r\n        </p>\r\n        <p>本站会不定期检查连接有效性，如果因为贵站改版、服务到期等原因导致无法正常访问的，我会暂时停掉贵站友链，待贵站可以正常访问后，本站会继续启用贵站友链。</p>\r\n        <p>特别注意：以下情况，本站将在不做任何通知下，<strong>取消友链</strong>！</p>\r\n        <ul>\r\n            <li>私自取消本站友情链接</li>\r\n            <li>更换域名且未通知本站</li>\r\n            <li>网站内容长期不更新</li>\r\n            <li>友链上使用诸如nofollow之类的属性</li>\r\n        </ul>\r\n        <p>感谢您对 <a style=\"text-decoration:none;\" href=\"${config.siteUrl?if_exists}\" target=\"_blank\">${config.siteName?if_exists}</a> 的关注，如您有任何疑问，欢迎来我网站<a style=\"text-decoration:none;\" href=\"${config.siteUrl}/guestbook\" target=\"_blank\">留言</a>。</p>\r\n        <p>（注：此邮件由系统自动发出，请勿回复。）</p>\r\n    </div>\r\n    <div class=\"adL\">\r\n    </div>\r\n</div>\r\n</body>\r\n</html>', '2018-01-19 10:48:54', '2018-01-19 10:48:54');
INSERT INTO `sys_template` VALUES ('6', 'TM_COMMENT_AUDIT', '<!DOCTYPE html>\r\n<html lang=\"en\">\r\n<head>\r\n    <meta charset=\"UTF-8\">\r\n    <title>评论审核结果通知</title>\r\n</head>\r\n<body>\r\n<div style=\"border-radius:5px;font-size:13px;width:680px;font-family:微软雅黑,\'Helvetica Neue\',Arial,sans-serif;margin:10px auto 0px;border:1px solid #eee;max-width:100%\">\r\n    <div style=\"width:100%;background:#2f889a;color:#ffffff;border-radius:5px 5px 0 0\">\r\n        <p style=\"font-size:15px;word-break:break-all;padding:20px 32px;margin:0\">\r\n            评论审核结果通知\r\n        </p>\r\n    </div>\r\n    <div style=\"margin:0px auto;width:90%\">\r\n        <p>${comment.nickname?if_exists}，您好!</p>\r\n        <p>\r\n            您于 ${comment.createTime?string(\'yyyy-MM-dd HH:mm:ss\')} 在文章《${config.siteUrl?if_exists}${comment.sourceUrl?if_exists}》 上发表的<span class=\"il\">评论</span>：\r\n        </p>\r\n        <div style=\"background:#efefef;margin:15px 0px;padding:1px 15px;border-radius:5px;font-size:14px;color:#333\">${comment.content}</div>\r\n        <#if comment.status == \'APPROVED\'>\r\n        <p>已通过管理员审核并显示。</p>\r\n        <p>\r\n            您可以点击 <a style=\"text-decoration:none;\" href=\"${config.siteUrl}${comment.sourceUrl}\" target=\"_blank\">链接</a>查看回复的完整內容。\r\n        </p>\r\n        <#elseif comment.status == \'REJECT\'>\r\n        <p>审核失败！失败原因：</p>\r\n        <p style=\"background:#efefef;margin:15px 0px;padding:1px 15px;border-radius:5px;font-size:14px;color:#333\">${comment.remark}</p>\r\n        <#elseif comment.status == \'DELETED\'>\r\n        <p>已被管理员删除！删除原因：</p>\r\n        <p style=\"background:#efefef;margin:15px 0px;padding:1px 15px;border-radius:5px;font-size:14px;color:#333\">${comment.remark}</p>\r\n        <#else>\r\n        <p>管理员正在审核中！审核通过后将给您及时发送通知！</p>\r\n        </#if>\r\n        <p>感谢您对 <a style=\"text-decoration:none;\" href=\"${config.siteUrl}\" target=\"_blank\">${config.siteName}</a> 的关注，如您有任何疑问，欢迎来我网站<a style=\"text-decoration:none;\" href=\"${config.siteUrl}/guestbook\" target=\"_blank\">留言</a>。</p>\r\n        <p>（注：此邮件由系统自动发出，请勿回复。）</p>\r\n    </div>\r\n    <div class=\"adL\">\r\n    </div>\r\n</div>\r\n</body>\r\n</html>', '2018-01-26 21:17:38', '2018-01-26 21:17:38');
INSERT INTO `sys_template` VALUES ('7', 'TM_COMMENT_REPLY', '<!DOCTYPE html>\r\n<html lang=\"en\">\r\n<head>\r\n    <meta charset=\"UTF-8\">\r\n    <title>评论回复通知</title>\r\n</head>\r\n<body>\r\n<div style=\"border-radius:5px;font-size:13px;width:680px;font-family:微软雅黑,\'Helvetica Neue\',Arial,sans-serif;margin:10px auto 0px;border:1px solid #eee;max-width:100%\">\r\n    <div style=\"width:100%;background:#2f889a;color:#ffffff;border-radius:5px 5px 0 0\">\r\n        <p style=\"font-size:15px;word-break:break-all;padding:20px 32px;margin:0\">\r\n            评论回复通知\r\n        </p>\r\n    </div>\r\n    <div style=\"margin:0px auto;width:90%\">\r\n        <p>${comment.nickname}，您好!</p>\r\n        <p>\r\n            您于 ${comment.createTime?string(\'yyyy-MM-dd HH:mm:ss\')} 在文章《${config.siteUrl}${comment.sourceUrl}》 上发表的<span class=\"il\">评论</span>：\r\n        </p>\r\n        <div style=\"background:#efefef;margin:15px 0px;padding:1px 15px;border-radius:5px;font-size:14px;color:#333\">${comment.content}</div>\r\n        <p>\r\n            有了 <strong>新的回复</strong>！您可以点击 <a style=\"text-decoration:none;\" href=\"${config.siteUrl}${comment.sourceUrl}\" target=\"_blank\">链接</a>查看回复的完整內容。\r\n        </p>\r\n        <p>感谢您对 <a style=\"text-decoration:none;\" href=\"${config.siteUrl}\" target=\"_blank\">${config.siteName}</a> 的关注，如您有任何疑问，欢迎来我网站<a style=\"text-decoration:none;\" href=\"${config.siteUrl}/guestbook\" target=\"_blank\">留言</a>。</p>\r\n        <p>（注：此邮件由系统自动发出，请勿回复。）</p>\r\n    </div>\r\n    <div class=\"adL\">\r\n    </div>\r\n</div>\r\n</body>\r\n</html>', '2018-01-26 21:17:55', '2018-01-26 21:17:55');
INSERT INTO `sys_template` VALUES ('8', 'TM_LINKS_TO_ADMIN', '<!DOCTYPE html>\r\n<html lang=\"en\">\r\n<head>\r\n    <meta charset=\"UTF-8\">\r\n    <title>友情链接操作通知</title>\r\n</head>\r\n<body>\r\n<div style=\"border-radius:5px;font-size:13px;width:680px;font-family:微软雅黑,\'Helvetica Neue\',Arial,sans-serif;margin:10px auto 0px;border:1px solid #eee;max-width:100%\">\r\n    <div style=\"width:100%;background:#2f889a;color:#ffffff;border-radius:5px 5px 0 0\">\r\n        <p style=\"font-size:15px;word-break:break-all;padding:20px 32px;margin:0\">\r\n            友情链接操作通知\r\n        </p>\r\n    </div>\r\n    <div style=\"margin:0px auto;width:90%\">\r\n        <p>有新的友情链接加入，信息如下</p>\r\n        <p>\r\n        <ul>\r\n            <li>${link.name?if_exists}</li>\r\n            <li>${link.url?if_exists}</li>\r\n            <li>${link.description?if_exists}</li>\r\n            <#if link.favicon?exists><li><img src=\"${link.favicon?if_exists}\" width=\"100\" alt=\"LOGO\"></li></#if>\r\n        </ul>\r\n        </p>\r\n        <p><a style=\"text-decoration:none;\" href=\"http://admin.zhyd.me\" target=\"_blank\">去后台继续审核</a>。</p>\r\n        <p>（注：此邮件由系统自动发出，请勿回复。）</p>\r\n    </div>\r\n    <div class=\"adL\">\r\n    </div>\r\n</div>\r\n</body>\r\n</html>', '2018-03-31 14:14:11', '2018-03-31 14:14:11');
INSERT INTO `sys_template` VALUES ('9', 'TM_NEW_COMMENT', '<!DOCTYPE html>\r\n<html lang=\"en\">\r\n<head>\r\n    <meta charset=\"UTF-8\">\r\n    <title>新评论通知</title>\r\n</head>\r\n<body>\r\n<div style=\"border-radius:5px;font-size:13px;width:680px;font-family:微软雅黑,\'Helvetica Neue\',Arial,sans-serif;margin:10px auto 0px;border:1px solid #eee;max-width:100%\">\r\n    <div style=\"width:100%;background:#2f889a;color:#ffffff;border-radius:5px 5px 0 0\">\r\n        <p style=\"font-size:15px;word-break:break-all;padding:20px 32px;margin:0\">\r\n            新评论通知\r\n        </p>\r\n    </div>\r\n    <div style=\"margin:0px auto;width:90%\">\r\n        <p>\r\n            评论内容：\r\n        </p>\r\n        <div style=\"background:#efefef;margin:15px 0px;padding:1px 15px;border-radius:5px;font-size:14px;color:#333\"><#if comment?exists>${comment.content}<#else>**无评论内容**</#if></div>\r\n        <p>\r\n            <a style=\"text-decoration:none;\" href=\"${config.siteUrl}${comment.sourceUrl}\" target=\"_blank\">查看完整评论</a>\r\n        </p>\r\n        <p>（注：此邮件由系统自动发出，请勿回复。）</p>\r\n    </div>\r\n    <div class=\"adL\">\r\n    </div>\r\n</div>\r\n</body>\r\n</html>', '2018-03-31 14:19:04', '2018-03-31 14:19:04');

-- ----------------------------
-- Table structure for sys_update_recorde
-- ----------------------------
DROP TABLE IF EXISTS `sys_update_recorde`;
CREATE TABLE `sys_update_recorde` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `version` varchar(20) DEFAULT NULL COMMENT '更新版本',
  `description` varchar(2500) DEFAULT NULL COMMENT '更新记录备注',
  `recorde_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '项目更新时间',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_update_recorde
-- ----------------------------
INSERT INTO `sys_update_recorde` VALUES ('1', '1.0.1', '第一版', '2018-01-19 22:16:40', '2018-01-19 22:16:40', '2018-01-19 22:16:40');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL COMMENT '登录密码',
  `nickname` varchar(30) DEFAULT '' COMMENT '昵称',
  `mobile` varchar(30) DEFAULT NULL COMMENT '手机号',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱地址',
  `qq` varchar(20) DEFAULT NULL COMMENT 'QQ',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `gender` tinyint(2) unsigned DEFAULT NULL COMMENT '性别',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像地址',
  `user_type` enum('ROOT','ADMIN','USER') DEFAULT 'ADMIN' COMMENT '超级管理员、管理员、普通用户',
  `company` varchar(100) DEFAULT NULL COMMENT '公司',
  `blog` varchar(255) DEFAULT NULL COMMENT '个人博客地址',
  `location` varchar(255) DEFAULT NULL COMMENT '地址',
  `source` enum('GITHUB','WEIBO','QQ','ZHYD') DEFAULT 'ZHYD' COMMENT '用户来源(默认ZHYD=本网站注册用户)',
  `privacy` tinyint(2) DEFAULT NULL COMMENT '隐私（1：公开，0：不公开）',
  `notification` tinyint(2) unsigned DEFAULT NULL COMMENT '通知：(1：通知显示消息详情，2：通知不显示详情)',
  `score` int(10) unsigned DEFAULT '0' COMMENT '金币值',
  `experience` int(10) unsigned DEFAULT '0' COMMENT '经验值',
  `reg_ip` varchar(30) DEFAULT NULL COMMENT '注册IP',
  `last_login_ip` varchar(30) DEFAULT NULL COMMENT '最近登录IP',
  `last_login_time` datetime DEFAULT NULL COMMENT '最近登录时间',
  `login_count` int(10) unsigned DEFAULT '0' COMMENT '登录次数',
  `remark` varchar(100) DEFAULT NULL COMMENT '用户备注',
  `status` int(1) unsigned DEFAULT NULL COMMENT '用户状态',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('36', 'admin', 'DC44B82A2A999917B8E098EB7AA449B1', '超级管理员', '13999923333', '2228114147@qq.com', '2228114147', null, null, null, 'ADMIN', null, null, null, 'ZHYD', null, null, '0', '0', null, null, null, '0', null, null, '2018-06-06 20:34:53', '2018-06-06 20:34:53');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1', '1', '2018-01-02 10:47:27', '2018-01-02 10:47:27');
INSERT INTO `sys_user_role` VALUES ('5', '26', '1', '2018-05-31 18:37:02', '2018-05-31 18:37:02');
INSERT INTO `sys_user_role` VALUES ('9', '31', '1', '2018-06-06 09:23:59', '2018-06-06 09:23:59');
INSERT INTO `sys_user_role` VALUES ('21', '2', '2', '2018-06-06 15:58:47', '2018-06-06 15:58:47');
INSERT INTO `sys_user_role` VALUES ('22', '35', '3', '2018-06-06 16:00:57', '2018-06-06 16:00:57');
INSERT INTO `sys_user_role` VALUES ('23', '30', '1', '2018-06-06 20:33:39', '2018-06-06 20:33:39');
INSERT INTO `sys_user_role` VALUES ('25', '36', '1', '2018-06-06 20:34:58', '2018-06-06 20:34:58');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '20', '190');
INSERT INTO `user` VALUES ('2', '21', '195');
INSERT INTO `user` VALUES ('3', '19', '199');
INSERT INTO `user` VALUES ('3', '19', '169');
