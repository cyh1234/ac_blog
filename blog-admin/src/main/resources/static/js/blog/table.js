//遍历表单， 根据值填充表单
//修改时： 标签为flag的设为readonly, 表示此项是不可更改的. 其他的值直接填充
//添加时： 清空所有组件的值， 并将readonly去掉
function resetForm(info) {
    if (info == '' || info == null){
        info = {};
    }
    $("#addOrUpdateForm").find('input,textarea,select').each(function(){
        var name = $(this).attr('name');
        var value = '';
        try {
            value = info[name];
        }catch (err){
        }
        console.log(name +"---"+value);

        if ( (this.type == 'textarea' || this.type == 'number' || this.type == 'select-one' ||this.type == "email" || this.type == "text" || this.type == "tel")){
            $(this).val(value);
            console.log($(this).val());
        }
        if (this.type == 'select-one' && value == null){
            $(this).val(0);
        }
        if (this.type == "password"){
            $(this).val('');
        }
        if ($(this).attr("flag") == "readonly"){
            if (value == null    || value == ''){
                $(this).removeAttr('readonly');
            }else{
                $(this).attr("readonly", "readonly");
            }
        }
    });
}

//得到所有选中的id
function getSelectedId() {
    var selectedJson = $("#mytable").bootstrapTable('getAllSelections');
    var ids = []
    $.each(selectedJson, function (i) {
        ids.push(selectedJson[i].id);
    })
    return ids;
}

//表格对象
var TableInit = function() {
    var tableInit = new Object();
    tableInit.init = function (options) {
        $('#mytable').bootstrapTable({
            url: options.url,
            method: 'post',                      //请求方式（*）
            //toolbar: '#toolbar',                //工具按钮用哪个容器
            striped: true,                      //是否显示行间隔色
            cache: true,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
            contentType: "application/x-www-form-urlencoded", // 发送到服务器的数据编码类型, application/x-www-form-urlencoded为了实现post方式提交
            sortable: false,                     //是否启用排序
            sortOrder: "asc",                   //排序方式
            sortStable: true,                   // 设置为 true 将获得稳定的排序
            queryParams: tableInit.queryParams,//传递参数（*）
            queryParamsType: '',
            pagination: true,                   //是否显示分页（*）
            sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
            pageNumber: 1,                       //初始化加载第一页，默认第一页
            pageSize: 10,                       //每页的记录行数（*）
            pageList: [10, 20, 30, 50, 100],        //可供选择的每页的行数（*）
            search: true,                       //是否启用搜索框 根据sidePagination选择从前后台搜索
            strictSearch: true,                 //设置为 true启用 全匹配搜索，否则为模糊搜索
            searchOnEnterKey: true,            // 设置为 true时，按回车触发搜索方法，否则自动触发搜索方法
            minimumCountColumns: 1,             //最少允许的列数
            showColumns: true,                  //是否显示 内容列下拉框
            showRefresh: true,                  //是否显示刷新按钮
            showToggle: true,                   //是否显示详细视图和列表视图的切换按钮
            columns: options.columns
        })
    };
    tableInit.queryParams= function (params) {
        params = {
            'pageNumber': params.pageNumber,
            'pageSize': params.pageSize,
            'sortOrder': "asc"
        }
        return params;
    };

    return tableInit;
};

var ButtonInit = function(){
    var buttonInit = new Object();
    buttonInit.init = function (options) {
        //添加信息， 显示模态， 添加保存按钮的点击事件
        $('#btn_add').on('click',function(){
            //$('#addOrUpdateForm').get(0).reset();
            resetForm();
           // $(this).removeAttr('readonly');
            $('#addOrUpdateModal').find('.modal-title').html('添加'+options.modalName);
            $('#addOrUpdateModal').modal("show");
            bindSaveInfoEvent(options.createUrl, options.url);
        });

        //修改
        $('#mytable').on('click','.btn-update',function(){
            var id =  $(this).attr('data-id');
            //获取信息，填充模态
            $.ajax({
                type: 'post',
                url: options.getInfoUrl.replace("{id}",id),
                success: function (msg) {
                    var data = msg.data.data;
                    console.log(data);
                    resetForm(data);
                    $('input[name="id"]').val(data.id);
                    $('#addOrUpdateModal').find('.modal-title').html('修改'+options.modalName);
                    $('#addOrUpdateModal').modal('show');
                    bindSaveInfoEvent(options.updateUrl, options.url);
                }
            });
        });

        //移除一个
        $('#mytable').on('click','.btn-remove',function(){
            var id =  $(this).attr('data-id');
            remove(id);
        });

        //删除批量
        $('#btn_delete_ids').on('click',function(){
            var ids = getSelectedId();
            remove(ids);
        })
        
        function remove(ids) {
            console.log(options.removeUrl);
            $.confirm({
                title:'提示',
                content:'确定要删除吗？',
                buttons: {
                    info: {
                        text: '确定',
                        btnClass: 'btn-blue',
                        action: function(){
                            $.ajax({
                                type: 'post',
                                url: options.removeUrl,
                                traditional: true,
                                data: {'ids': ids},
                                success: function (msg) {
                                    $("#mytable").bootstrapTable('refresh', {url: options.url});
                                    $.alert(msg.message);
                                }
                            });
                        }
                    },
                    heyThere: {
                        text: '取消',
                    }
                }
            });
        }

    };
    return buttonInit;
};


//保存or修改按钮事件, 提交的url ，  刷新的url
function bindSaveInfoEvent(url, refreshUrl) {
    $(".addOrUpdateBtn").unbind('click');
    $('.addOrUpdateBtn').on('click',function(){
        if ($("#addOrUpdateForm").valid()){
            $.ajax({
                type: 'post',
                url: url,
                data: $("#addOrUpdateForm").serialize(),
                success: function (msg) {
                    $.alert(msg.message);
                    $('#addOrUpdateModal').modal("hide");
                    $("#mytable").bootstrapTable('refresh', {url: refreshUrl});
                }
            });
        }
    });
}