// var E = window.wangEditor;
var editor =  new wangEditor('#editor');
function initEdit() {
    editor.customConfig.uploadImgServer = '/upload/edit-img'; //上传URL
    editor.customConfig.uploadImgMaxSize = 3 * 1024 * 1024;
    editor.customConfig.uploadImgMaxLength = 5;
    editor.customConfig.uploadFileName = 'edit-img'; //和后台的对应
    editor.customConfig.uploadImgHooks = {
        customInsert: function (insertImg, result, editor) {
            // 图片上传并返回结果，自定义插入图片的事件（而不是编辑器自动插入图片！！！）
            // insertImg 是插入图片的函数，editor 是编辑器对象，result 是服务器端返回的结果
            var url = result.data.data;
            // 举例：假如上传图片成功后，服务器端返回的是 {url:'....'} 这种格式，即可这样插入图片：
            insertImg(url);
            // result 必须是一个 JSON 格式字符串！！！否则报错
        }
    }
    editor.customConfig.zIndex = 1;
    editor.create();
}

//加载分类和标签
function initData() {

    //加载分类
    $.ajax({
        type: 'post',
        url: '/type/getAll',
        success: function (msg) {
           if (msg.code == "1"){
               var data = msg.data.data;
               var html = "";
               $.each(data, function(i){
                   html += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
               });
               $('#typeId').html(html);
           }
        }
    });

    //加载标签
    $.ajax({
        type: 'post',
        url: '/tag/getAll',
        success: function (msg) {
            if (msg.code == "1"){
                var data = msg.data.data;
                var html = "";
                $.each(data, function(i){
                    html += '<li><label>' +
                        '<input type="checkbox" name="ts" class="flat" value="'+data[i].id+'"/>' +data[i].name
                        '</label></li>';
                });
                $('#tag-list').html(html);
            }
        }
    });


    $('#file-upload-btn').on('click',function(){
        $('#chooseImg').modal('show');
    });

    $('#tab-btn').on('click',function(){
        $('#cover-img-file').click();
    });

    $('.publishBtn').on('click',function () {

        //获取编辑其内容，填充到textarea
        $('#content').val(editor.txt.html());
        console.log($("#publishForm").serialize());
        $.ajax({
            type: 'post',
            url: '/article/save',
            traditional:true,
            data: $("#publishForm").serialize(),
            success: function (msg) {
                if (msg.code == "1"){

                    $.confirm({
                        title:'提示',
                        content:'保存成功',
                        buttons: {
                            info: {
                                text: '确定',
                                btnClass: 'btn-blue',
                                action: function () {
                                    window.location.href = "/articles";
                                }
                            }
                        }
                    });
                }
            }
        });
    });
}


//编辑文章初始化
function editInit(id) {
    $.ajax({
        type: 'post',
        url: '/article/get/'+id,
        success: function (msg) {
            if (msg.code == "1"){
                var data = msg.data.data;
                editor.txt.html(data.content);
                $('input[name="title"]').val(data.title);
                data.original == 1 ? $('input[name="original"]').prop("checked","true") : $('input[name="original"]').prop("checked","false");
                $('img[name="coverImg"]').prop('src',data.coverImage);
                $('img[name="coverImg"]').show();
                $('#typeId').val(data.typeId);
                $.each(data.tags, function(i,v){
                    var tag =  $('input[name=ts][value='+v.id+']');
                    tag.prop('checked', true);
                })
                if (data.status == 1){
                    $('input[name=status][value=1]').prop('checked',true);
                }else{
                    $('input[name=status][value=0]').prop('checked',true);
                }
                $('#description').val(data.description);
                $('#keywords').val(data.keywords);
            }
        }
    });
}


/**
 * 上传封面
 */
function uploadCover() {
    var cover = $('#cover-img-file').val();
    var formData = new FormData($("#publishForm")[0]);

    $('img[name="coverImg"]').hide();
    $('#loadingImg').show(0);

    setTimeout(function () {
        $.ajax({
            url: '/upload/cover-img',
            type: 'post',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (msg) {
                var path = msg.data.data;
                $('#loadingImg').hide();
                $('img[name="coverImg"]').attr('src',path);
                $('input[name="coverImage"]').val(path);
                $('img[name="coverImg"]').show();
            }
        });
    }, 100);
}