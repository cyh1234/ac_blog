<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title> - 登录</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="/static/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/css/font-awesome.css?v=4.4.0" rel="stylesheet">

    <link href="/static/css/animate.css" rel="stylesheet">
    <link href="/static/css/style.css?v=4.1.0" rel="stylesheet">
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <script>if(window.top !== window.self){ window.top.location = window.location;}</script>
</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen  animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name">C</h1>
        </div>
        <h3>欢迎使用 hAdmin 当前用户:${username}</h3>

        <form class="m-t" role="form" id="loginForm">
            <div class="form-group">
                <input type="email" class="form-control" name="username" placeholder="用户名" required="">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" placeholder="密码" required="">
            </div>
            <div class="form-group" style="text-align: left">
                <label class="radio-inline">
                    <input id="leavetype" name="rememberMe" type="checkbox">记住我
                </label>
            </div>

            <button id="loginBtn" class="btn btn-primary block full-width m-b">登 录</button>


            <#--<p class="text-muted text-center"> <a href="login.html#"><small>忘记密码了？</small></a> | <a href="register.html">注册一个新账号</a>-->
            <#--</p>-->

        </form>
    </div>
</div>

<!-- 全局js -->
<script src="/static/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/js/bootstrap.min.js?v=3.3.6"></script>

</body>

<script>
    $(function(){
        $("#loginBtn").on("click",function () {
            $.ajax({
                type: 'post',
                url: '/passport/signin',
                data: $("#loginForm").serialize(),
                success: function (msg) {
                    if (msg.code == "0"){
                        $.alert(msg.message)
                    }else{
                        window.location.href = "/";
                    }
                },
                error:function () {
                    console.log(1);
                }
            });
        });
    })
</script>

</html>
