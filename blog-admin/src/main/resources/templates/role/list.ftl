<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title> - 角色列表</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="/static/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/css/font-awesome.css?v=4.4.0" rel="stylesheet">

    <link href="/static/css/animate.css" rel="stylesheet">
    <#--<link href="/static/css/style.css?v=4.1.0" rel="stylesheet">-->
    <!--提示框-->
    <link rel="stylesheet" href="/static/css/jquery-confirm.min.css">

    <link rel="stylesheet" href="/static/css/zTree/demo.css" type="text/css">
    <#--<link rel="stylesheet" href="/static/css/zTree/zTreeStyle/zTreeStyle.css" type="text/css">-->
    <#--<link href="https://cdn.bootcss.com/zTree.v3/3.5.29/css/metroStyle/metroStyle.min.css" rel="stylesheet">-->

    <link rel="stylesheet" href="/static/css/zTree/demo.css" type="text/css">
    <link rel="stylesheet" href="/static/css/zTree/zTreeStyle/zTreeStyle.css" type="text/css">


    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
</head>

<body>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="/">首页</a></li>
            <li class="active">角色管理</li>
        </ol>
        <div class="x_panel">
            <div class="x_content">
                <div class="<#--table-responsive-->">
                    <div class="btn-group hidden-xs" id="toolbar">
                        <button id="btn_add" type="button" class="btn btn-default" title="新增角色">
                            <i class="fa fa-plus"></i> 新增角色
                        </button>
                        <button id="btn_delete_ids" type="button" class="btn btn-default" title="删除选中">
                            <i class="fa fa-trash-o"></i> 批量删除
                        </button>
                    </div>
                    <table id="mytable">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!--添加or修改角色弹框-->
<div class="modal fade" id="addOrUpdateModal" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addroleLabel">添加角色</h4>
            </div>
            <div class="modal-body">
                <form id="addOrUpdateForm" class="form-horizontal form-label-left" novalidate>
                    <input type="hidden" name="id">
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >角色名: <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" name="description" id="description" minlength="2" required placeholder="请输入角色名"/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">是否可用 <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="available"  class="form-control">
                                <option value="1">可用</option>
                                <option value="0">禁用</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary addOrUpdateBtn">保存</button>
            </div>
        </div>
    </div>
</div>
<!--/添加角色弹框-->


<#--给角色分配资源模态-->
<div class="modal fade" id="allotModal" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addroleLabel">分配资源</h4>
            </div>
            <div class="modal-body">
                <form id="boxRoleForm">
                    <div class="treeClass">
                        <ul id="treeDemo" class="ztree"></ul>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<!-- 全局js -->
<script src="/static/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/js/bootstrap.min.js?v=3.3.6"></script>
<!-- bootstrap-table.min.js -->
<script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/bootstrap-table.min.js"></script>
<!-- 引入中文语言包 -->
<script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/locale/bootstrap-table-zh-CN.min.js"></script>
<!--myjs-->
<script src="/static/js/blog/table.js"></script>
<!--验证-->
<#--<script src="/static/js/validator.js"></script>-->
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
<!--提示框-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

<#--<script type="text/javascript" src="/static/js/zTree/jquery.ztree.core.js"></script>-->
<#--<script type="text/javascript" src="/static/js/zTree/jquery.ztree.excheck.js"></script>-->

<#--<script src="https://cdn.bootcss.com/zTree.v3/3.5.29/js/jquery.ztree.core.min.js"></script>-->
<#--<script src="https://cdn.bootcss.com/zTree.v3/3.5.29/js/jquery.ztree.excheck.min.js"></script>-->
<script type="text/javascript" src="/static/js/zTree/jquery.ztree.core.js"></script>
<script type="text/javascript" src="/static/js/zTree/jquery.ztree.excheck.js"></script>

</body>

<script>

    //自定义按钮
    function operateFormatter(code, row, index) {
        var userid = ${user.id}; //当前登录的角色
        var btns = [
            '<button class="btn btn-info btn-update"  data-id="'+row.id+'">编辑</button>&nbsp'
        ]
        if (row.id != userid){
            btns.push('<button class="btn btn-danger btn-remove" data-id="'+row.id+'">删除</button>&nbsp');
            btns.push('<button class="btn btn-success btn-allot" data-id="'+row.id+'">分配权限</button>');
        }
        return btns.join('');
    };

    $(function(){

        $("#addOrUpdateForm").validate();
        //供table.js 使用
        var options = {
            url: "/role/list",
            createUrl: "/role/add", //添加角色
            getInfoUrl: "/role/get/{id}", //获取角色信息
            updateUrl: "/role/edit",
            removeUrl: "/role/remove",
            columns: [
                {
                    checkbox: true
                }, {
                    field: 'description',
                    title: '角色名',
                    editable: false,
                }, {
                    field: 'nickname',
                    title: '是否可用',
                    editable: true,
                    formatter: function (code, row, index) {
                        console.log('----');
                        console.log(row);
                        var status = row.available == 1 ? "可用" : "禁用";
                        return status;
                    }
                }, {
                    field: 'operate',
                    title: '操作',
                    formatter: operateFormatter
                }
            ],
            modalName: "角色"
        };

        //初始化表格对象，按钮
        var table = new TableInit();
        var button = new ButtonInit();
        table.init(options);
        button.init(options);
    });

    /**
     * 分配权限
     */
    $('#mytable').on('click', '.btn-allot', function(){
        var roleId = $(this).attr('data-id');
        $.ajax({
            type: 'post',
            url: '/resources/resourcesSelected',
            data: {'roleId':roleId},
            success: function (msg) {
                var result = msg.data.data;
                console.log(result);
                var setting = {
                    view: {
                        selectedMulti: false
                    },
                    check: {
                        enable: true
                    },
                    data: {
                        simpleData: {
                            enable: true
                        }
                    },
                    callback: {
                        onCheck: function (event, treeId, treeNode) {
                            //得到操作树对象
                            var obj = $.fn.zTree.getZTreeObj(treeId);
                            var nodes = obj.getCheckedNodes(true); //true全部选中， false非选中
                            var ids = new Array();
                            console.log(nodes);
                            for (var i=0; i<nodes.length; i++){
                                console.log(nodes[i]);
                                ids.push(nodes[i].id);
                            }
                            $.ajax({
                                type: 'post',
                                url: '/role/saveRoleResource',
                                data: {"ids": ids.join(','), 'roleId': roleId},
                                success: function (data) {
                                    console.log(data);
                                }
                            });
                        }
                    }
                };
                //初始化
                var tree = $.fn.zTree.init($("#treeDemo"), setting, result);
                $('#allotModal').modal('show');
                tree.expandAll(true);//全部展开
            }
        });
    });
</script>

</html>
