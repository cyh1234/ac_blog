<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title> - 资源列表</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="/static/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/css/font-awesome.css?v=4.4.0" rel="stylesheet">

    <link href="/static/css/animate.css" rel="stylesheet">
    <link href="/static/css/style.css?v=4.1.0" rel="stylesheet">
    <!--提示框-->
    <link rel="stylesheet" href="/static/css/jquery-confirm.min.css">
    <#--ztree-->
    <link href="https://cdn.bootcss.com/zTree.v3/3.5.33/css/metroStyle/metroStyle.css" rel="stylesheet">


    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
</head>

<body>


<div class="panel panel-default">
    <div class="panel-heading">
        <ol class="breadcrumb">
            <li><a href="/">首页</a></li>
            <li class="active">资源管理</li>
        </ol>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="<#--table-responsive-->">
                            <div class="btn-group hidden-xs" id="toolbar">
                                <button id="btn_add" type="button" class="btn btn-default" title="新增资源">
                                    <i class="fa fa-plus"></i> 新增资源
                                </button>
                                <button id="btn_delete_ids" type="button" class="btn btn-default" title="删除选中">
                                    <i class="fa fa-trash-o"></i> 批量删除
                                </button>
                            </div>
                            <table id="mytable">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--添加or修改资源弹框-->
<div class="modal fade" id="addOrUpdateModal" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addroleLabel">添加资源</h4>
            </div>
            <div class="modal-body">
                <form id="addOrUpdateForm" class="form-horizontal form-label-left" novalidate>
                    <input type="hidden" name="id">
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >资源名: <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" name="name"  minlength="2" required placeholder="请输入资源名"/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >资源类型: <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control" name="type">
                                <option value="menu">菜单</option>
                                <option value="button">按钮</option>
                            </select>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >父级资源: <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control" name="parentId">
                                <option selected value="0">无</option>
                                <@bkTag method="parentResources">
                                    <#if parentResources?? && (parentResources?size > 0)>
                                        <#list parentResources as item>
                                            <option value="${item.id}">${item.name}</option>
                                        </#list>
                                    </#if>
                                </@bkTag>
                            </select>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >资源链接: <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" name="url"  minlength="2" required placeholder="请输入资源链接"/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >资源权限: <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" name="permission"  minlength="2" required placeholder="请输入资源权限"/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >资源排序: <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" name="sort" id="sort"  required placeholder="(是整数,越接近0级别越高)"/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >资源图标: <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" name="icon"  minlength="2" required placeholder="请输入资源图标"/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">是否可用 <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="available"  class="form-control">
                                <option value="1">可用</option>
                                <option value="0">禁用</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary addOrUpdateBtn">保存</button>
            </div>
        </div>
    </div>
</div>
<!--/添加资源弹框-->

<!-- 全局js -->
<script src="/static/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/js/bootstrap.min.js?v=3.3.6"></script>
<!-- bootstrap-table.min.js -->
<script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/bootstrap-table.min.js"></script>
<!-- 引入中文语言包 -->
<script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/locale/bootstrap-table-zh-CN.min.js"></script>
<!--myjs-->
<script src="/static/js/blog/table.js"></script>
<!--验证-->
<#--<script src="/static/js/validator.js"></script>-->
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
<!--提示框-->
<script src="/static/js/jquery-confirm.min.js"></script>
<#--树-->
<script src="https://cdn.bootcss.com/zTree.v3/3.5.33/js/jquery.ztree.all.js"></script>

</body>

<script>

    //自定义按钮
    function operateFormatter(code, row, index) {
        var userid = ${user.id}; //当前登录的资源
        var btns = [
            '<button class="btn btn-info btn-update"  data-id="'+row.id+'">编辑</button>&nbsp',
            '<button class="btn btn-danger btn-remove" data-id="'+row.id+'">删除</button>&nbsp'
        ]
        return btns.join('');
    };

    $(function(){
        $("#addOrUpdateForm").validate();

        //供table.js 使用
        var options = {
            url: "/resources/list",
            createUrl: "/resources/add", //添加资源
            getInfoUrl: "/resources/get/{id}", //获取资源信息
            updateUrl: "/resources/edit",
            removeUrl: "/resources/remove",
            columns: [
                {
                    checkbox: true
                }, {
                    field: 'name',
                    title: '资源名',
                    editable: false
                }, {
                    field: 'type',
                    title: '资源类型',
                    editable: false,
                    formatter: function(code, row, index){
                        return row.type == "menu" ? '菜单' : '按钮';
                    }
                },{
                    field: 'url',
                    title: '资源地址',
                    editable: false
                },{
                    field: 'permission',
                    title: '资源权限',
                    editable: false
                },{
                    field: 'parentId',
                    title: '父级资源',
                    editable: false,
                    formatter: function (code, row, index) {
                        if (row.parentId != 0){
                            return row.sysResourcesParent.name
                        }
                    }
                },{
                    field: 'icon',
                    title: '资源图标',
                    editable: false
                },{
                    field: 'sort',
                    title: '排序',
                    editable: false
                },{
                    field: 'external',
                    title: '外部资源',
                    editable: false
                },{
                    field: 'available',
                    title: '可用',
                    editable: false,
                    formatter: function (code, row, index) {
                        var status = row.available == 1 ? "可用" : "禁用";
                        return status;
                    }
                },
                {
                    field: 'operate',
                    title: '操作',
                    formatter: operateFormatter
                }
            ],
            modalName: "资源"
        };

        //初始化表格对象，按钮
        var table = new TableInit();
        var button = new ButtonInit();
        table.init(options);
        button.init(options);
    });


    /**
     * 分配权限
     */
//    $('#mytable').on('click', 'btn-allot', function(){
//        $.ajax({
//            type: 'post',
//            url: '/role/allot',
//            data: $("#Form").serialize(),
//            success: function (msg) {
//            }
//        });
//    });

</script>

</html>
