<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title> - 文章列表</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="/static/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/css/font-awesome.css?v=4.4.0" rel="stylesheet">

    <link href="/static/css/animate.css" rel="stylesheet">
    <link href="/static/css/style.css?v=4.1.0" rel="stylesheet">
    <!--提示框-->
    <link rel="stylesheet" href="/static/css/jquery-confirm.min.css">


    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
</head>

<body>


<div class="panel panel-default">
    <div class="panel-heading">
        <ol class="breadcrumb">
            <li><a href="/">首页</a></li>
            <li class="active">文章管理</li>
        </ol>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="<#--table-responsive-->">
                            <div class="btn-group hidden-xs" id="toolbar">
                                <button id="btn_delete_ids" article="button" class="btn btn-default" title="删除选中">
                                    <i class="fa fa-trash-o"></i> 批量删除
                                </button>
                            </div>
                            <table id="mytable">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--回复弹框-->
<div class="modal fade" id="replyModal" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="addroleLabel">回复评论</h4>
                <button article="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form id="replyForm" class="form-horizontal form-label-left" novalidate>
                    <input name="pid" value="" hidden />
                    <input name="sid" value="" hidden />
                    <textarea name="content" style="height: 100px;" cols="84"></textarea>
                </form>
            </div>
            <div class="modal-footer">
                <button article="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button article="button" class="btn btn-primary submitReply">回复</button>
            </div>
        </div>
    </div>
</div>

<!-- 全局js -->
<script src="/static/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/js/bootstrap.min.js?v=3.3.6"></script>
<!-- bootstrap-table.min.js -->
<script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/bootstrap-table.min.js"></script>
<!-- 引入中文语言包 -->
<script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/locale/bootstrap-table-zh-CN.min.js"></script>
<!--myjs-->
<script src="/static/js/blog/table.js"></script>
<!--验证-->
<#--<script src="/static/js/validator.js"></script>-->
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
<!--提示框-->
<script src="/static/js/jquery-confirm.min.js"></script>

<script src="/static/js/blog/mytools.js"></script>

</body>

<script>

    //自定义按钮
    function operateFormatter(code, row, index) {
        var btns = [];
        if (row.status == "APPROVED"){
            btns.push('<button s-id="'+row.sid+'" p-id="'+row.id+'" href="/article/update" class="btn btn-info btn-reply">回复</button>&nbsp');
        }else {
            btns.push('<button class="btn btn-danger btn-pass" data-id="'+row.id+'">审核</button>&nbsp');
        }
        btns.push('<button class="btn btn-danger btn-remove" data-id="'+row.id+'">删除</button>&nbsp');
        return btns.join('');
    };


    function init() {
        //回复
        $('#mytable').on('click','.btn-reply', function(){
            $('#replyForm input[name="sid"]').val($(this).attr('s-id'));
            $('#replyForm input[name="pid"]').val($(this).attr('p-id'));
            $('#replyForm textarea[name="content"]').val("");
            $('#replyModal').modal('show');
        });

        //通过审核
        $('#mytable').on('click','.btn-pass', function(){
            $.ajax({
                type: 'post',
                url: '/comment/pass',
                data: {'id': $(this).attr('data-id')},
                success: function (msg) {
                    $.alert(msg.message);
                    $("#mytable").bootstrapTable('refresh', {url: '/comment/list'});
                }
            });
        });

        //删除
//        $('#mytable').on('click','.btn-remove', function(){
//            $.ajax({
//                type: 'post',
//                url: '/comment/pass',
//                data: {'id': $(this).attr('data-id')},
//                success: function (msg) {
//                    $.alert(msg.message);
//                    $("#mytable").bootstrapTable('refresh', {url: '/comment/list'});
//                }
//            });
//        });

        //提交评论
        $('.submitReply').on('click',function () {
           alert($("#replyForm").serialize());
            $.ajax({
                type: 'post',
                url: '/comment/reply',
                data: $("#replyForm").serialize(),
                success: function (msg) {
                    $('#replyModal').modal('hide');
                    $.alert(msg.message);
                }
            });
        })

    }
    

    $(function(){
        $("#addOrUpdateForm").validate();

        //供table.js 使用
        var options = {
            url: "/comment/list",
            createUrl: "/comment/add",
            getInfoUrl: "/comment/get/{id}",
            removeUrl: "/comment/remove",
            columns: [
                {
                    checkbox: true
                },{
                    field: 'nickname',
                    title: '作者',
                    editable: true,
                    formatter: function (code, row, index) {
                        return '<ul>' +
                                '<li><img style="width: 30px;height: 30px;" src="'+row.avatar+'"></img>&nbsp;'+row.nickname+'</li>' +
                                '<li>qq号:'+row.qq+'</li>' +
                                '<li>浏览器类型:'+row.browser+'</li>' +
                                '<li>浏览器名称'+row.browser_short_name+'</li>' +
                                '<li>地址'+row.address+'</li>' +
                                '<ul>'
                    }
                }, {
                    field: 'content',
                    title: '内容',
                    editable: true
                }, {
                    field: 'sid',
                    title: '回复至',
                    editable: true,
                    formatter: function (code, row, index) {
                        console.log('----))__');
                        console.log(row);
                        if (row.sid == -1 || row.sid == null){
                            return '<a href="#">留言板</a>';
                        }else{
                            return '<a href="/article/'+row.sid+'">'+row.bizArticle.title+'</a>';
                        }
                    }
                }, {
                    field: 'support',
                    title: '赞/踩',
                    editable: true,
                    formatter: function (code, row, index) {
                        return row.support+' / '+row.oppose;
                    }
                },{
                    field: 'status',
                    title: '状态',
                    editable: true,
                    formatter: function (code, row, index) {
                        if (row.status == "APPROVED"){
                            return "已通过";
                        }else{
                            return "未通过";
                        }
                    }
                }, {
                    field: 'operate',
                    title: '操作',
                    formatter: operateFormatter
                }
            ],
            modalName: "文章"
        };

        //初始化表格对象，按钮
        var table = new TableInit();
        var button = new ButtonInit();
        table.init(options);
        button.init(options);

        init();
    });

    

</script>

</html>
