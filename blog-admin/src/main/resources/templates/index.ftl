<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">

    <title> 啊程博客- 主页</title>

    <meta name="keywords" content="">
    <meta name="description" content="">

    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->

    <link rel="shortcut icon" href="favicon.ico">
    <link href="/static/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/css/animate.css" rel="stylesheet">
    <link href="/static/css/style.css?v=4.1.0" rel="stylesheet">
    <link rel="stylesheet" href="/static/css/jquery-confirm.min.css">
</head>

<body class="fixed-sidebar full-height-layout gray-bg" style="overflow:hidden">
<div id="wrapper">
    <!--左侧导航开始-->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="nav-close"><i class="fa fa-times-circle"></i>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear">
                                    <span class="block m-t-xs" style="font-size:20px;">
                                        <i class="fa fa-area-chart"></i>
                                        <strong class="font-bold">啊程博客后台</strong>
                                    </span>
                                </span>
                        </a>
                    </div>
                    <div class="logo-element">啊程博客后台
                    </div>
                </li>


                <li>
                    <a class="J_menuItem" href="/home">
                    <#--获取菜单-->
                    <@bkTag method="menus" userId="1">
                        <#if menus?? && menus?size gt 0>
                        <#list menus as item>
                            <#if !item.sysResourcesList?? || item.sysResourcesList?size == 0>
                                <li>
                                    <a class="J_menuItem" href="${item.url}">
                                        <i class="${item.icon}"></i>
                                        <span class="nav-label">${item.name}</span>
                                    </a>
                                </li>
                            <#else>
                                <li>
                                    <a href="#">
                                        <i class="${item.icon}"></i>
                                        <span class="nav-label">${item.name}</span>
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul class="nav nav-second-level">
                                       <#list item.sysResourcesList as node>
                                           <li>
                                               <a class="J_menuItem" href="${node.url}">${node.name}</a>
                                           </li>
                                       </#list>
                                    </ul>
                                </li>
                            </#if>
                        </#list>
                        </#if>
                    </@bkTag>
                    </a>
                </li>

                <li>
                    <a href="#" class="exit">
                        <i class="fa fa-arrow-circle-left"></i>
                        <span class="nav-label">退出系统</span>
                    </a>
                </li>

                <li class="line dk"></li>
            </ul>
        </div>
    </nav>
    <!--左侧导航结束-->
    <!--右侧部分开始-->
    <@bkTag method="comments">
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header"><a class="navbar-minimalize minimalize-styl-2 btn btn-info " href="#"><i class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" method="post" action="search_results.html">
                        <div class="form-group">
                            <input type="text" placeholder="请输入您需要查找的内容 …" class="form-control" name="top-search" id="top-search">
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-envelope"></i> <span class="label label-warning">${comments?size}</span>
                        </a>
                        <ul class="dropdown-menu dropdown-messages">
                                <#if comments?? && (comments?size > 0)>
                                    <#list comments as item>
                                        <li class="m-t-xs">
                                        <div class="dropdown-messages-box">
                                            <a href="#" class="pull-left">
                                            <img alt="image" class="img-circle" src="${itme.avatar}">
                                            </a>
                                            <div class="media-body">
                                            <small class="pull-right">待审核</small>
                                            <strong>${item.nickname}：</strong>${item.content}
                                            <br>
                                            <small class="text-muted">${item.createTime?string('yyyy-MM-dd hh:mm:ss')}</small>
                                        </div>
                                        </div>
                                        </li>
                                        <li class="divider"></li>
                                    </#list>
                                </#if>
                            <li>
                                <div class="text-center link-block">
                                    <a class="J_menuItem" href="/comments">
                                        <i class="fa fa-envelope"></i>
                                        <strong> 查看所有评论</strong>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row J_mainContent" id="content-main">
            <iframe id="J_iframe" width="   100%" height="100%" src="/home" frameborder="0"  seamless></iframe>
        </div>
    </div>
    </@bkTag>
    <!--右侧部分结束-->
</div>

<!-- 全局js -->
<script src="/static/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/static/js/plugins/layer/layer.min.js"></script>

<!-- 自定义js -->
<script src="/static/js/hAdmin.js?v=4.1.0"></script>
<script type="text/javascript" src="/static/js/index.js"></script>

<!-- 第三方插件 -->
<script src="/static/js/plugins/pace/pace.min.js"></script>
<#--驗證-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

<script>
    //退出
    $('.exit').on('click',function(){
        $.confirm({
            title:'提示',
            content:'确定要退出吗？',
            buttons: {
                info: {
                    text: '确定',
                    btnClass: 'btn-blue',
                    action: function(){
                        window.location.href = "/passport/logout";
                    }
                },
                heyThere: {
                    text: '取消',
                    action: function () {
                    }
                }
            }
        });
    })
</script>

</body>

</html>
