<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title> - 用户列表</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="/static/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/css/font-awesome.css?v=4.4.0" rel="stylesheet">

    <link href="/static/css/animate.css" rel="stylesheet">
    <link href="/static/css/style.css?v=4.1.0" rel="stylesheet">
    <!--提示框-->
    <link rel="stylesheet" href="/static/css/jquery-confirm.min.css">

    <link rel="stylesheet" href="/static/css/zTree/demo.css" type="text/css">
    <link rel="stylesheet" href="/static/css/zTree/zTreeStyle/zTreeStyle.css" type="text/css">



    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
</head>

<body>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="/">首页</a></li>
            <li class="active">用户管理</li>
        </ol>
        <div class="x_panel">
            <div class="x_content">
                <div class="<#--table-responsive-->">
                    <div class="btn-group hidden-xs" id="toolbar">
                        <button id="btn_add" type="button" class="btn btn-default" title="新增用户">
                            <i class="fa fa-plus"></i> 新增用户
                        </button>
                        <button id="btn_delete_ids" type="button" class="btn btn-default" title="删除选中">
                            <i class="fa fa-trash-o"></i> 批量删除
                        </button>
                    </div>
                    <table id="mytable">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>




<#--给用户分配角色模态-->
<div class="modal fade" id="allotModal" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addroleLabel">分配角色</h4>
            </div>
            <div class="modal-body">
                <form id="boxRoleForm">
                    <div class="treeClass">
                        <ul id="treeDemo" class="ztree"></ul>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<!--添加or修改用户弹框-->
<div class="modal fade" id="addOrUpdateModal" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addroleLabel">添加用户</h4>
            </div>
            <div class="modal-body">
                <form id="addOrUpdateForm" class="form-horizontal form-label-left" novalidate>
                    <input type="hidden" name="id">
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">用户名: <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input flag="readonly" type="text" class="form-control col-md-7 col-xs-12" name="username" id="username" minlength="2" required placeholder="请输入用户名"/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" minlength="2" for="password">密码: <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="password" class="form-control col-md-7 col-xs-12" id="password" name="password" required="required" placeholder="请输入密码 6位以上"/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nickname">昵称:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" name="nickname" id="nickname" required placeholder="请输入昵称"/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile">手机:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="tel" class="form-control col-md-7 col-xs-12" name="mobile" id="mobile" data-validate-length-range="8,20" placeholder="请输入手机号"/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">邮箱:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="email" class="form-control col-md-7 col-xs-12" name="email" id="email" placeholder="请输入邮箱"/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="qq">QQ:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" class="form-control col-md-7 col-xs-12" name="qq" id="qq" placeholder="请输入QQ"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary addOrUpdateBtn">保存</button>
            </div>
        </div>
    </div>
</div>
<!--/添加用户弹框-->

<!-- 全局js -->
<script src="/static/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/js/bootstrap.min.js?v=3.3.6"></script>
<!-- bootstrap-table.min.js -->
<script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/bootstrap-table.min.js"></script>
<!-- 引入中文语言包 -->
<script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/locale/bootstrap-table-zh-CN.min.js"></script>
<!--myjs-->
<script src="/static/js/blog/table.js"></script>
<!--验证-->
<#--<script src="/static/js/validator.js"></script>-->
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
<!--提示框-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

<script type="text/javascript" src="/static/js/zTree/jquery.ztree.core.js"></script>
<script type="text/javascript" src="/static/js/zTree/jquery.ztree.excheck.js"></script>

</body>

<script>

    //自定义按钮
    function operateFormatter(code, row, index) {
        console.log(row);
        var userid = ${user.id}; //当前登录的用户
        var btns = [
            '<button class="btn btn-info btn-update"  data-id="'+row.id+'">编辑</button>&nbsp'
        ]
        if (row.id != userid){
            btns.push('<button class="btn btn-danger btn-remove" data-id="'+row.id+'">删除</button>&nbsp');
            btns.push('<button class="btn btn-success btn-allot" data-id="'+row.id+'">分配角色</button>');
        }
        return btns.join('');
    };

    $(function(){
        $("#addOrUpdateForm").validate();

        //供table.js 使用
        var options = {
            url: "/user/list",
            createUrl: "/user/add", //添加用户
            getInfoUrl: "/user/get/{id}", //获取用户信息
            updateUrl: "/user/edit",
            removeUrl: "/user/remove",
            columns: [
                {
                    checkbox: true
                }, {
                    field: 'username',
                    title: '用户名',
                    editable: false,
                }, {
                    field: 'nickname',
                    title: '昵称',
                    editable: true
                }, {
                    field: 'email',
                    title: '邮箱',
                    editable: true
                }, {
                    field: 'qq',
                    title: 'qq',
                    editable: true
                }, {
                    field: 'operate',
                    title: '操作',
                    formatter: operateFormatter
                }
            ],
            modalName: "用户"
        };

        //初始化表格对象，按钮
        var table = new TableInit();
        var button = new ButtonInit();
        table.init(options);
        button.init(options);

        $('#mytable').on('click','.btn-allot', function(){
            var userId = $(this).attr('data-id');
            $.ajax({
                type: 'post',
                url: '/role/selectRoleSelected',
                data: {'userId':userId},
                success: function (msg) {
                    var result = msg.data.data;
                    console.log(result);
                    var setting = {
                        view: {
                            selectedMulti: false
                        },
                        check: {
                            enable: true,
                            chkStyle: "radio"
                        },
                        data: {
                            simpleData: {
                                enable: true
                            }
                        },
                        callback: {
                            onCheck: function (event, treeId, treeNode) { //结点点击事件
                                //得到操作树对象
                                var obj = $.fn.zTree.getZTreeObj(treeId);
                                var nodes = obj.getCheckedNodes(true); //true全部选中， false非选中
                                if (nodes.length != 1){
                                    return '';
                                }
                                var item = nodes[0];
                                console.log(userId);
                                console.log(item.id);
                                $.ajax({
                                    type: 'post',
                                    url: '/role/saveUserRole', //保存用户角色
                                    data: {"userId":userId, 'roleId':item.id},
                                    success: function (data) {
                                        console.log(data);
                                    }
                                });
                            }
                        }
                    };
                    //初始化
                    var tree = $.fn.zTree.init($("#treeDemo"), setting, result);
                    $('#allotModal').modal('show');
                    tree.expandAll(true);//全部展开
                }
            });
        })
    });

</script>

</html>
