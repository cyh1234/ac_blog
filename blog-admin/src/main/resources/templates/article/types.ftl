<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title> - 分类列表</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="/static/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/css/font-awesome.css?v=4.4.0" rel="stylesheet">

    <link href="/static/css/animate.css" rel="stylesheet">
    <link href="/static/css/style.css?v=4.1.0" rel="stylesheet">
    <!--提示框-->
    <link rel="stylesheet" href="/static/css/jquery-confirm.min.css">


    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
</head>

<body>

<div class="panel panel-default">
    <div class="panel-heading">
        <ol class="breadcrumb">
            <li><a href="/">首页</a></li>
            <li class="active">分类管理</li>
        </ol>
    </div>
    <div class="panel-body">

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="/">首页</a></li>
                    <li class="active">分类管理</li>
                </ol>
                <div class="x_panel">
                    <div class="x_content">
                        <div class="<#--table-responsive-->">
                            <div class="btn-group hidden-xs" id="toolbar">
                                <button id="btn_add" type="button" class="btn btn-default" title="新增用户">
                                    <i class="fa fa-plus"></i> 新增分类
                                </button>
                                <button id="btn_delete_ids" type="button" class="btn btn-default" title="删除选中">
                                    <i class="fa fa-trash-o"></i> 批量删除
                                </button>
                            </div>
                            <table id="mytable">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--添加or修改弹框-->
<div class="modal fade" id="addOrUpdateModal" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addroleLabel">添加分类</h4>
            </div>
            <div class="modal-body">
                <form id="addOrUpdateForm" class="form-horizontal form-label-left" novalidate>
                    <input type="hidden" name="id">
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">名称: <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" name="name" id="name" minlength="2" required placeholder="请输入分类名称"/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="description">描述: <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea type="text" class="form-control col-md-7"  rows="3" id="description" name="description" required="required" placeholder="请输入描述"></textarea>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sort">排序</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" class="form-control col-md-7 col-xs-12" name="sort" id="sort" required placeholder="请输入排序"/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="available">是否可用</label>
                        <label>
                            <input type="radio" class="flat" checked name="available" value="1"> 可用
                        </label>
                        <label>
                            <input type="radio" class="flat" name="available" value="0"> 禁用
                        </label>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="icon">图标:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" name="icon" id="email" placeholder="bootstrap自带图标库"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary addOrUpdateBtn">保存</button>
            </div>
        </div>
    </div>
</div>
<!--/添加用户弹框-->

<!-- 全局js -->
<script src="/static/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/js/bootstrap.min.js?v=3.3.6"></script>
<!-- bootstrap-table.min.js -->
<script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/bootstrap-table.min.js"></script>
<!-- 引入中文语言包 -->
<script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/locale/bootstrap-table-zh-CN.min.js"></script>
<!--myjs-->
<script src="/static/js/blog/table.js"></script>
<!--验证-->
<#--<script src="/static/js/validator.js"></script>-->
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
<!--提示框-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

</body>

<script>

    //自定义按钮
    function operateFormatter(code, row, index) {
        var userid = ${user.id};
        var btns = [
            '<button class="btn btn-info btn-update"  data-id="'+row.id+'">编辑</button>&nbsp',
            '<button class="btn btn-danger btn-remove" data-id="'+row.id+'">删除</button>&nbsp'
        ]
        return btns.join('');
    };

    $(function(){
        $("#addOrUpdateForm").validate();

        //供table.js 使用
        var options = {
            url: "/type/list",
            createUrl: "/type/add",
            getInfoUrl: "/type/get/{id}",
            updateUrl: "/type/edit",
            removeUrl: "/type/remove",
            columns: [
                {
                    checkbox: true
                }, {
                    field: 'id',
                    title: 'ID',
                    editable: false,
                }, {
                    field: 'name',
                    title: '名称',
                    editable: true,
                    formatter: function (code, row, index) {
                        var id = row.id;
                        return '<a href="#">'+row.name+'</a>';
                    }
                }, {
                    field: 'pid',
                    title: '父级分类',
                    editable: true
                }, {
                    field: 'description',
                    title: '描述',
                    editable: true
                }, {
                    field: 'sort',
                    title: '排序',
                    editable: true
                }, {
                    field: 'available',
                    title: '可用',
                    editable: true
                }, {
                    field: 'icon',
                    title: '图标',
                    editable: true,
                    formatter: function (code, row, index) {
                        console.log('....');

                        return '<i class="' + row.icon + '"></i>';
                    }
                },{
                    field: 'operate',
                    title: '操作',
                    formatter: operateFormatter
                }
            ],
            modalName: "分类"
        };

        //初始化表格对象，按钮
        var table = new TableInit();
        var button = new ButtonInit();
        table.init(options);
        button.init(options);
    });

</script>

</html>
