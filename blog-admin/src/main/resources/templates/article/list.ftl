<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title> - 文章列表</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="/static/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/css/font-awesome.css?v=4.4.0" rel="stylesheet">

    <link href="/static/css/animate.css" rel="stylesheet">
    <link href="/static/css/style.css?v=4.1.0" rel="stylesheet">
    <!--提示框-->
    <link rel="stylesheet" href="/static/css/jquery-confirm.min.css">


    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
</head>

<body>

<div class="panel panel-default">
    <div class="panel-heading">
        <ol class="breadcrumb">
            <li><a href="/">首页</a></li>
            <li class="active">文章管理</li>
        </ol>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="<#--table-responsive-->">
                            <div class="btn-group hidden-xs" id="toolbar">
                                <a href="/article/publish" class="btn btn-default" title="新增用户">
                                    <i class="fa fa-plus"></i> 发布文章
                                </a>
                                <button id="btn_delete_ids" article="button" class="btn btn-default" title="删除选中">
                                    <i class="fa fa-trash-o"></i> 批量删除
                                </button>
                            </div>
                            <table id="mytable">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--添加or修改弹框-->
<div class="modal fade" id="addOrUpdateModal" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button article="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addroleLabel">添加文章</h4>
            </div>
            <div class="modal-body">
                <form id="addOrUpdateForm" class="form-horizontal form-label-left" novalidate>
                    <input article="hidden" name="id">
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">名称: <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input article="text" class="form-control col-md-7 col-xs-12" name="name" id="name" minlength="2" required placeholder="请输入文章名称"/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="description">描述: <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea article="text" class="form-control col-md-7"  rows="3" id="description" name="description" required="required" placeholder="请输入描述"></textarea>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sort">排序</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input article="number" class="form-control col-md-7 col-xs-12" name="sort" id="sort" required placeholder="请输入排序"/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="available">是否可用</label>
                        <label>
                            <input article="radio" class="flat" checked name="available" value="1"> 可用
                        </label>
                        <label>
                            <input article="radio" class="flat" name="available" value="0"> 禁用
                        </label>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="icon">图标:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input article="text" class="form-control col-md-7 col-xs-12" name="icon" id="email" placeholder="bootstrap自带图标库"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button article="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button article="button" class="btn btn-primary addOrUpdateBtn">保存</button>
            </div>
        </div>
    </div>
</div>
<!--/添加用户弹框-->

<!-- 全局js -->
<script src="/static/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/js/bootstrap.min.js?v=3.3.6"></script>
<!-- bootstrap-table.min.js -->
<script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/bootstrap-table.min.js"></script>
<!-- 引入中文语言包 -->
<script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/locale/bootstrap-table-zh-CN.min.js"></script>
<!--myjs-->
<script src="/static/js/blog/table.js"></script>
<!--验证-->
<#--<script src="/static/js/validator.js"></script>-->
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
<!--提示框-->
<script src="/static/js/jquery-confirm.min.js"></script>

<script src="/static/js/blog/mytools.js"></script>

</body>

<script>

    //自定义按钮
    function operateFormatter(code, row, index) {
        var userid = ${user.id};
        var btns = [
            '<a href="/article/update/'+row.id+'" class="btn btn-info btn-update">编辑</a>&nbsp',
            '<button class="btn btn-danger btn-remove" data-id="'+row.id+'">删除</button>&nbsp'
        ]
        return btns.join('');
    };

    $(function(){
        $("#addOrUpdateForm").validate();

        //供table.js 使用
        var options = {
            url: "/article/list",
            createUrl: "/article/add",
            getInfoUrl: "/article/get/{id}",
            removeUrl: "/article/remove",
            columns: [
                {
                    checkbox: true
                }, {
                    field: 'id',
                    title: 'ID',
                    editable: false,
                }, {
                    field: 'title',
                    title: '标题',
                    editable: true,
                    formatter: function (code, row, index) {
                        console.log('-----');
                        console.log(code);
                        console.log(row);
                        var title = row.original == 1 ? "[ 原创 ] " : "[ 转载 ]";
                        title += row.title;
                        return title;
                    }
                }, {
                    field: 'status',
                    title: '状态',
                    editable: true,
                    formatter: function (code, row, index) {
                        var status = code == 1 ? "已发布" : "草稿";
                        return status;
                    }
                }, {
                    field: 'recommended',
                    title: '推荐',
                    editable: true,
                    formatter: function (code, row, index) {
                        return code == 1 ? "是" : "否";
                    }
                }, {
                    field: 'top',
                    title: '置顶',
                    editable: true,
                    formatter: function (code, row, index) {
                        return code == 1 ? "是" : "否";
                    }
                }, {
                    field: 'type_id',
                    title: '分类',
                    editable: true,
                    formatter: function (code, row, index) {
                        return row.bizType.name;
                    }
                }, {
                    field: 'tags',
                    title: '标签',
                    editable: true,
                    formatter: function (code, row, index) {
                        var html = ""
                        for (var i=0; i<row.tags.length; i++){
                            html+= '[ '+row.tags[i].name+' ] ';
                        }
                        return html;
                    }
                }, {
                    field: 'create_time',
                    title: '发布时间',
                    editable: true,
                    formatter: function (code, row, index) {
                        var time = row.createTime;
                        return new Date(time).format("yyyy-MM-dd hh:mm:ss");
                    }
                },{
                    field: 'operate',
                    title: '操作',
                    formatter: operateFormatter
                }
            ],
            modalName: "文章"
        };

        //初始化表格对象，按钮
        var table = new TableInit();
        var button = new ButtonInit();
        table.init(options);
        button.init(options);
    });

</script>

</html>
