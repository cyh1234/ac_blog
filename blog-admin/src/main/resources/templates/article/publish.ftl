<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title> - 分类列表</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="/static/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/css/font-awesome.css?v=4.4.0" rel="stylesheet">

    <link href="/static/css/animate.css" rel="stylesheet">
    <link href="/static/css/style.css?v=4.1.0" rel="stylesheet">
    <!--提示框-->
    <link rel="stylesheet" href="/static/css/jquery-confirm.min.css">


    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
</head>

<body>


<div class="panel panel-default">
    <div class="panel-heading">
        <ol class="breadcrumb">
            <li><a href="/">首页</a></li>
            <li class="active">发布文章</li>
        </ol>
    </div>
    <div class="panel-body">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>发布文章 <small></small></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <form id="publishForm" class="form-horizontal form-label-left"  enctype="multipart/form-data">
                                <input type="hidden" name="userId" value="${user.id}">
                                <input type="hidden" name="id" value="${aid}">
                                <div class="item form-group">
                                    <label class="control-label col-md-1 col-sm-1 col-xs-12" for="title">标题 <span class="required">*</span></label>
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <input type="text" class="form-control col-md-7 col-xs-12" name="title" id="title" required="required" placeholder="请输入标题"/>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-xs-12">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="square" name="original"> 原创
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-1 col-sm-1 col-xs-12" for="title">封面 <span class="required">*</span></label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <div class="choose-local-img">
                                            <button type="button" class="btn btn-success" id="file-upload-btn">上传图片</button>
                                            <input id="cover-img-file" type="file" onchange="uploadCover()" name="cover-img" style="display: none">
                                            <input id="cover-img-input" type="hidden" name="coverImage">
                                            <div class="preview" class="fa-2x" style="padding: 10px;">
                                                <img name="coverImg" src="" style="display: none; width: 50%; height: 50%">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-1 col-sm-1 col-xs-12" for="password">内容 <span class="required">*</span></label>
                                    <div class="col-md-11 col-sm-11 col-xs-12" style="height: 380px;">
                                        <div id="editor"  style="height: 480px; max-height:480px;"></div>
                                        <textarea id="content" name="content" style="display: none"></textarea>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-1 col-sm-1 col-xs-12" for="nickname"><i class="fa fa-refresh fa-fw fa-1x pointer" id="refressType"></i>分类 <span class="required">*</span></label>
                                    <div class="col-md-11 col-sm-11 col-xs-12">
                                        <div class="input-group">
                                            <select class="form-control" name="typeId" id="typeId">

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-1 col-sm-1 col-xs-12" for="mobile"><i class="fa fa-refresh fa-fw fa-1x pointer" id="refressTag"></i>标签 <span class="required">*</span></label>
                                    <div class="col-md-11 col-sm-11 col-xs-12">
                                        <ul class="list-unstyled list-inline" id="tag-list">
                                        </ul>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-1 col-sm-1 col-xs-12" for="mobile">状态 <span class="required">*</span></label>
                                    <div class="col-md-11 col-sm-11 col-xs-12">
                                        <ul class="list-unstyled list-inline">
                                            <li>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" class="square" checked name="status" value="1"> 发布
                                                    </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" class="square" name="status" value="0"> 草稿
                                                    </label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="custom-panel">
                                    <h2 class="x_title custom-dropdown">其他录入项 <small> - 方便SEO收录</small> <i class="pull-right fa fa-angle-double-up"></i></h2>
                                    <div class="custom-container">
                                        <div class="item form-group">
                                            <label class="control-label col-md-1 col-sm-1 col-xs-12" for="password">摘要 <span class="required">*</span></label>
                                            <div class="col-md-10 col-sm-10 col-xs-12">
                                                <textarea class="form-control col-md-7 col-xs-12" id="description" name="description" required="required"></textarea>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-1 col-sm-1 col-xs-12" for="password">关键词 <span class="required">*</span></label>
                                            <div class="col-md-10 col-sm-10 col-xs-12">
                                                <textarea class="form-control col-md-7 col-xs-12" id="keywords" name="keywords" required="required"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-10 col-sm-10 col-xs-12">
                                        <button type="button" class="btn btn-success publishBtn">保存</button>
                                        <button type="reset" class="btn btn-primary">重置</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--上传图片-->
<div class="modal fade" id="chooseImg" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addroleLabel">选择图片</h4>
            </div>
            <div class="modal-body">
                <!--面板 s-->

                <ul id="myTab" class="nav nav-tabs">
                    <li class="active">
                        <a href="#system" data-toggle="tab">系统图片</a>
                    </li>
                    <li>
                        <a href="#local" data-toggle="tab">本地上传</a>
                    </li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade in active" id="system">

                    </div>
                    <div class="tab-pane fade" id="local">
                        <button id="tab-btn" class="btn btn-info">上传图片</button>
                        <div class="row">
                            <div class="col-md-12">
                                <img name="coverImg" src="" style="display: none; width: 50%; height: 50%">
                                <img id="loadingImg" src="/static/img/loading.gif" style="display: none;">
                            </div>
                        </div>
                    </div>
                </div>

                <!--面板 e-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-success" data-dismiss="modal">确定</button>
            </div>
        </div>
    </div>
</div>


<!-- 全局js -->
<script src="/static/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/js/bootstrap.min.js?v=3.3.6"></script>
<!-- bootstrap-table.min.js -->
<script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/bootstrap-table.min.js"></script>
<!-- 引入中文语言包 -->
<script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/locale/bootstrap-table-zh-CN.min.js"></script>
<!--myjs-->
<script src="/static/js/blog/table.js"></script>
<!--验证-->
<#--<script src="/static/js/validator.js"></script>-->
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
<!--提示框-->
<#--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>-->
<script src="/static/js/jquery-confirm.min.js"></script>
<script src="/static/js/wangEditor.min.js"></script>
<script src="/static/js/blog/publish.js"></script>

</body>

<script>
    $(function () {
        initEdit(); //初始化编辑器
        initData(); //初始化数据
        
        //若id不是空则是编辑一篇文章，去获取此id内容
        var aid = '${aid}';
        if (aid != null){
            editInit(aid);
        }
    })

</script>

</html>
