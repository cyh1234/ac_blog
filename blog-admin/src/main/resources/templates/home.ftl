<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title> - 个人信息</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="/static/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/static/css/font-awesome.css?v=4.4.0" rel="stylesheet">

    <link href="/static/css/animate.css" rel="stylesheet">
    <link href="/static/css/style.css?v=4.1.0" rel="stylesheet">
    <!--提示框-->
    <link rel="stylesheet" href="/static/css/jquery-confirm.min.css">


    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
</head>

<body>

<div class="panel panel-default">
    <div class="panel-heading">
        <ol class="breadcrumb">
            <li><a href="/">首页</a></li>
        </ol>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">
                <div class="col-md-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>头像</h5>
                        </div>
                        <div class="ibox-content">
                            <img src="${config.headImg}" style="width: 100%; height: 200px;"/>
                            <button class="btn btn-info btn-block">更换头像</button>
                        </div>
                    </div>
                </div>

                <#--<div class="col-md-12">-->
                    <#--<div class="ibox float-e-margins">-->
                        <#--<div class="ibox-title">-->
                            <#--<h5>签名</h5>-->
                        <#--</div>-->
                        <#--<div class="ibox-content">-->
                            <#--签名：<input class="form-control site-desc" value="${config.siteDesc}"/>-->
                        <#--</div>-->
                    <#--</div>-->
                <#--</div>-->

            </div>
            <div class="col-md-9">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>个人信息</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="<#--table-responsive-->">
                                    <div class="btn-group hidden-xs" id="toolbar">
                                        <button id="btn_add" class="btn btn-default" title="新增用户">
                                            <i class="fa fa-plus"></i> 新增信息
                                        </button>
                                        <button id="btn_delete_ids" article="button" class="btn btn-default" title="删除选中">
                                            <i class="fa fa-trash-o"></i> 批量删除
                                        </button>
                                    </div>
                                    <table id="mytable">
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="panel padder-v item bg-info">
                                <div class="h1 text-fff font-thin h1">521</div>
                                <span class="text-muted text-xs">今日访问</span>
                                <div class="top text-right w-full">
                                    <i class="fa fa-caret-down text-warning m-r-sm"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!--添加or修改弹框-->
<div class="modal fade" id="addOrUpdateModal" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button tag="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addroleLabel">添加信息</h4>
            </div>
            <div class="modal-body">
                <form id="addOrUpdateForm" class="form-horizontal form-label-left" novalidate>
                    <input hidden name="id">
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">信息key：<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input tag="text" class="form-control col-md-7 col-xs-12" name="infoKey" minlength="2" required/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">信息值：<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input tag="text" class="form-control col-md-7 col-xs-12" name="infoValue" minlength="2" required/>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">图标：<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input tag="text" class="form-control col-md-7 col-xs-12" name="infoIcon" minlength="2" required/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button tag="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button tag="button" class="btn btn-primary addOrUpdateBtn">保存</button>
            </div>
        </div>
    </div>
</div>
<!--/添加用户弹框-->



<!-- 全局js -->
<script src="/static/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/js/bootstrap.min.js?v=3.3.6"></script>
<!-- bootstrap-table.min.js -->
<script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/bootstrap-table.min.js"></script>
<!-- 引入中文语言包 -->
<script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/locale/bootstrap-table-zh-CN.min.js"></script>
<!--myjs-->
<script src="/static/js/blog/table.js"></script>
<!--验证-->
<#--<script src="/static/js/validator.js"></script>-->
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
<!--提示框-->
<script src="/static/js/jquery-confirm.min.js"></script>

<script src="/static/js/blog/mytools.js"></script>

</body>

<script>

    //自定义按钮
    function operateFormatter(code, row, index) {
        console.log(row);
        var btns = [
            '<button class="btn btn-info btn-update" data-id="'+row.id+'">编辑</button>&nbsp',
            '<button class="btn btn-danger btn-remove" data-id="'+row.id+'">删除</button>&nbsp'
        ]
        return btns.join('');
    };

    $(function(){
        $("#addOrUpdateForm").validate();

        //供table.js 使用
        var options = {
            url: "/info/list",
            createUrl: "/info/add",
            getInfoUrl: "/info/get/{id}",
            updateUrl: "/info/edit",
            removeUrl: "/info/remove",
            columns: [
                {
                    checkbox: true
                }, {
                    field: 'id',
                    title: 'ID',
                    editable: false,
                }, {
                    field: 'infoIcon',
                    title: '图标',
                    editable: true,
                    formatter:function (code, row, index) {
                        return '<i class="'+row.infoIcon+'"></i>'
                    }
                }, {
                    field: 'infoKey',
                    title: '信息key',
                    editable: true
                }, {
                    field: 'infoValue',
                    title: '信息值',
                    editable: true
                },{
                    field: 'operate',
                    title: '操作',
                    formatter: operateFormatter
                }
            ],
            modalName: "信息"
        };

        //初始化表格对象，按钮
        var table = new TableInit();
        var button = new ButtonInit();
        table.init(options);
        button.init(options);

//
//        $('.site-desc').change(function () {
//            var  siteDesc = $(this).val();
//            $.ajax({
//                type: 'post',
//                url: '/home/editSiteDesc',
//                data: {'siteDesc',siteDesc}
//                success: function (msg) {
//
//                }
//            });
//        })
    });

</script>

</html>
