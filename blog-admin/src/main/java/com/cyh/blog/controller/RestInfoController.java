package com.cyh.blog.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.cyh.blog.business.service.SysInfoService;
import com.cyh.blog.business.vo.InfoVo;
import com.cyh.blog.framework.object.PageResult;
import com.cyh.blog.persistence.beans.SysInfo;
import com.cyh.blog.util.Result;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/info")
public class RestInfoController {

    @Autowired
    private SysInfoService sysInfoService;

    @PostMapping("/list")
    public PageResult list(InfoVo infoVo) {
        PageInfo<SysInfo> pageInfo = sysInfoService.selectByCondition(infoVo);
        return Result.tablePage(pageInfo);
    }


    @PostMapping("/add")
    public Result add(SysInfo sysInfo) {
        try{
            sysInfoService.insert(sysInfo);
        }catch (Exception e){
            e.printStackTrace();
            return Result.success("添加失败");
        }
        return Result.success("添加成功");
    }

    @PostMapping("/get/{id}")
    public Result get(@PathVariable("id") Integer id) {
        SysInfo sysInfo = sysInfoService.selectOne(new EntityWrapper<SysInfo>().eq("id", id));
        return Result.success("获取成功").add("data", sysInfo);
    }

    @PostMapping("/edit")
    public Result edit(SysInfo sysInfo) {
        try {
            sysInfoService.updateById(sysInfo);
        }catch (Exception e){
            return  Result.error("更新失败");
        }
        return Result.success("更新成功");
    }

    @PostMapping("/remove")
    public Result remove(Integer[] ids) {
        try {
            if(ids != null){
                for (int i=0; i<ids.length; i++){
                    sysInfoService.deleteById(ids[i]);
                }
            }
        }catch (Exception e){
            return Result.error("删除失败");
        }
        return Result.success("成功删除"+ids.length+"条记录");
    }
}
