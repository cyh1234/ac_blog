package com.cyh.blog.controller;


import com.cyh.blog.business.constant.SessionConstant;
import com.cyh.blog.business.service.BizCommentService;
import com.cyh.blog.business.service.BizTagsService;
import com.cyh.blog.business.vo.CommentVo;
import com.cyh.blog.framework.object.PageResult;
import com.cyh.blog.persistence.beans.BizComment;
import com.cyh.blog.persistence.beans.SysUser;
import com.cyh.blog.util.Result;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Date;

@RestController
@RequestMapping("/comment")
public class RestCommentController {
    @Autowired
    private BizTagsService bizTagsService;

    @Autowired
    private BizCommentService bizCommentService;

    @PostMapping("/list")
    public PageResult list(CommentVo commentVo) {
        PageInfo<BizComment> pageInfo = bizCommentService.selectByCondition(commentVo);
        return Result.tablePage(pageInfo);
    }
//
//    @PostMapping("/getAll")
//    public Result getAll() {
//        List<BizTags> bizTypes = bizTagsService.selectList(null);
//        return Result.success("加载成功").add("data",bizTypes);
//    }
//
//
    @PostMapping("/reply")
    public Result reply(BizComment bizComment, HttpSession session) {
        //评论人
        SysUser sysUser = (SysUser) session.getAttribute(SessionConstant.USER_SESSION_KEY);

        bizComment.setUserId(sysUser.getId());
        bizComment.setEmail(sysUser.getEmail());
        bizComment.setAvatar(sysUser.getAvatar());
        bizComment.setCreateTime(new Date());
        bizComment.setStatus("APPROVED");
        bizComment.setNickname(sysUser.getNickname());
        bizComment.setQq(sysUser.getQq());

        try{
            bizCommentService.insert(bizComment);
        }catch (Exception e){
            e.printStackTrace();
            return Result.success("评论失败");
        }
        return Result.success("评论成功");
    }


    @PostMapping("/pass")
    public Result pass(BizComment bizComment) {
        bizComment.setStatus("APPROVED");
        bizCommentService.updateById(bizComment);
        return Result.success("审核成功");
    }
//
//    @PostMapping("/get/{id}")
//    public Result get(@PathVariable("id") Integer id) {
//        BizTags bizType = bizTagsService.selectOne(new EntityWrapper<BizTags>().eq("id", id));
//        return Result.success("获取成功").add("data", bizType);
//    }
//
//    @PostMapping("/edit")
//    public Result edit(BizTags bizType) {
//        try {
//            bizTagsService.updateById(bizType);
//        }catch (Exception e){
//            return  Result.error("更新失败");
//        }
//        return Result.success("更新成功");
//    }

    @PostMapping("/remove")
    public Result remove(Integer[] ids) {
        try {
            if(ids != null){
                for (int i=0; i<ids.length; i++){
                    bizCommentService.deleteById(ids[i]);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            return Result.error("删除失败");
        }
        return Result.success("成功删除"+ids.length+"条记录");
    }
}
