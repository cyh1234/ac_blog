package com.cyh.blog.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.cyh.blog.business.service.SysNoticeService;
import com.cyh.blog.business.vo.NoticeVo;
import com.cyh.blog.framework.object.PageResult;
import com.cyh.blog.persistence.beans.SysNotice;
import com.cyh.blog.util.Result;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/notice")
public class RestRoticeController {

    private final static String ROTICE_STATUS = "RELEASE";


    @Autowired
    private SysNoticeService sysNoticeService;

    @PostMapping("/list")
    public PageResult list(NoticeVo noticeVo) {
        PageInfo<SysNotice> pageInfo = sysNoticeService.selectByCondition(noticeVo);
        return Result.tablePage(pageInfo);
    }


    @PostMapping("/add")
    public Result add(SysNotice sysNotice) {
        sysNotice.setUserId(0);
        try{
            sysNoticeService.insert(sysNotice);
        }catch (Exception e){
            e.printStackTrace();
            return Result.success("添加失败");
        }
        return Result.success("添加成功");
    }

    @PostMapping("/get/{id}")
    public Result get(@PathVariable("id") Integer id) {
        SysNotice sysNotice = sysNoticeService.selectOne(new EntityWrapper<SysNotice>().eq("id", id));
        return Result.success("获取成功").add("data", sysNotice);
    }

    @PostMapping("/edit")
    public Result edit(SysNotice sysNotice) {
        try {
            sysNoticeService.updateById(sysNotice);
        }catch (Exception e){
            return  Result.error("更新失败");
        }
        return Result.success("更新成功");
    }

    @PostMapping("/remove")
    public Result remove(Integer[] ids) {
        try {
            if(ids != null){
                for (int i=0; i<ids.length; i++){
                    sysNoticeService.deleteById(ids[i]);
                }
            }
        }catch (Exception e){
            return Result.error("删除失败");
        }
        return Result.success("成功删除"+ids.length+"条记录");
    }
}
