package com.cyh.blog.controller;

import com.cyh.blog.util.Result;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

/**
 *  链接跳转
 */
@Controller
public class RenderController {

    //去主界面
    @GetMapping(value = {"/index", ""})
    public ModelAndView index() {
        return Result.view("index");
    }

    //登录界面
    @GetMapping("/login")
    public String login(){
        Integer userid = (Integer)SecurityUtils.getSubject().getPrincipal();
        if (userid != null ){
            return "index";
        }
        return "login";
    }

    //用户列表
    @GetMapping("/users")
    public String users() {
        return "user/list"; //;Result.view("");
    }

    //文章分类
    @GetMapping("/article/types")
    public ModelAndView types() {
        return Result.view("/article/types");
    }

    //标签分类
    @GetMapping("/article/tags")
    public ModelAndView tags() {
        return Result.view("/article/tags");
    }

    //新建文章
    @GetMapping("/article/publish")
    public ModelAndView publish() {
        return Result.view("/article/publish");
    }

    //文章列表
    @GetMapping("/articles")
    public ModelAndView articles() {
        return Result.view("/article/list");
    }

    //去修改一篇, 传入id， 然后在编辑文章页将此id内容获取，再填充到表单中
    @GetMapping("/article/update/{id}")
    public ModelAndView user(@PathVariable Integer id, ModelMap modelMap) {
        modelMap.addAttribute("aid",id);
        return Result.view("/article/publish");
    }

    //评论列表
    @GetMapping("/comments")
    public ModelAndView comments() {
        return Result.view("/comment/list");
    }

    //角色列表
    @GetMapping("/roles")
    public ModelAndView roles() {
        return Result.view("/role/list");
    }

    //资源列表
    @GetMapping("/resources")
    public ModelAndView resources() {
        return Result.view("/resources/list");
    }

    //公告管理
    @GetMapping("/notices")
    public ModelAndView notices() {
        return Result.view("/notices/list");
    }

    //友链管理
    @GetMapping("/links")
    public ModelAndView links() {
        return Result.view("/link/list");
    }
}
