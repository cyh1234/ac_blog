package com.cyh.blog.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.cyh.blog.business.service.SysRoleResourcesService;
import com.cyh.blog.business.service.SysRoleService;
import com.cyh.blog.business.service.SysUserRoleService;
import com.cyh.blog.business.vo.RoleVo;
import com.cyh.blog.framework.object.PageResult;
import com.cyh.blog.persistence.beans.SysRole;
import com.cyh.blog.util.Result;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/role")
public class RestRoleController {

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SysRoleResourcesService sysRoleResourcesService;

    @Autowired
    private SysUserRoleService sysUserRoleService;


    //保存用户对应的角色
    @PostMapping("/saveUserRole")
    public Result saveUserRole(Integer userId, Integer roleId) {
        sysUserRoleService.saveUserRole(userId, roleId);
        return Result.success("分配成功");
    }

    //保存角色拥有的资源
    @PostMapping("/saveRoleResource")
    public Result saveRoleResource(String ids, Integer roleId) {
        sysRoleResourcesService.saveRoleResource(ids,roleId);
        return Result.success("分配成功");
    }

    @PostMapping("/selectRoleSelected")
    public Result selectRoleSelected(Integer userId) {
        List<Map<String, Object>> list = sysRoleService.selectRoleSelected(userId);
        return Result.success("获取成功").add("data", list);
    }


    @PostMapping("/list")
    public PageResult list(RoleVo roleVo) {
        PageInfo<SysRole> pageInfo = sysRoleService.selectByCondition(roleVo);
        return Result.tablePage(pageInfo);
    }


    @PostMapping("/get/{id}")
    public Result get(@PathVariable("id") Integer id) {
        SysRole sysRole = sysRoleService.selectOne(new EntityWrapper<SysRole>().eq("id", id));
        return Result.success("获取成功").add("data", sysRole);
    }

    @PostMapping("/remove")
    public Result remove(Long[] ids) {
        try {
            if(ids != null){
                for (int i=0; i<ids.length; i++){
                    sysRoleService.deleteById(ids[i]);
                }
            }
        }catch (Exception e){
            return Result.error("删除失败");
        }
        return Result.success("成功删除"+ids.length+"条记录");
    }

    @PostMapping("/add")
    public Result add(SysRole sysRole) {
        SysRole r = sysRoleService.selectOne(new EntityWrapper<SysRole>().eq("description", sysRole.getDescription()));
        if (r != null){
            return Result.error("角色名["+sysRole.getDescription()+"]已存在,请更换");
        }
        try{
            Date date = new Date();
            sysRole.setUpdateTime(date);
            sysRole.setCreateTime(date);
            sysRoleService.insert(sysRole);
        }catch (Exception e){
            e.printStackTrace();
            return Result.success("添加失败");
        }
        return Result.success("添加成功");
    }
//
    @PostMapping("/edit")
    public Result edit(SysRole sysRole) {
        sysRole.setUpdateTime(new Date());
        try {
            sysRoleService.updateById(sysRole);
        }catch (Exception e){
            return  Result.error("更新失败");
        }
        return Result.success("更新成功");
    }
}
