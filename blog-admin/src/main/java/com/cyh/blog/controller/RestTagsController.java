package com.cyh.blog.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.cyh.blog.business.service.BizTagsService;
import com.cyh.blog.business.vo.TagsVo;
import com.cyh.blog.framework.object.PageResult;
import com.cyh.blog.persistence.beans.BizTags;
import com.cyh.blog.util.Result;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/tag")
public class RestTagsController {
    @Autowired
    private BizTagsService bizTagsService;

    @PostMapping("/list")
    public PageResult list(TagsVo tagsVo) {
        PageInfo<BizTags> pageInfo = bizTagsService.selectByCondition(tagsVo);
        return Result.tablePage(pageInfo);
    }

    @PostMapping("/getAll")
    public Result getAll() {
        List<BizTags> bizTypes = bizTagsService.selectList(null);
        return Result.success("加载成功").add("data",bizTypes);
    }



    @PostMapping("/add")
    public Result add(BizTags bizType) {
        BizTags t = bizTagsService.selectOne(new EntityWrapper<BizTags>().eq("name", bizType.getName()));
        if (t != null){
            return Result.error("分类名["+bizType.getName()+"]已存在,请更换");
        }
        try{
            bizTagsService.insert(bizType);
        }catch (Exception e){
            e.printStackTrace();
            return Result.success("添加失败");
        }
        return Result.success("添加成功");
    }

    @PostMapping("/get/{id}")
    public Result get(@PathVariable("id") Integer id) {
        BizTags bizType = bizTagsService.selectOne(new EntityWrapper<BizTags>().eq("id", id));
        return Result.success("获取成功").add("data", bizType);
    }

    @PostMapping("/edit")
    public Result edit(BizTags bizType) {
        try {
            bizTagsService.updateById(bizType);
        }catch (Exception e){
            return  Result.error("更新失败");
        }
        return Result.success("更新成功");
    }

    @PostMapping("/remove")
    public Result remove(Long[] ids) {
        try {
            if(ids != null){
                for (int i=0; i<ids.length; i++){
                    bizTagsService.deleteById(ids[i]);
                }
            }
        }catch (Exception e){
            return Result.error("删除失败");
        }
        return Result.success("成功删除"+ids.length+"条记录");
    }
}
