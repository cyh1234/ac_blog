package com.cyh.blog.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.cyh.blog.business.service.SysLinkService;
import com.cyh.blog.business.vo.LinkVo;
import com.cyh.blog.framework.object.PageResult;
import com.cyh.blog.persistence.beans.SysLink;
import com.cyh.blog.util.Result;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/link")
public class RestLinkController {

    @Autowired
    private SysLinkService sysLinkService;

    @PostMapping("/list")
    public PageResult list(LinkVo linkVo) {
        PageInfo<SysLink> pageInfo = sysLinkService.selectByCondition(linkVo);
        return Result.tablePage(pageInfo);
    }


    @PostMapping("/add")
    public Result add(SysLink sysLink) {
        try{
            sysLinkService.insert(sysLink);
        }catch (Exception e){
            e.printStackTrace();
            return Result.success("添加失败");
        }
        return Result.success("添加成功");
    }

    @PostMapping("/get/{id}")
    public Result get(@PathVariable("id") Integer id) {
        SysLink sysLink = sysLinkService.selectOne(new EntityWrapper<SysLink>().eq("id", id));
        return Result.success("获取成功").add("data", sysLink);
    }

    @PostMapping("/edit")
    public Result edit(SysLink sysLink) {
        try {
            sysLinkService.updateById(sysLink);
        }catch (Exception e){
            return  Result.error("更新失败");
        }
        return Result.success("更新成功");
    }

    @PostMapping("/remove")
    public Result remove(Integer[] ids) {
        try {
            if(ids != null){
                for (int i=0; i<ids.length; i++){
                    sysLinkService.deleteById(ids[i]);
                }
            }
        }catch (Exception e){
            return Result.error("删除失败");
        }
        return Result.success("成功删除"+ids.length+"条记录");
    }
}
