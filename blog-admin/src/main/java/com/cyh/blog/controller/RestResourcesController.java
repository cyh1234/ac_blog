package com.cyh.blog.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.cyh.blog.business.service.SysResourcesService;
import com.cyh.blog.business.vo.ResourcesVo;
import com.cyh.blog.framework.object.PageResult;
import com.cyh.blog.persistence.beans.SysResources;
import com.cyh.blog.util.Result;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/resources")
public class RestResourcesController {

    @Autowired
    private SysResourcesService sysResourcesService;

    /**
     * 角色选中的资源
     * resources 资源选中
     * @return
     */
    @PostMapping("/resourcesSelected")
    public Result resourcesSelected(Integer roleId) {
        List<Map<String, Object>>list = sysResourcesService.selectResourcesSelected(roleId);
        return Result.success("获取成功").add("data",list);
    }


    @PostMapping("/list")
    public PageResult list(ResourcesVo resourcesVo) {
        PageInfo<SysResources> pageInfo = sysResourcesService.selectByCondition(resourcesVo);
        return Result.tablePage(pageInfo);
    }


    @PostMapping("/get/{id}")
    public Result get(@PathVariable("id") Integer id) {
        SysResources sysResources = sysResourcesService.selectOne(new EntityWrapper<SysResources>().eq("id", id));
        return Result.success("获取成功").add("data", sysResources);
    }


    @PostMapping("/add")
    public Result add(SysResources sysResources) {
        SysResources r = sysResourcesService.selectOne(new EntityWrapper<SysResources>().eq("name", sysResources.getName()));
        if (r != null){
            return Result.error("角色名["+sysResources.getName()+"]已存在,请更换");
        }
        try{
            Date date = new Date();
            sysResources.setUpdateTime(date);
            sysResources.setCreateTime(date);
            sysResourcesService.insert(sysResources);
        }catch (Exception e){
            e.printStackTrace();
            return Result.error("添加失败");
        }
        return Result.success("添加成功");
    }



    @PostMapping("/remove")
    public Result remove(Integer[] ids) {
        try {
            if(ids != null){
                for (int i=0; i<ids.length; i++){
                    sysResourcesService.deleteById(ids[i]);
                }
            }
        }catch (Exception e){
            return Result.error("删除失败");
        }
        return Result.success("成功删除"+ids.length+"条记录");
    }

    @PostMapping("/edit")
    public Result edit(SysResources sysResources) {
        sysResources.setUpdateTime(new Date());
        try {
            sysResourcesService.updateById(sysResources);
        }catch (Exception e){
            return  Result.error("更新失败");
        }
        return Result.success("更新成功");
    }
}
