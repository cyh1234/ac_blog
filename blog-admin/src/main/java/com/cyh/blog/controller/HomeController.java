package com.cyh.blog.controller;

import com.cyh.blog.business.service.BizArticleService;
import com.cyh.blog.business.service.BizArticleTagsService;
import com.cyh.blog.business.service.BizCommentService;
import com.cyh.blog.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/home")
public class HomeController {

    @Autowired
    private BizArticleService bizArticleService;


    @Autowired
    private BizArticleTagsService bizArticleTagsService;

    @Autowired
    private BizCommentService bizCommentService;
    
    @GetMapping()
    public ModelAndView home(ModelMap map) {
        map.addAttribute("comments", bizCommentService.selectByNotApproved());
        return Result.view("home");
    }



}
