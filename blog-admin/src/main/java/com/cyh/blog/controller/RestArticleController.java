package com.cyh.blog.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.cyh.blog.business.constant.SessionConstant;
import com.cyh.blog.business.service.BizArticleService;
import com.cyh.blog.business.service.BizArticleTagsService;
import com.cyh.blog.business.vo.ArticleVo;
import com.cyh.blog.framework.object.PageResult;
import com.cyh.blog.persistence.beans.BizArticle;
import com.cyh.blog.persistence.beans.BizArticleTags;
import com.cyh.blog.persistence.beans.SysUser;
import com.cyh.blog.util.Result;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/article")
public class RestArticleController {

    @Autowired
    private BizArticleService bizArticleService;

    @Autowired
    private BizArticleTagsService bizArticleTagsService;


    @PostMapping("/save")
    public Result save(ArticleVo articleVo, Integer[] ts, HttpSession session) {
//        if (ts == null ){
//            return Result.error("标签不能为空");
//        }else if (bizArticle.getBizType() == null){
//            return Result.error("类别不能为空");
//        }else if (bizArticle.getCoverImage() == null){
//            return Result.error("封面不能为空");
//        }
        SysUser sysUser = (SysUser) session.getAttribute(SessionConstant.USER_SESSION_KEY);
        articleVo.setUserId(sysUser.getId());
        try {
            Integer aid = bizArticleService.insertOrUpdateBean(articleVo);
            bizArticleTagsService.delete(new EntityWrapper<BizArticleTags>().eq("article_id",aid));
            bizArticleTagsService.insertByIds(aid, ts);
        }catch (Exception e){
            e.printStackTrace();
            return Result.success("保存失败");
        }
        return Result.success("保存成功");
    }

    @RequiresPermissions("article")
    @PostMapping("/list")
    public PageResult list(ArticleVo articleVo) {
        PageInfo<BizArticle> pageInfo = bizArticleService.selectByCondition(articleVo);
        return Result.tablePage(pageInfo);
    }

    @PostMapping("/update/{id}")
    public Result user(@PathVariable("id") Integer id) {
        return Result.success("");
    }

    @PostMapping("/get/{id}")
    public Result get(@PathVariable Integer id) {
        if (id == null){
            Result.error("id不存在");
        }
        BizArticle bizArticle = bizArticleService.selectByKey(id);
        return Result.success("获取成功").add("data", bizArticle);
    }

    @PostMapping("/remove")
    public Result remove(Integer[] ids) {
        try {
            if(ids != null){
                for (int i=0; i<ids.length; i++){
                    bizArticleService.removeArticleById(ids[i]);
                }
            }
        }catch (Exception e){
            return Result.error("删除失败");
        }
        return Result.success("成功删除"+ids.length+"条记录");
    }
}